package com.kpfu.mikhail.myndk.general;

public interface LoadingView {

    void showLoading();

    void hideLoading();

}