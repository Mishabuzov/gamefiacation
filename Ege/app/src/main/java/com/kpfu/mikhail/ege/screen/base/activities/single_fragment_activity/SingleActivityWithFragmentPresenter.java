package com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity;

import android.support.annotation.NonNull;

class SingleActivityWithFragmentPresenter {

    @NonNull
    private final SingleActivityWithFragmentView mSingleActivityWithFragmentView;

    SingleActivityWithFragmentPresenter(@NonNull SingleActivityWithFragmentView singleActivityWithFragmentView) {
        mSingleActivityWithFragmentView = singleActivityWithFragmentView;
    }

    void defineToolbarScrollingState(int lastVisibleItemPosition, int size) {
        if (lastVisibleItemPosition == size - 1) {
            mSingleActivityWithFragmentView.turnOffToolbarScrolling();
        } else {
            mSingleActivityWithFragmentView.turnOnToolbarScrolling();
        }
    }

    void defineToolbarScrollingState(int scrollViewHeight,
                                     int scrollViewPaddingTop,
                                     int scrollViewPaddingBottom,
                                     int childHeight) {
        boolean isScrollable = scrollViewHeight < childHeight
                + scrollViewPaddingTop + scrollViewPaddingBottom;
        if (isScrollable) {
            mSingleActivityWithFragmentView.turnOnToolbarScrolling();
        } else {
            mSingleActivityWithFragmentView.turnOffToolbarScrolling();
        }
    }

}
