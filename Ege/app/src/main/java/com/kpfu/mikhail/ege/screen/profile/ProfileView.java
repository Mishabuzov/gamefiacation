package com.kpfu.mikhail.ege.screen.profile;

import android.content.Context;

import com.kpfu.mikhail.ege.content.Profile;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_scrolling.BaseScrollingFragmentView;

interface ProfileView extends BaseScrollingFragmentView {

    void showProfile(Profile profile);

    void upgradeToolbar();

    void setCompressAvatar();

    Context getContext();

}
