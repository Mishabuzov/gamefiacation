package com.kpfu.mikhail.ege.content.forms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kpfu.mikhail.ege.content.UserAnswer;
import com.kpfu.mikhail.ege.content.types.TrainType;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckAnswersForm {

    @JsonProperty("train_type")
    private TrainType trainType;

    @JsonProperty("list")
    private List<UserAnswer> answers;

    public CheckAnswersForm() {
    }

    public CheckAnswersForm(TrainType trainType, List<UserAnswer> answers) {
        this.trainType = trainType;
        this.answers = answers;
    }

    public TrainType getTrainType() {
        return trainType;
    }

    public void setTrainType(TrainType trainType) {
        this.trainType = trainType;
    }

    public List<UserAnswer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<UserAnswer> answers) {
        this.answers = answers;
    }

}
