package com.kpfu.mikhail.ege.screen.single_theme;

import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_recycler.BaseRecyclerFragmentView;

import java.util.List;

interface SingleThemeView extends BaseRecyclerFragmentView {

    void showTasks(List<Task> tasks);

    void showResultDialogAndConfigToolbarUpgrade(int solvedTasks,
                                                 int allTasks,
                                                 int gotPoints,
                                                 int allPoints);

    void startSingleThemeMode();

    void setTrainingTitles(List<String> titles);

    void startTimer();

    void refreshAdapter(List<Task> tasks);

}
