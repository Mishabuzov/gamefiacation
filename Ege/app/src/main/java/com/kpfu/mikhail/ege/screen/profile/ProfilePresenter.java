package com.kpfu.mikhail.ege.screen.profile;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.forms.AvatarContainer;
import com.kpfu.mikhail.ege.repository.GameProvider;
import com.kpfu.mikhail.ege.utils.AttachmentsHelper;
import com.kpfu.mikhail.ege.utils.PreferenceUtils;

import ru.arturvasilov.rxloader.LifecycleHandler;

import static com.kpfu.mikhail.ege.utils.Constants.CURRENT_USER_PROFILE_ID;

class ProfilePresenter {

    private LifecycleHandler mLifecycleHandler;
    private ProfileView mView;
    private AttachmentsHelper mAttachmentsHelper;
    private int mUserId;


    ProfilePresenter(@NonNull LifecycleHandler lifecycleHandler,
                     @NonNull ProfileView view,
                     @NonNull AttachmentsHelper attachmentsHelper,
                     int userId) {
        mLifecycleHandler = lifecycleHandler;
        mView = view;
        mAttachmentsHelper = attachmentsHelper;
        recognizeUserId(userId);
    }

    private void recognizeUserId(int userId) {
        if (userId == PreferenceUtils.getUserId()) {
            userId = CURRENT_USER_PROFILE_ID;
        }
        mUserId = userId;
    }

    void getUserProfile() {
        GameProvider.provideProfileRepository()
                .getUserProfile(mUserId)
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .compose(mLifecycleHandler.reload(R.id.user_profile_request))
                .subscribe((profile) -> {
                    mView.configToolbarBehavior();
                    mView.showProfile(profile);
                }, throwable -> mView.handleError(throwable,
                        this::getUserProfile));
    }

    private boolean isCurrentUserProfile() {
        return mUserId == CURRENT_USER_PROFILE_ID;
    }

    int calculateBadgeMargin(int gridLayoutCellWidth) {
        return (int) (gridLayoutCellWidth * 0.1);
    }

    void solveToUpgradeToolbar() {
        if (isCurrentUserProfile()) {
            mView.upgradeToolbar();
        }
    }

    void changeAvatar() {
        if (isCurrentUserProfile()) {
            mAttachmentsHelper.showAddPhotoDialogAndTryAddPhoto(R.string.change_avatar_dialog);
        }
    }

    void uploadAvatarAndUpdate(Bitmap bitmap) {
        GameProvider.provideImageService()
                .uploadAvatar(bitmap, mView.getContext())
                .doOnSubscribe(mView::showLoading)
                .compose(mLifecycleHandler.reload(R.id.upload_user_avatar))
                .subscribe(pictureLink -> updateAvatar(pictureLink.getPicture()),
                        throwable -> showTroubleMessage(throwable.getMessage()));
    }

    private void showTroubleMessage(String message) {
        mView.showToastMessage("Не удалось обновить профиль: " + message);
    }

    private void updateAvatar(String avatarUri) {
        GameProvider.provideProfileRepository()
                .updateAvatar(PreferenceUtils.getUserId(),
                        new AvatarContainer(avatarUri))
                .doOnTerminate(mView::hideLoading)
                .compose(mLifecycleHandler.reload(R.id.update_user_avatar))
                .subscribe(aVoid -> mView.setCompressAvatar(),
                        throwable -> showTroubleMessage(throwable.getMessage()));
    }

}
