package com.kpfu.mikhail.ege.screen.base.fragments.error_fragment;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.kpfu.mikhail.ege.screen.base.BaseView;
import com.kpfu.mikhail.ege.utils.Function;

public interface ErrorView extends BaseView {

    void handleError(Throwable e, Function reloadFunction);

    void handleErrorWithToasts(@NonNull Throwable e,
                               @StringRes int httpErrorToast);

    void cleanDataAndExit();

}
