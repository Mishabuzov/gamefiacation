package com.kpfu.mikhail.ege.screen.task_details;


import android.content.Context;
import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.content.forms.TaskAnswerForm;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_scrolling.BaseScrollingFragmentView;

interface TaskDetailsView extends BaseScrollingFragmentView {

    void showNewTask(Task task);

    void showAnswerMessage(TaskAnswerForm form);

    void cleanAnswerField();

    void disableSendingAnswersUI();

    void clearFields();

    void showNextTask();

    Context getContext();

    void showEmptyAnswerErrorOnTaskB();

    void showEmptyAnswerErrorOnTaskC();

    void showCurrentAnswer(String answer,
                           int imagesCount,
                           @NonNull Task currentTask);

    void showEmptyAnswerResult();

    void hideAnswer();

    void startTimer();

    void stopTimer();

}
