package com.kpfu.mikhail.ege.screen.single_theme;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

class SingleThemeAdapter extends BaseAdapter<RecyclerView.ViewHolder, Task> {

    private static final int TYPE_ITEM_THEME_HEADER = 0;

    private static final int TYPE_ITEM_INCOMPLETE_TASK = 1;

    private static final int TYPE_ITEM_COMPLETE_TASK = 2;

    private final boolean mIsTrainingMode;

    private Theme mTheme;

    private List<String> mTrainingThemeTitles;

    private String mThemeTitle;

    private TaskCallback mCallback;

//    private boolean mIsTrainingFinished = false;

    SingleThemeAdapter(boolean isTrainingMode,
                       @NonNull TaskCallback callback) {
        super(new ArrayList<>());
        mIsTrainingMode = isTrainingMode;
        mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM_THEME_HEADER) {
            return new SingleThemeHolderHeader(LayoutInflater.from(parent.getContext()).inflate(R.layout.badge_detail_list_header, parent, false),
                    mTheme);
        } else if (viewType == TYPE_ITEM_INCOMPLETE_TASK) {
            return new InCompletedTaskHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.challenge_list_item, parent, false),
                    mThemeTitle, mCallback, mIsTrainingMode);
        } else {
            return new CompletedTaskHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.completed_challenge_list_item, parent, false),
                    mThemeTitle, mCallback, mIsTrainingMode);
        }
    }

    void setTheme(Theme theme) {
        mTheme = theme;
        mThemeTitle = theme.getTitle();
    }
/*
    public void setTrainingFinished(boolean isTrainingFinished) {
        mIsTrainingFinished = isTrainingFinished;
    }*/

    void setTrainingThemeTitle(List<String> titles) {
        mTrainingThemeTitles = titles;
    }

    @Override
    public int getItemViewType(int position) {
//        Task task = getItem(position);
        //TODO: MAKE BINDING FOR COMPLETE TASK, WHEN API WILL CONTAIN FIELD WITH USER'S POINTS
        if (position == 0 && !mIsTrainingMode) {
            return TYPE_ITEM_THEME_HEADER;
        } else if (getItem(position).getPoints() > 0) {
            return TYPE_ITEM_COMPLETE_TASK;
        } else {
            return TYPE_ITEM_INCOMPLETE_TASK;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        if (holder.getItemViewType() == TYPE_ITEM_THEME_HEADER) {
            SingleThemeHolderHeader h = (SingleThemeHolderHeader) holder;
            h.bind(getItemCount());
        } else if (holder.getItemViewType() == TYPE_ITEM_INCOMPLETE_TASK) {
            InCompletedTaskHolder h = (InCompletedTaskHolder) holder;
            setThemeIfTraining(h, position);
            h.bind(getItem(position));
        } else {
            CompletedTaskHolder h = (CompletedTaskHolder) holder;
            setThemeIfTraining(h, position);
            h.bind(getItem(position));
        }
    }

    private void setThemeIfTraining(TaskHolder h,
                                    int position) {
        if (mIsTrainingMode) {
            h.setThemeTitle(mTrainingThemeTitles.get(position));
        }
    }

    interface TaskCallback {

        void openTaskDetailsScreen(int themeId, String themeTitle, int taskPosition, int taskType);

    }

}
