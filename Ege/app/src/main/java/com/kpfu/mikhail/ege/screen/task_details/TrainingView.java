package com.kpfu.mikhail.ege.screen.task_details;

public interface TrainingView {

    void startTimer();

    void stopTimer();

}
