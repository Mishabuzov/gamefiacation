package com.kpfu.mikhail.ege.screen.training;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;
import com.kpfu.mikhail.ege.screen.single_theme.SingleThemeActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.kpfu.mikhail.ege.utils.Constants.IS_TRAINING_MODE;

public class StartTrainingFragment extends BaseFragment {

    @OnClick(R.id.button_begin)
    public void onClickBeginButton() {
        Intent intent = new Intent(getActivity(), SingleThemeActivity.class);
        intent.putExtra(IS_TRAINING_MODE, true);
        startActivity(intent);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start_training, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

}
