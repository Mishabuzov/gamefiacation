package com.kpfu.mikhail.ege.screen.single_theme;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.RightAnswer;
import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.content.UserAnswer;
import com.kpfu.mikhail.ege.content.forms.CheckAnswersForm;
import com.kpfu.mikhail.ege.repository.GameProvider;

import java.util.ArrayList;
import java.util.List;

import ru.arturvasilov.rxloader.LifecycleHandler;

import static com.kpfu.mikhail.ege.content.types.TrainType.EXAM;

class SingleThemePresenter {

    @NonNull
    private final LifecycleHandler mLifecycleHandler;

    @NonNull
    private final SingleThemeView mView;

    @NonNull
    private List<Task> mTasks;

//    private long mTimeUntilPreviousTrainEnd;

    SingleThemePresenter(@NonNull LifecycleHandler lifecycleHandler,
                         @NonNull SingleThemeView view) {
        mLifecycleHandler = lifecycleHandler;
        mView = view;
        mTasks = new ArrayList<>();
    }

    private void onSuccessGettingTaskByOneTheme(List<Task> tasks, int topicId) {
        mTasks = tasks;
        mView.showTasks(tasks);
        if (tasks != null && !tasks.isEmpty()) {
            setThemeIndicatorIsLoaded(topicId);
        }
    }

    void getTasksByTopicId(int topicId) {
        if (GameProvider.provideTasksService().isThemeTasksLoaded(topicId)) {
            GameProvider.provideTasksService()
                    .getAllTasksFromTopicFromDB(topicId)
                    .doOnSubscribe(mView::showLoading)
                    .doOnTerminate(mView::hideLoading)
                    .subscribe((tasks) -> onSuccessGettingTaskByOneTheme(tasks, topicId),
                            throwable -> mView.handleError(throwable,
                                    () -> getTasksByTopicId(topicId)));
        } else {
            GameProvider.provideTasksService()
                    .getTasksFromTopicFromServer(topicId, 0, 10)
                    .doOnSubscribe(mView::showLoading)
                    .doOnTerminate(mView::hideLoading)
                    .compose(mLifecycleHandler.reload(R.id.tasks_request))
                    .subscribe((tasks) -> onSuccessGettingTaskByOneTheme(tasks, topicId),
                            throwable -> mView.handleError(throwable,
                                    () -> getTasksByTopicId(topicId)));
        }
   /*     GameProvider.provideGameRepository()
                .getTasks(topicId, 0, 10)
                .doOnSubscribe(mView::showLoading)
                .compose(mLifecycleHandler.load(R.id.tasks_request))
                .subscribe((tasks) -> {
                            mView.showTasks(tasks);
                            if (tasks != null && !tasks.isEmpty()) {
                                setThemeIndicatorIsLoaded(topicId);
                            }
                        },
                        throwable -> mView.handleError(throwable,
                                () -> getTasksByTopicId(topicId)));*/
    }

    private void setThemeIndicatorIsLoaded(int topicId) {
        GameProvider.provideTasksService()
                .setThemeTasksLoaded(topicId);
    }

 /*   private void checkUnFinishedTrainingAndGetTasks(){
        mTimeUntilPreviousTrainEnd = PreferenceUtils.getTimeUntilCurrentTrainingFinished();
        if(mTimeUntilPreviousTrainEnd <= 0){
            clearTrainingData();
        }
        getTraining();
    }*/

    private void getTraining() {
        GameProvider.provideTasksService()
                .getTrainingTasks()
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .compose(mLifecycleHandler.load(R.id.training_request))
                .subscribe(this::getTrainingTaskTitles, throwable -> mView.handleError(throwable,
                        this::getTraining));
    }

    private void getTrainingTaskTitles(List<Task> tasks) {
        mTasks = tasks;
        int[] topicIds = new int[tasks.size()];
        for (int i = 0; i < tasks.size(); i++) {
            topicIds[i] = tasks.get(i).getTopicId();
        }
        List<String> themeTitles = new ArrayList<>();
        GameProvider.provideThemesService()
                .getThemesFromDbByIds(topicIds)
                .doOnSubscribe(mView::showLoading)
                .subscribe(themes -> {
                    for (Theme theme : themes) {
                        themeTitles.add(theme.getTitle());
                    }
                    mView.setTrainingTitles(themeTitles);
                    mView.showTasks(tasks);
                    mView.startTimer();
                }, Throwable::printStackTrace);
    }

    void getUserAnswersAndFinishTraining() {
        GameProvider.provideTasksService()
                .getUserAnswers()
                .doOnSubscribe(mView::hideScreenAndShowLoading)
                .subscribe((answers) -> {
                    if (answers.isEmpty()) {
                        mView.showToastMessage(R.string.training_empty_user_answer);
                        mView.finish();
                    } else {
                        convertUserAnswersFromRealmToJsonFormat(answers);
                        finishTraining(answers);
                    }
                }, Throwable::printStackTrace);
    }

    private void convertUserAnswersFromRealmToJsonFormat(List<UserAnswer> answers) {
        for (UserAnswer userAnswer : answers) {
            if (!userAnswer.getRealmImages().isEmpty()) {
                userAnswer.convertRealmImagesToImages();
            }
        }
    }

    private void finishTraining(List<UserAnswer> userAnswers) {
        GameProvider.provideGameRepository()
                .checkUserAnswers(new CheckAnswersForm(EXAM, userAnswers))
                .compose(mLifecycleHandler.reload(R.id.check_training_request))
                .subscribe((rightAnswers) -> processResults(rightAnswers, userAnswers),
                        throwable -> mView.handleError(throwable, () -> finishTraining(userAnswers)));
    }

    void recognizeMode(boolean isTraining) {
        if (isTraining) {
            clearTrainingData();
            getTraining();
//            checkUnFinishedTrainingAndGetTasks();
        } else {
            mView.startSingleThemeMode();
        }
    }

  /*  String getThemeTitleFromDb(int topicId) {
        final String[] themeTitle = new String[1];
        GameProvider.provideThemesService()
                .getThemeFromDBById(topicId)
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .subscribe(theme -> themeTitle[0] = theme.getTitle(), Throwable::printStackTrace);
        return themeTitle[0];
    }*/

    private int mSolvedTasksCount = 0;

    private int mPoints = 0;

    private int mMaxPoints;

    private void processResults(@NonNull List<RightAnswer> rightAnswers,
                                @NonNull List<UserAnswer> userAnswers) {
        int tasksCount = rightAnswers.size();
        int currentTaskPoints;
        for (int i = 0; i < rightAnswers.size(); i++) {
            currentTaskPoints = rightAnswers.get(i).getPoints();
            mPoints += currentTaskPoints;
            mMaxPoints += rightAnswers.get(i).getMaxPoints();
            /*if (mTasks.get(i).getType() == TaskType.TASK_B.getTaskType()) {
                mTasks.get(i).setAnsweredTask(false);
            }*/
            if (currentTaskPoints != 0) {
                mSolvedTasksCount++;
                mTasks.get(i).setTrainingRightTask(true);
                mTasks.get(i).setPoints(currentTaskPoints);
            } /*else if(userAnswers.get(i).getAnswer().isEmpty()
                    && userAnswers.get(i).getImages() == null){
                mTasks.get(i).setTrainingEmptyTask(true);
            }*/
        }
        mView.refreshAdapter(mTasks);
        mView.showResultDialogAndConfigToolbarUpgrade(mSolvedTasksCount, tasksCount,
                mPoints, mMaxPoints);
      /*  clearUserAnswers();
        clearTrainingTasks();*/
    }

    void clearTrainingData() {
//        PreferenceUtils.clearTrainingTimer();
        clearUserAnswers();
        clearTrainingTasks();
//        mView.finish();
    }

    void checkOnAnswersAndUpdateAdapter(@Nullable List<Integer> indexes,
                                        @Nullable List<Task> refreshedTasks) {
        if (indexes != null && !indexes.isEmpty()
                && refreshedTasks != null && !refreshedTasks.isEmpty()) {
            for (int i = 0; i < indexes.size(); i++) {
                mTasks.set(indexes.get(i), refreshedTasks.get(i));
            }
            mView.refreshAdapter(mTasks);
        }
    }

    /*void refreshTasksFromDb(boolean isTrainingMode) {
        if (isTrainingMode) {
            getTrainingTasksFromDb();
        } else {

        }
    }

    private void getTrainingTasksFromDb() {
        GameProvider.provideTasksService()
                .getTrainingFromDb()
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .subscribe(mView::refreshAdapter, Throwable::printStackTrace);
    }*/

    private void clearUserAnswers() {
        GameProvider.provideTasksService()
                .clearTrainingUserAnswers();
    }

    private void clearTrainingTasks() {
        GameProvider.provideTasksService()
                .clearTrainingTasksInDB();
    }

}
