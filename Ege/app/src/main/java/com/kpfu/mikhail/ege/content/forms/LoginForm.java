package com.kpfu.mikhail.ege.content.forms;

public class LoginForm {

    public static final String GRANT_TYPE = "password";

    private String mLogin;

    private String mPassword;

    public LoginForm(String login, String password) {
        mLogin = login;
        mPassword = password;
    }

    public String getLogin() {
        return mLogin;
    }

    public void setLogin(String login) {
        mLogin = login;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public static String getGrantType() {
        return GRANT_TYPE;
    }
}
