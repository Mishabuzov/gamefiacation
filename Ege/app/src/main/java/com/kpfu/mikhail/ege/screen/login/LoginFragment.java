package com.kpfu.mikhail.ege.screen.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.screen.base.activities.navigation.NavigationActivity;
import com.kpfu.mikhail.ege.screen.base.fragments.base_login.BaseLoginFragment;
import com.kpfu.mikhail.ege.screen.register.RegisterActivity;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class LoginFragment extends BaseLoginFragment implements LoginView {

    @BindString(R.string.login_unsuccess_default_error) String mUnSuccessLoginError;

    @BindString(R.string.login_error_edit_text) String mErrorLogin;

    @BindString(R.string.password_error_edit_text) String mErrorPassword;

    @BindString(R.string.content_login_btn_text_hide) String mHideButtonText;

    @BindString(R.string.content_login_btn_text_show) String mShowButtonText;

    @BindView(R.id.edittext_login) EditText mLoginEditText;

    @BindView(R.id.login_text_input_layout) TextInputLayout mLoginTextInputLayout;

    @BindView(R.id.edittext_password) EditText mPasswordEditText;

    @BindView(R.id.password_text_input_layout) TextInputLayout mPasswordTextInputLayout;

    @BindView(R.id.btn_login) Button mBtnLogin;

    @BindView(R.id.show_pswd_button) Button mShowPasswordButton;

    @BindView(R.id.login_main_scroll) NestedScrollView mMainScroll;

    private LoginPresenter mPresenter;

    @OnClick(R.id.btn_login)
    public void onClickLoginBtn() {
        mPresenter.checkFieldsAndTryLogin(
                String.valueOf(mLoginEditText.getText()).trim(),
                String.valueOf(mPasswordEditText.getText()).trim());
    }

    @OnLongClick(R.id.btn_login)
    public boolean onLongClickSignInButton() {
//        if (BuildConfig.DEBUG) {
        final String[] logins = {"mishabuzov", "mishabuzov2"};
        final String[] passwords = {"qwerty007", "qwerty007"};
        new MaterialDialog.Builder(getContext()).items((CharSequence[]) logins)
                .itemsCallback((dialog, view1, which, text) -> {
                    mLoginEditText.setText(logins[which]);
                    mPasswordEditText.setText(passwords[which]);
                })
                .show();
        return true;
        //        }
    }

    @OnClick(R.id.register_button)
    public void register() {
        startActivity(new Intent(getActivity(), RegisterActivity.class));
    }

    @OnClick(R.id.show_pswd_button)
    public void onClickShowPasswordButton() {
        mPresenter.defineInputType();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        initFragmentElements();
        return view;
    }

    @Override
    public void changeStateOfLoginButton(boolean state) {
        mBtnLogin.setEnabled(state);
    }

    private void initFragmentElements() {
        mBtnLogin.setEnabled(false);
        mPresenter = new LoginPresenter(getLifecycleHandler(), this);
        setTextWatcher(mLoginEditText);
        setTextWatcher(mPasswordEditText);
    }

    private void setTextWatcher(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mPresenter.tryEnableAndDisableLoginButton(String.valueOf(mLoginEditText.getText()).trim(),
                        String.valueOf(mPasswordEditText.getText()).trim());
            }
        });
    }

    @Override
    public void openThemesScreen() {
        startActivity(new Intent(getActivity(), NavigationActivity.class));
    }

    @Override
    public void showErrorInLoginField() {
        showErrorInInputLayout(mLoginTextInputLayout, mErrorLogin);
    }

    @Override
    public void hideErrorInLoginField() {
        hideErrorFromInputLayout(mLoginTextInputLayout);
    }

    @Override
    public void showErrorInPasswordField() {
        showErrorInInputLayout(mPasswordTextInputLayout, mErrorPassword);
    }

    @Override
    public void hideErrorInPasswordField() {
        hideErrorFromInputLayout(mPasswordTextInputLayout);
    }

    @Override
    public void showPassword() {
        super.showPassword(mPasswordEditText, mShowPasswordButton, mHideButtonText);
    }

    @Override
    public void hidePassword() {
        super.hidePassword(mPasswordEditText, mShowPasswordButton, mShowButtonText);
    }

    @Override
    protected NestedScrollView getMainScroll() {
        return mMainScroll;
    }

}