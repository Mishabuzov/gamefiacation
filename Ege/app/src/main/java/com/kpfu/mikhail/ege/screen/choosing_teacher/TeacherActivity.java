package com.kpfu.mikhail.ege.screen.choosing_teacher;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleFragmentActivity;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;
import com.kpfu.mikhail.ege.utils.ToolbarUtils;
import com.kpfu.mikhail.ege.utils.UiUtils;

public class TeacherActivity extends SingleFragmentActivity
        implements TeacherFragment.TeacherActivityCallback {

    private TeacherFragment mFragment;

    private ImageButton mNextButton;

    @Override
    protected BaseFragment installFragment() {
        mFragment = new TeacherFragment();
        return mFragment;
    }

    @Override
    protected Bundle attachFragmentArguments() {
        return null;
    }

    @Override
    protected void doCreatingActions() {
        upgradeToolbar();
        setToolbarTitle(R.string.choosing_teacher_title);
    }

    private void upgradeToolbar() {
        View upgradeView = UiUtils.getUpgradeView(this, R.layout.toolbar_school_upgrade);
        mNextButton = (ImageButton) upgradeView.findViewById(R.id.next_button);
        ViewGroup mainToolbarView = (ViewGroup) getToolbar().findViewById(R.id.main_toolbar_layout);
        mainToolbarView.addView(upgradeView);
        ToolbarUtils.configToolbarUpgradeView(upgradeView);
        setOnPreviousClickListener(upgradeView);
        setOnNextClickListener();
    }

    private void setOnPreviousClickListener(View upgradeView) {
        ImageButton imageButton = (ImageButton) upgradeView.findViewById(R.id.back_arrow);
        imageButton.setOnClickListener(v -> finish());
    }

    private void setOnNextClickListener() {
        mNextButton.setOnClickListener(v -> mFragment.finishChoosingTeacher());
    }

    @Override
    public void showNextButton() {
        if (mNextButton.getVisibility() == View.INVISIBLE) {
            mNextButton.setVisibility(View.VISIBLE);
        }
    }

}
