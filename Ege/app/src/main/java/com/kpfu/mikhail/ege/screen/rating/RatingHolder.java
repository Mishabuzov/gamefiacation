package com.kpfu.mikhail.ege.screen.rating;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.kpfu.mikhail.ege.BuildConfig;
import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.RatingItem;
import com.kpfu.mikhail.ege.utils.AndroidUtils;
import com.kpfu.mikhail.ege.widget.textviews.RobotoBoldTextView;
import com.kpfu.mikhail.ege.widget.textviews.RobotoRegularTextView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

class RatingHolder extends RecyclerView.ViewHolder {

    @BindColor(R.color.selected_color) int mSelectedColor;
    @BindColor(R.color.theme_primary) int mUnSelectedColor;
    @BindView(R.id.rating_num) RobotoRegularTextView mRatingNum;
    @BindView(R.id.user_avatar) ImageView mUserAvatar;
    @BindView(R.id.name_tv) RobotoBoldTextView mNameTv;
    @BindView(R.id.points) RobotoRegularTextView mPoints;
    @BindView(R.id.points_hint) RobotoRegularTextView mPointsHint;
    private View mMainView;
    private RatingAdapter.RatingCallback mCallback;

    RatingHolder(@NonNull View itemView,
                 @NonNull RatingAdapter.RatingCallback callback) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mMainView = itemView;
        mCallback = callback;
    }

    void bind(@NonNull RatingItem ratingItem) {
        mNameTv.setText(ratingItem.getName());
        mPoints.setText(String.valueOf(ratingItem.getPoints()));
        mRatingNum.setText(String.valueOf(getAdapterPosition() + 1));
        setUserAvatar(ratingItem.getAvatar());
        mPointsHint.setText(AndroidUtils.getPointsSubLineResId(ratingItem.getPoints()));
        mMainView.setOnClickListener(view -> {
            mMainView.setBackgroundColor(mSelectedColor);
            mCallback.showUserProfile(ratingItem.getId());
            mMainView.setBackgroundColor(mUnSelectedColor);
        });
    }

    private void setUserAvatar(String avatarUri) {
        if (avatarUri == null || avatarUri.isEmpty()) {
            mUserAvatar.setImageResource(R.drawable.user_pic);
        } else {
            Picasso.with(mUserAvatar.getContext())
                    .load(BuildConfig.API_ENDPOINT + avatarUri)
                    .into(mUserAvatar, new Callback() {
                        @Override
                        public void onSuccess() {
                            mCallback.showScreenAndHideLoading();
                        }

                        @Override
                        public void onError() {
                            mCallback.showNetworkErrorMessage();
                        }
                    });
        }
    }

}
