package com.kpfu.mikhail.ege.utils;


public final class Constants {

    public static final int NONE_SCROLLING = 0;

    public static final int HTTP_EXCEPTION_CODE = 400;

    public static final int SERVER_EXCEPTION_CODE = 500;

    public static final int MIN_LOGIN_LENGTH = 2;

    public static final int MIN_PASSWORD_LENGTH = 4;

    public static final int MIN_EMAIL_LENGTH = 4;

    public static final int EXAM_TIME = 14_401_000;

    public static final String GRANT_TYPE = "grant_type";

    public static final String GRANT_TYPE_PASSWORD = "password";

    public static final String USERNAME = "username";

    public static final String PASSWORD = "password";

    public static final String REFRESHED_INDEXES = "refreshed_indexes";

    public static final String REFRESHED_TASKS = "refreshed_tasks";

    public static final String TOPIC_ID_FIELD = "topicId";

    public static final String IS_TRAINING_TASK_FIELD = "mIsTrainingTask";

    public static final String CODE_FIELD = "code";

    public static final String IS_TRAINING_MODE = "is_training_mode";

    public static final String ID_FIELD = "id";

    public static final int MAX_ATTACHMENTS_SIZE = 5;

    public static final int CURRENT_USER_PROFILE_ID = 0;

    public static final String USER_ID = "user_id";

    public static final String TIMER = "timer";

    public static final String TYPE_FIELD = "type";

    public static final String THEME_TYPE = "theme_type_extra";

    public static final int ALL_THEMES = -1;

    public static final String IS_TRAINING_FINISHED = "is_training_finished";

}
