package com.kpfu.mikhail.ege.screen.themes_and_types;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;
import com.kpfu.mikhail.ege.screen.themes_and_types.themes.ThemesFragment;
import com.kpfu.mikhail.ege.screen.themes_and_types.types.TypesFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ThemesSwitchFragment extends BaseFragment {

    private static final int THEMES_PAGE = 0;

    private static final int TYPES_PAGE = 1;

    @BindString(R.string.themes_fragment_header) String mThemesHeader;

    @BindString(R.string.types_fragment_header) String mTypesHeader;

    @BindView(R.id.search_tabs) TabLayout mTabs;

    @BindView(R.id.search_pager) ViewPager mPager;

//    @BindView(R.id.toolbar) Toolbar mToolbar;

//    @BindView(R.id.toolbar_title) TextView mToolbarTitle;

    private ThemesFragment mThemesFragment;

    private TypesFragment mTypesFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_switch_themes, container, false);
        ButterKnife.bind(this, view);
        initFragments();
        setupViewPager();
        return view;
    }

    private void initFragments() {
        mThemesFragment = new ThemesFragment();
        mTypesFragment = new TypesFragment();
    }

    PagerAdapter pagerAdapter;

    private void setupViewPager() {
        pagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager());
        pagerAdapter.addFragment(mThemesFragment, getString(R.string.themes_fragment_header));
        pagerAdapter.addFragment(mTypesFragment, getString(R.string.types_fragment_header));
        mPager.setAdapter(pagerAdapter);
        mTabs.setVisibility(View.VISIBLE);
        mTabs.setupWithViewPager(mPager);
      /*  mTabs.post(new Runnable() {
            @Override
            public void run() {
                mTabs.setupWithViewPager(mPager);
            }
        });*/
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                LayoutParams params = (LayoutParams) mToolbar.getLayoutParams();
                switch (position) {
                    case THEMES_PAGE:
                        mThemesFragment.setToolbarState();
                        setToolbarTitle(mThemesHeader);
                        break;
                    case TYPES_PAGE:
                        mTypesFragment.setToolbarState();
                        setToolbarTitle(mTypesHeader);
                        break;
                }
               /* mToolbar.setLayoutParams(params);
                mToolbarTitle.setText(title);*/
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private static class PagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        void addFragment(Fragment fragment, String fragmentTitle) {
            mFragments.add(fragment);
            mFragmentTitles.add(fragmentTitle);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }
    }

}
