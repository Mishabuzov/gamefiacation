package com.kpfu.mikhail.ege.content.forms;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.realm.RealmObject;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RealmString extends RealmObject {

    @JsonIgnore
    private String mString;

    public RealmString(String string) {
        mString = string;
    }

    public RealmString() {
    }

    public String getString() {
        return mString;
    }

    public void setString(String string) {
        mString = string;
    }
}
