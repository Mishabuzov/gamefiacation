package com.kpfu.mikhail.ege.utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.kpfu.mikhail.ege.R;

public class ToolbarUtils {

    public static void configToolbarUpgradeView(View upgradeView) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) upgradeView.getLayoutParams();
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        upgradeView.setLayoutParams(params);
    }

    public static void configToolbarRightButtonView(View upgradeView, int marginEnd) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) upgradeView.getLayoutParams();
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        params.addRule(RelativeLayout.ALIGN_PARENT_END);
        params.setMargins(0, 0, marginEnd, 0);
        upgradeView.setLayoutParams(params);
    }

    public static void setToolbarBackArrow(@NonNull View backArrowView,
                                           @NonNull Toolbar toolbar,
                                           @NonNull Function onClickFunction) {
        backArrowView.setOnClickListener(v -> onClickFunction.action());
        ViewGroup mainToolbarView = (ViewGroup) toolbar.findViewById(R.id.main_toolbar_layout);
        mainToolbarView.addView(backArrowView);
        ToolbarUtils.configToolbarUpgradeView(backArrowView);
    }

}
