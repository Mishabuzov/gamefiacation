package com.kpfu.mikhail.ege.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.forms.TaskAnswerForm;

import static android.content.DialogInterface.BUTTON_NEUTRAL;
import static android.content.DialogInterface.BUTTON_POSITIVE;

public class DialogUtils {

    public static void createTaskAnswerDialog(@NonNull Activity activity,
                                              @NonNull TaskAnswerForm form) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        setDialogMessage(activity, alertDialog, form);
        configDialogButtons(alertDialog, activity, form.getNextFunction());
        alertDialog.show();
    }

    private static void setDialogMessage(Activity activity,
                                         AlertDialog alertDialog,
                                         TaskAnswerForm form) {
        String yourAnswerMessage = getResourceById(activity, R.string.task_details_your_answer);
        String pointsMessage = getResourceById(activity, R.string.task_details_points);
        String maxPointsMessage = getResourceById(activity, R.string.task_details_max_points);
        String format = getResourceById(activity, R.string.answer_message_format);
        String message = String.format(format, yourAnswerMessage, form.getTextAnswer(),
                pointsMessage, form.getPoints(),
                maxPointsMessage, form.getMaxPoints());
        switch (form.getAnswerType()) {
            case RIGHT:
                setDialogParams(alertDialog, R.string.task_details_right_answer, message);
                alertDialog.setIcon(R.drawable.right);
                break;
            case WRONG:
                setDialogParams(alertDialog, R.string.task_details_wrong_answer, message);
                alertDialog.setIcon(R.drawable.error);
                break;
            case C_TASK:
                String attachmentMessage = getResourceById(activity, R.string.alert_dialog_attachments_text_view);
                String taskMessage = getResourceById(activity, R.string.alert_dialog_task_text_view);
                String countMessage = getResourceById(activity, R.string.task_c_answer_message_count);
                format = getResourceById(activity, R.string.task_c_answer_message_format);
                message = String.format(format, taskMessage, form.getTaskCode(),
                        yourAnswerMessage, form.getTextAnswer(), attachmentMessage,
                        form.getAttachmentsCount(), countMessage);
                setDialogParams(alertDialog, R.string.alert_task_c_title, message);
        }
    }

    private static void setDialogParams(@NonNull AlertDialog dialog,
                                        @StringRes int title,
                                        @NonNull String message) {
        dialog.setTitle(title);
        dialog.setMessage(message);
    }

    private static void configDialogButtons(@NonNull AlertDialog alertDialog,
                                            @NonNull Activity activity,
                                            @NonNull Function nextFunction) {
        String positiveMessage = getResourceById(activity, R.string.alert_diaolg_button_next);
        String neutralMessage = getResourceById(activity, R.string.alert_diaolg_button_stop);
        alertDialog.setButton(BUTTON_POSITIVE, positiveMessage, (dialog, which) -> {
            alertDialog.closeOptionsMenu();
            nextFunction.action();
        });
        alertDialog.setButton(BUTTON_NEUTRAL, neutralMessage, (dialog, which) -> alertDialog.closeOptionsMenu());
        alertDialog.setOnShowListener(dialog -> {
            alertDialog.getButton(BUTTON_POSITIVE).setTextColor(ContextCompat
                    .getColor(activity, R.color.selected_color));
            alertDialog.getButton(BUTTON_NEUTRAL).setTextColor(ContextCompat
                    .getColor(activity, R.color.theme_primary));
        });
    }

    private static String getResourceById(Context activity,
                                          @StringRes int stringRes) {
        return activity.getResources().getString(stringRes);
    }

    public static void createConfirmDialogWithContent(@NonNull Context context,
                                                      @NonNull Function nextFunction,
                                                      @StringRes int title,
                                                      @StringRes int content) {
        new MaterialDialog.Builder(context)
                .title(title)
                .content(content)
                .positiveText(R.string.standard_dialog_positive)
                .positiveColor(ContextCompat.getColor(context, R.color.card_background))
                .negativeText(R.string.standard_dialog_negative)
                .negativeColor(ContextCompat.getColor(context, R.color.card_background))
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    nextFunction.action();
                })
                .onNegative((dialog, which) -> dialog.dismiss())
                .dismissListener(DialogInterface::dismiss)
                .show();
    }

    public static void createConfirmDialog(@NonNull Context context,
                                           @NonNull Function nextFunction,
                                           @StringRes int title) {
        new MaterialDialog.Builder(context)
                .title(title)
                .positiveText(R.string.standard_dialog_positive)
                .positiveColor(ContextCompat.getColor(context, R.color.card_background))
                .negativeText(R.string.standard_dialog_negative)
                .negativeColor(ContextCompat.getColor(context, R.color.card_background))
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    nextFunction.action();
                })
                .onNegative((dialog, which) -> dialog.dismiss())
                .dismissListener(DialogInterface::dismiss)
                .show();
    }

    public static void createTrainingResultMessageAndShowDialog(@NonNull Context context,
                                                                @NonNull Function nextFunction,
                                                                int solvedTasks,
                                                                int allTasks,
                                                                int gotPoints,
                                                                int allPoints) {
        String format = getResourceById(context, R.string.training_result_dialog_format);
        String rightSolvedLabel = getResourceById(context, R.string.training_result_dialog_label_solved);
        String gotPointsLabel = getResourceById(context, R.string.training_result_dialog_label_points);
        String resultMessage = String.format(format, rightSolvedLabel, String.valueOf(solvedTasks),
                String.valueOf(allTasks), gotPointsLabel, String.valueOf(gotPoints), String.valueOf(allPoints));
        createConfirmDialogAndShow(context, nextFunction, resultMessage);
    }

    public static void createConfirmDialogAndShow(@NonNull Context context,
                                                  @NonNull Function nextFunction,
                                                  @NonNull String resultMessage) {
        new MaterialDialog.Builder(context)
                .title(R.string.training_result_dialog_title)
                .titleGravity(GravityEnum.CENTER)
                .content(resultMessage)
                .positiveText(R.string.content_dialog_button_ok)
                .positiveColor(ContextCompat.getColor(context, R.color.selected_color))
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    nextFunction.action();
                })
                .onNegative((dialog, which) -> dialog.dismiss())
                .dismissListener(DialogInterface::dismiss)
                .show();
    }

    public enum AnswerType {
        RIGHT, WRONG, C_TASK;
    }

}
