package com.kpfu.mikhail.ege.screen.single_theme;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_recycler.BaseRecyclerFragment;
import com.kpfu.mikhail.ege.screen.task_details.TaskDetailsActivity;
import com.kpfu.mikhail.ege.utils.DialogUtils;
import com.kpfu.mikhail.ege.utils.Function;

import java.util.List;

import static com.kpfu.mikhail.ege.screen.task_details.TaskDetailsActivity.TASK_TYPE;
import static com.kpfu.mikhail.ege.screen.task_details.TaskDetailsFragment.TASK_POSITION;
import static com.kpfu.mikhail.ege.screen.task_details.TaskDetailsFragment.THEME_ID;
import static com.kpfu.mikhail.ege.screen.task_details.TaskDetailsFragment.THEME_TITLE;
import static com.kpfu.mikhail.ege.utils.Constants.IS_TRAINING_FINISHED;
import static com.kpfu.mikhail.ege.utils.Constants.IS_TRAINING_MODE;
import static com.kpfu.mikhail.ege.utils.Constants.REFRESHED_INDEXES;
import static com.kpfu.mikhail.ege.utils.Constants.REFRESHED_TASKS;
import static com.kpfu.mikhail.ege.utils.Constants.TIMER;

public class SingleThemeFragment extends BaseRecyclerFragment
        implements SingleThemeView, SingleThemeAdapter.TaskCallback {

    public static final String THEME = "theme";

    public static final int REQUEST_TIMER = 1;

    public static final int REQUEST_SINGLE_THEME = 2;

    private SingleThemePresenter mPresenter;

    private SingleThemeAdapter mAdapter;

    private Theme mTheme;

    private boolean mIsTrainingMode = false;

    private TimerCallback mCallback;

    private Bundle mArgs;

    @Override
    protected void getArgs() {
        mArgs = getArguments();
        if (mArgs != null) {
            mIsTrainingMode = mArgs.getBoolean(IS_TRAINING_MODE);
        }
    }

    @Override
    protected void doActions() {
        mPresenter.recognizeMode(mIsTrainingMode);
    }

    @Override
    public void startSingleThemeMode() {
        mTheme = mArgs.getParcelable(THEME);
        if (mTheme != null) {
            mPresenter.getTasksByTopicId(mTheme.getId());
            mAdapter.setTheme(mTheme);
        }
    }

    @Override
    public void setTrainingTitles(List<String> titles) {
        mAdapter.setTrainingThemeTitle(titles);
    }

    @Override
    public void startTimer() {
        ((TimerCallback) getActivity()).updateTimer(
                ((TimerCallback) getActivity()).getTimeUntilFinished());
    }

    @Override
    protected void initPresenter() {
        mPresenter = new SingleThemePresenter(getLifecycleHandler(), this);
    }

    @Override
    protected void installAdapter() {
        mAdapter = new SingleThemeAdapter(mIsTrainingMode, this);
        mAdapter.attachToRecyclerView(getRecyclerView());
        getRecyclerView().setAdapter(mAdapter);
    }

    @Override
    public void showTasks(List<Task> tasks) {
        if (!mIsTrainingMode) {
            tasks.add(0, new Task());  //ADDING HEADER
        }
        mAdapter.changeDataSet(tasks);
        configToolbarBehavior(tasks.size());
        showScreenAndHideLoading();
    }

    @Override
    public void showResultDialogAndConfigToolbarUpgrade(int solvedTasks,
                                                        int allTasks,
                                                        int gotPoints,
                                                        int allPoints) {
        Function detailsFunction = () -> DialogUtils.createTrainingResultMessageAndShowDialog(
                getContext(), () -> {
                }, solvedTasks, allTasks, gotPoints, allPoints);
        detailsFunction.action();
        ((TimerCallback) getActivity()).configToolbarUpgrade(detailsFunction);
        showScreenAndHideLoading();
    }

    private boolean isTrainingFinished() {
        return ((TimerCallback) getActivity()).isTrainingFinished();
    }

    @Override
    public void openTaskDetailsScreen(int themeId, String themeTitle, int taskPosition, int taskType) {
        Intent intent = new Intent(getActivity(), TaskDetailsActivity.class);
        intent.putExtra(THEME_ID, themeId);
        intent.putExtra(THEME_TITLE, themeTitle);
        intent.putExtra(TASK_POSITION, taskPosition);
        intent.putExtra(TASK_TYPE, taskType);
        intent.putExtra(IS_TRAINING_MODE, mIsTrainingMode);
        intent.putExtra(IS_TRAINING_FINISHED, isTrainingFinished());
        if (mIsTrainingMode && !isTrainingFinished()) {
            mCallback = (TimerCallback) getActivity();
            intent.putExtra(TIMER, mCallback.getTimeUntilFinishedAndStopTimer());
            startActivityForResult(intent, REQUEST_TIMER);
        } else {
            startActivityForResult(intent, REQUEST_SINGLE_THEME);
        }
    }

    void finishTraining() {
        mPresenter.getUserAnswersAndFinishTraining();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_TIMER:
                    long timeUntilFinished = data.getLongExtra(TIMER, 0);
                    mCallback.updateTimer(timeUntilFinished);
            }
            mPresenter.checkOnAnswersAndUpdateAdapter(
                    data.getIntegerArrayListExtra(REFRESHED_INDEXES),
                    data.getParcelableArrayListExtra(REFRESHED_TASKS));
        }
    }

    @Override
    public void refreshAdapter(List<Task> tasks) {
        mAdapter.changeDataSet(tasks);
    }

    void cleanTrainingDataAndExit() {
        mPresenter.clearTrainingData();
        getActivity().finish();
    }

    interface TimerCallback {

        long getTimeUntilFinishedAndStopTimer();

        long getTimeUntilFinished();

        void updateTimer(long time);

        boolean isTrainingFinished();

        void configToolbarUpgrade(Function detailsFunction);

    }

}