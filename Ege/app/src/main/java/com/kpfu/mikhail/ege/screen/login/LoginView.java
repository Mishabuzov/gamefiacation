package com.kpfu.mikhail.ege.screen.login;

import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_scrolling.BaseScrollingFragmentView;

interface LoginView extends BaseScrollingFragmentView {

    void changeStateOfLoginButton(boolean state);

    void openThemesScreen();

    void showErrorInLoginField();

    void hideErrorInLoginField();

    void showErrorInPasswordField();

    void hideErrorInPasswordField();

    void showPassword();

    void hidePassword();

}