package com.kpfu.mikhail.ege.screen.base.activities.navigation;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.content.types.ScreenType;
import com.kpfu.mikhail.ege.screen.themes_and_types.ThemesSwitchFragment;

import static com.kpfu.mikhail.ege.content.types.ScreenType.THEMES;

class NavigationPresenter {

    @NonNull
    private final NavigationView mView;

    private ScreenType mCurrentType;

    NavigationPresenter(@NonNull NavigationView view) {
        mView = view;
        mCurrentType = THEMES;
    }

    void checkAndSwitchScreen(ScreenType type) {
        if (type != mCurrentType) {
            mView.hideNetworkErrorLayout();
            mView.hideProfileToolbarUpgrade();
            switchScreen(type);
            mCurrentType = type;
        }
    }

    private void switchScreen(ScreenType type) {
        switch (type) {
            case THEMES:
                mView.setFragment(new ThemesSwitchFragment());
//                mView.startThemesActivity();
                break;
            case PROFILE:
                mView.setProfileFragment();
//                startActivity(new Intent(this, AuthActivity.class));
                break;
            case TRAINING:
                mView.turnOnToolbarScrolling();
                mView.setTrainingFragment();
//                setFragment(new FavoritesFragment());
                break;
            case RATING:
                mView.setRatingFragment();
/*                PreferenceUtils.clearPreference();
                startActivity(new Intent(this, AuthActivity.class));
                this.finish();*/
                break;
        }
    }

}
