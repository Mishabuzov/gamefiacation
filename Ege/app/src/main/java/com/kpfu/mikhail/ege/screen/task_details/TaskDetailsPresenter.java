package com.kpfu.mikhail.ege.screen.task_details;


import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.util.SparseArrayCompat;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.PictureLink;
import com.kpfu.mikhail.ege.content.RightAnswer;
import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.content.UserAnswer;
import com.kpfu.mikhail.ege.content.forms.CheckAnswersForm;
import com.kpfu.mikhail.ege.content.forms.TaskAnswerForm;
import com.kpfu.mikhail.ege.content.types.task_types.TaskType;
import com.kpfu.mikhail.ege.repository.GameProvider;
import com.kpfu.mikhail.ege.utils.AttachmentsHelper;
import com.kpfu.mikhail.ege.utils.DialogUtils.AnswerType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.arturvasilov.rxloader.LifecycleHandler;

import static com.kpfu.mikhail.ege.content.types.TrainType.FREE;
import static com.kpfu.mikhail.ege.content.types.task_types.TaskType.TASK_B;
import static com.kpfu.mikhail.ege.content.types.task_types.TaskType.TASK_C;
import static com.kpfu.mikhail.ege.screen.task_details.TaskDetailsFragment.TaskCallback;
import static com.kpfu.mikhail.ege.utils.Constants.MAX_ATTACHMENTS_SIZE;
import static com.kpfu.mikhail.ege.utils.DialogUtils.AnswerType.C_TASK;
import static com.kpfu.mikhail.ege.utils.DialogUtils.AnswerType.RIGHT;
import static com.kpfu.mikhail.ege.utils.DialogUtils.AnswerType.WRONG;

class TaskDetailsPresenter {

    @NonNull
    private final LifecycleHandler mLifecycleHandler;

    @NonNull
    private final TaskDetailsView mView;

    private List<Task> mTasks;

    private Map<Integer, Task> mAnsweredTasks;

    private TaskCallback mTaskCallback;

    private int mLastPosition;

    private int mPenultimatePosition;

    private AttachmentsHelper mAttachmentsHelper;

    private boolean mIsTrainingMode;

    private SparseArrayCompat<UserAnswer> mAnswers;

    private int mTopicId;

    private boolean mIsTrainingFinished;

    TaskDetailsPresenter(@NonNull LifecycleHandler lifecycleHandler,
                         @NonNull TaskDetailsView view,
                         @NonNull TaskCallback taskCallback,
                         @NonNull AttachmentsHelper attachmentsHelper,
                         boolean isTrainingMode,
                         boolean isTrainingFinished) {
        mLifecycleHandler = lifecycleHandler;
        mView = view;
        mTaskCallback = taskCallback;
        mAttachmentsHelper = attachmentsHelper;
        mIsTrainingMode = isTrainingMode;
        mIsTrainingFinished = isTrainingFinished;
        mAnswers = new SparseArrayCompat<>();
        mAnsweredTasks = new HashMap<>();
    }

    public void setTopicId(int topicId) {
        mTopicId = topicId;
    }

    private void getTasksByTopic(int taskPosition) {
        if (GameProvider.provideTasksService().isThemeTasksLoaded(mTopicId)) {
            GameProvider.provideTasksService()
                    .getAllTasksFromTopicFromDB(mTopicId)
                    .doOnSubscribe(mView::showLoading)
                    .doOnTerminate(mView::hideLoading)
                    .subscribe((tasks) -> onSuccessGettingTasks(tasks, taskPosition)
                            , throwable -> mView.handleError(throwable,
                                    () -> getTasksByTopic(taskPosition)));
        } else {
            GameProvider.provideTasksService()
                    .getTasksFromTopicFromServer(mTopicId, 0, 10)
                    .doOnSubscribe(mView::showLoading)
                    .doOnTerminate(mView::hideLoading)
                    .compose(mLifecycleHandler.reload(R.id.tasks_request))
                    .subscribe((tasks) -> onSuccessGettingTasks(tasks, taskPosition)
                            , throwable -> mView.handleError(throwable,
                                    () -> getTasksByTopic(taskPosition)));
        }

    }

    private void onSuccessGettingTasks(List<Task> tasks, int position) {
        mTasks = tasks;
        mLastPosition = tasks.size() - 1;
        mPenultimatePosition = tasks.size() - 2;
        getUserAnswers();
        updateTaskInfo(position);
    }

    private void getUserAnswers() {
        GameProvider.provideTasksService()
                .getUserAnswers()
                .doOnSubscribe(mView::showLoading)
                .subscribe(this::convertAnswersToSparse, Throwable::printStackTrace);
    }

    void getTasksAndShowOne(int taskPosition) {
        if (mIsTrainingMode) {
            getTrainingTasks(taskPosition);
        } else {
            getTasksByTopic(taskPosition);
        }
    }

    private void getTrainingTasks(int taskPosition) {
        GameProvider.provideTasksService()
                .getTrainingTasks()
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .compose(mLifecycleHandler.reload(R.id.training_request))
                .subscribe(tasks -> onSuccessGettingTasks(tasks, taskPosition),
                        throwable -> mView.handleError(throwable,
                                () -> getTrainingTasks(taskPosition)));
    }

    private void getAnswerAndShowIt(Task currentTask) {
        UserAnswer currentAnswer = mAnswers.get(currentTask.getId());
        if (currentAnswer != null) {
            mView.showCurrentAnswer(currentAnswer.getAnswer(),
                    currentAnswer.getImagesCount(), currentTask);
        } else if (mIsTrainingFinished) {
            mView.showEmptyAnswerResult();
        } else {
            mView.hideAnswer();
        }
    }

    void defineTrainingState() {
        if (mIsTrainingFinished) {
            mView.disableSendingAnswersUI();
        }
    }

    void updateTaskInfo(int position) {
        mAttachmentsHelper.cleanAttachments();
        mView.cleanAnswerField();
        if (position > mTasks.size() - 1) {
            mView.finish();
            if (mIsTrainingMode) {
                mView.showToastMessage(R.string.task_details_last_training_task_message);
            } else {
                mView.showToastMessage(R.string.task_details_last_task_message);
            }
            return;
        }
        Task currentTask = mTasks.get(position);
        mView.hideMainScrollView();
        mView.showNewTask(currentTask);
        if (mIsTrainingMode) {
            getAnswerAndShowIt(currentTask);
        }
        mTaskCallback.configColors(currentTask.getType());
        mView.configToolbarBehavior();
        checkOnLastItems(position);
        mView.scrollUp();
        mView.showMainScrollView();
    }

    private void checkOnLastItems(int position) {
        int firstPosition = 0;
        int secondPosition = 1;
        if (position == mLastPosition) {
            mTaskCallback.hideRightArrow();
        } else if (position == firstPosition) {
            mTaskCallback.hideLeftArrow();
        } else if (position == secondPosition) {
            mTaskCallback.showLeftArrow();
        } else if (position == mPenultimatePosition) {
            mTaskCallback.showRightArrow();
        }
    }

    void tryAddPhoto() {
        if (mAttachmentsHelper.getAttachmentSize() < MAX_ATTACHMENTS_SIZE) {
            mAttachmentsHelper.showAddPhotoDialogAndTryAddPhoto(R.string.add_photo_dialog_title);
        } else {
            mView.showToastMessage(R.string.too_many_content_message);
        }
    }

    void checkTextAnswerAndProcess(String textAnswer, Task task) {
        if (task.getType() == TASK_B.getTaskType() && textAnswer.isEmpty()) {
            mView.showEmptyAnswerErrorOnTaskB();
        } else if (task.getType() == TASK_C.getTaskType()
                && mAttachmentsHelper.getAttachmentSize() == 0) {
            mView.showEmptyAnswerErrorOnTaskC();
        } else {
            if (textAnswer.isEmpty()) {
                textAnswer = null;
            }
            mView.showLoading();
            processAnswer(task, textAnswer);
        }
    }

    private void processAnswer(Task task, String textAnswer) {
        if (task.getType() == TASK_B.getTaskType()) {
            recognizeMode(new UserAnswer(task.getId(), textAnswer), TASK_B, task);
        } else if (task.getType() == TASK_C.getTaskType()
                && !mAttachmentsHelper.getPhotoLinks().isEmpty()) {
            uploadBitmapsAndCheckAnswers(mAttachmentsHelper.getCompressedImages(), textAnswer, task);
        } else {
            recognizeMode(new UserAnswer(task.getId(), textAnswer), TASK_C, task);
        }
    }

    private void recognizeMode(UserAnswer userAnswer,
                               TaskType type,
                               Task task) {
        if (mIsTrainingMode) {
            saveAnswer(userAnswer, task);
        } else {
            sendAnswer(setAnswer(userAnswer), type, task);
        }
    }

    private void saveAnswer(UserAnswer answer, Task task) {
        GameProvider.provideTasksService()
                .insertOrUpdateUserAnswer(answer);
        int taskIndex = mTasks.indexOf(task);
        mAnswers.put(answer.getId(), answer);
        mTasks.get(taskIndex).setAnsweredTask(true);
        saveTaskIndex(taskIndex, task);
        mView.showToastMessage("Ваш ответ записан!");
        mView.clearFields();
        mView.showNextTask();
        mView.hideLoading();
    }

    private void saveTaskIndex(int taskIndex, @NonNull Task task) {
        mAnsweredTasks.put(taskIndex, task);
    }

    void refreshDataInDb() {
        if (mIsTrainingMode) {
            refreshTrainingTasks();
        } else {
            refreshTasksFromTheme();
        }
    }

    private void refreshTrainingTasks() {
        GameProvider.provideTasksService()
                .updateTrainingTasksInDB(getTasksByIds());
    }

    private void refreshTasksFromTheme() {
        GameProvider.provideTasksService()
                .updateTasksFromTopicInDB(mTopicId, getTasksByIds());
    }

    private List<Task> getTasksByIds() {
        List<Task> answeredTasks = new ArrayList<>();
        for (int taskIndex : mAnsweredTasks.keySet()) {
            answeredTasks.add(mTasks.get(taskIndex));
        }
        return answeredTasks;
    }

    ArrayList<Integer> getAnsweredTaskIndexes() {
        return new ArrayList<>(mAnsweredTasks.keySet());
    }

    ArrayList<Task> getAnsweredTasks() {
        return new ArrayList<Task>(mAnsweredTasks.values());
    }

    /*private void uploadImagesAndCheckAnswers(List<String> fileUris,
                                             int taskId,
                                             String textAnswer,
                                             String taskCode) {
        GameProvider.provideImageService()
                .uploadImages(fileUris)
                .doOnSubscribe(mView::showLoading)
                .compose(mLifecycleHandler.reload(R.id.upload_image_request))
                .subscribe((links) -> recognizeMode(new UserAnswer(taskId,
                                textAnswer, getStringLinks(links)), TASK_C, taskCode)
                        , throwable -> {
                            mView.handleError(throwable,
                                    () -> uploadImagesAndCheckAnswers(fileUris, taskId, textAnswer, taskCode));
                            mView.hideLoading();
                        });
    }*/

    private void uploadBitmapsAndCheckAnswers(List<Bitmap> bitmaps,
                                              String textAnswer,
                                              Task task) {
        if (mIsTrainingMode) {
            mView.stopTimer();
        }
        GameProvider.provideImageService()
                .uploadBitmaps(bitmaps, mView.getContext())
                .doOnSubscribe(mView::showLoading)
                .compose(mLifecycleHandler.reload(R.id.upload_image_request))
                .subscribe((links) -> {
                            if (mIsTrainingMode) {
                                mView.startTimer();
                            }
                            recognizeMode(new UserAnswer(task.getId(),
                                    textAnswer, getStringLinks(links)), TASK_C, task);
                        }
                        , throwable -> {
                            mView.handleError(throwable,
                                    () -> uploadBitmapsAndCheckAnswers(bitmaps,
                                            textAnswer, task));
                            mView.hideLoading();
                        });
    }

    private List<UserAnswer> setAnswer(UserAnswer answer) {
        mAnswers.put(answer.getId(), answer);
        return convertAnswersToList();
    }

    private List<UserAnswer> convertAnswersToList() {
        List<UserAnswer> answerList = new ArrayList<>();
        for (int i = 0; i < mAnswers.size(); i++) {
            answerList.add(mAnswers.valueAt(i));
        }
        return answerList;
    }

    private void convertAnswersToSparse(List<UserAnswer> answers) {
        for (UserAnswer answer : answers) {
            mAnswers.put(answer.getId(), answer);
        }
    }

    private void sendAnswer(List<UserAnswer> userAnswers, TaskType type, @NonNull Task task) {
        GameProvider.provideGameRepository()
                .checkUserAnswers(new CheckAnswersForm(FREE, userAnswers))
                .doOnTerminate(mView::hideLoading)
                .compose(mLifecycleHandler.reload(R.id.check_answers_request))
                .subscribe(rightAnswers -> {
                    RightAnswer rightAnswer = rightAnswers.get(0);
                    createAnswerForm(task, type,
                            rightAnswer, task.getCode(), userAnswers.get(0));
                    mView.clearFields();
                }, throwable -> mView.handleError(throwable, ()
                        -> sendAnswer(userAnswers, type, task)));
    }

    private void createAnswerForm(@NonNull Task task,
                                  @NonNull TaskType type,
                                  @NonNull RightAnswer rightAnswer,
                                  @NonNull String code,
                                  @NonNull UserAnswer userAnswer) {
        int taskIndex = mTasks.indexOf(task);
        if (type == TASK_C) {
            mTasks.get(taskIndex).setAnsweredTask(true);
            saveTaskIndex(taskIndex, task);
            mView.showAnswerMessage(new TaskAnswerForm(code, userAnswer.getAnswer(), C_TASK,
                    String.valueOf(mAttachmentsHelper.getAttachmentSize())));
        } else {
            createAnswerFormForTaskB(task, rightAnswer, code, userAnswer);
        }
    }

    private void createAnswerFormForTaskB(@NonNull Task task,
                                          RightAnswer rightAnswer,
                                          String code,
                                          UserAnswer userAnswer) {
        AnswerType answerType;
        int taskIndex = mTasks.indexOf(task);
        if (userAnswer.getAnswer().equals(rightAnswer.getCorrectAnswer())
                && rightAnswer.getPoints() != 0) {
            answerType = RIGHT;
            mTasks.get(taskIndex).setPoints(rightAnswer.getPoints());
            saveTaskIndex(taskIndex, task);
        } else {
            answerType = WRONG;
        }
        mView.showAnswerMessage(new TaskAnswerForm(code, userAnswer.getAnswer(), answerType,
                String.valueOf(rightAnswer.getPoints()),
                String.valueOf(rightAnswer.getMaxPoints()))
        );
    }

    private List<String> getStringLinks(List<PictureLink> pictureLinks) {
        List<String> stringLinks = new ArrayList<>();
        for (PictureLink link : pictureLinks) {
            stringLinks.add(link.getPicture());
        }
        return stringLinks;
    }

   /* private String getThemeTitle(int themeId){
        GameProvider.provideThemesService()
                .getThemeFromDBById(themeId)
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .subscribe();
    }*/

}
