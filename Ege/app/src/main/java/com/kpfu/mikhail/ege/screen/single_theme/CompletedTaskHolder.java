package com.kpfu.mikhail.ege.screen.single_theme;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.widget.textviews.RobotoMediumTextView;
import com.kpfu.mikhail.ege.widget.textviews.RobotoRegularTextView;

import butterknife.BindString;
import butterknife.BindView;

class CompletedTaskHolder extends TaskHolder {

    @BindString(R.string.task_completed_points_format) String mPointsFormat;

    @BindView(R.id.tv_points) RobotoRegularTextView mPointsValue;

    @BindView(R.id.challenge_title) RobotoMediumTextView mTaskTitle;

    @BindView(R.id.subject_name) RobotoRegularTextView mSubjectName;

    @BindView(R.id.iv_completed_challenge) ImageView mCompletedChallengeImage;

    CompletedTaskHolder(@NonNull View itemView,
                        @NonNull String themeTitle,
                        @NonNull SingleThemeAdapter.TaskCallback taskCallback,
                        boolean isTrainingMode) {
        super(itemView, themeTitle, taskCallback, isTrainingMode);
    }

    public void bind(@NonNull Task task) {
        mTaskTitle.setText(task.getCode());
        mSubjectName.setText(getThemeTitle());
        mPointsValue.setText(String.format(mPointsFormat,
                String.valueOf(task.getPoints()), String.valueOf(task.getMaxPoints())));
        mCompletedChallengeImage.setImageResource(R.drawable.completed_study_challenge);
        setOnItemClickListener(task.getTopicId(), task);
    }

}
