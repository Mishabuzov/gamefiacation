package com.kpfu.mikhail.ege.utils.logger;

/**
 * @author Orhan Obut
 */
public enum LogLevel {

    /**
     * Prints all logs
     */
    FULL,

    /**
     * No log will be printed
     */
    NONE
}
