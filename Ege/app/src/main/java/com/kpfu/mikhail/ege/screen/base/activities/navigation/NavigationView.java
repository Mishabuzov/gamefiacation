package com.kpfu.mikhail.ege.screen.base.activities.navigation;

import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleActivityWithFragmentView;

interface NavigationView extends SingleActivityWithFragmentView {

    void setProfileFragment();

    void setRatingFragment();

    void setTrainingFragment();

    void hideNetworkErrorLayout();

    void hideProfileToolbarUpgrade();

}
