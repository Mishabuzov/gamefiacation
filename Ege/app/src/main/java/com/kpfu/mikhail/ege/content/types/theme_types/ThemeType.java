package com.kpfu.mikhail.ege.content.types.theme_types;

public class ThemeType {

    private ThemeTypeTitle title;

    private int solved;

    private int all;

    public ThemeType() {
    }

    public ThemeType(ThemeTypeTitle title) {
        this.title = title;
    }

    public ThemeTypeTitle getTitle() {
        return title;
    }

    public void setTitle(ThemeTypeTitle title) {
        this.title = title;
    }

    public int getSolved() {
        return solved;
    }

    public void setSolved(int solved) {
        this.solved = solved;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
