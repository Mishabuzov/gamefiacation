package com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;

import com.kpfu.mikhail.ege.screen.base.activities.base_activity.BaseActivityView;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;
import com.kpfu.mikhail.ege.utils.Function;

public interface SingleActivityWithFragmentView extends BaseActivityView {

    void setFragment(@NonNull BaseFragment fragment);

    void setToolbarBehavior(LinearLayoutManager llm, int size);

    void setToolbarBehavior(int scrollHeight,
                            int scrollPaddingTop,
                            int scrollPaddingBottom,
                            int childHeight);

    void turnOffToolbarScrolling();

    void turnOnToolbarScrolling();

    void setNetworkErrorScreen(Function reloadFunction);

}
