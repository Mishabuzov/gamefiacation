package com.kpfu.mikhail.ege.screen.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.screen.base.activities.navigation.NavigationActivity;
import com.kpfu.mikhail.ege.screen.base.fragments.base_login.BaseLoginFragment;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterFragment extends BaseLoginFragment implements RegisterView {

    @BindString(R.string.register_request_error_message) String mRegisterErrorMessage;

    @BindString(R.string.register_request_success_message) String mRegisterSuccessMessage;

    @BindString(R.string.login_error_edit_text) String mErrorLogin;

    @BindString(R.string.password_error_edit_text) String mErrorPassword;

    @BindString(R.string.name_error_edit_text) String mErrorName;

    @BindString(R.string.surname_error_edit_text) String mErrorSurname;

    @BindString(R.string.email_error_edit_text) String mErrorEmail;

    @BindString(R.string.content_login_btn_text_hide) String mHideButtonText;

    @BindString(R.string.content_login_btn_text_show) String mShowButtonText;

//    @BindView(R.id.add_user_image) ImageView mAddUserImage;

    @BindView(R.id.register_edit_name) EditText mEditName;

    @BindView(R.id.register_name) TextInputLayout mNameInputLayout;

    @BindView(R.id.register_edit_surname) EditText mEditSurname;

    @BindView(R.id.register_surname) TextInputLayout mSurnameInputLayout;

    @BindView(R.id.register_edit_email) EditText mEditEmail;

    @BindView(R.id.register_email) TextInputLayout mEmailInputLayout;

    @BindView(R.id.register_edit_login) EditText mEditLogin;

    @BindView(R.id.register_login) TextInputLayout mLoginInputLayout;

    @BindView(R.id.edittext_password) EditText mEditPassword;

    @BindView(R.id.password_text_input_layout) TextInputLayout mPasswordInputLayout;

    @BindView(R.id.finish_button) Button mRegisterButton;

    @BindView(R.id.show_pswd_button) Button mShowPasswordButton;

    @BindView(R.id.register_main_scroll) NestedScrollView mMainScroll;

    private RegisterPresenter mPresenter;

    @OnClick(R.id.show_pswd_button)
    public void onClickShowPasswordButton() {
        mPresenter.defineInputType();
    }

    @OnClick(R.id.finish_button)
    public void onClickRegisterButton() {
        mPresenter.checkFieldsAndTryRegister(
                String.valueOf(mEditLogin.getText()).trim(),
                String.valueOf(mEditPassword.getText()).trim(),
                String.valueOf(mEditName.getText()).trim(),
                String.valueOf(mEditSurname.getText()).trim(),
                String.valueOf(mEditEmail.getText()).trim());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);
        initFragmentElements();
        return view;
    }

    private void initFragmentElements() {
        mPresenter = new RegisterPresenter(getLifecycleHandler(), this);
        mRegisterButton.setEnabled(false);
        setTextWatcher(mEditLogin);
        setTextWatcher(mEditPassword);
        setTextWatcher(mEditEmail);
        setTextWatcher(mEditName);
        setTextWatcher(mEditSurname);
    }

    private void setTextWatcher(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mPresenter.tryEnableAndDisableLoginButton(
                        String.valueOf(mEditLogin.getText()).trim(),
                        String.valueOf(mEditPassword.getText()).trim(),
                        String.valueOf(mEditName.getText()).trim(),
                        String.valueOf(mEditSurname.getText()).trim(),
                        String.valueOf(mEditEmail.getText()).trim()
                );
            }
        });
    }

    @Override
    public void showPassword() {
        super.showPassword(mEditPassword, mShowPasswordButton, mHideButtonText);
    }

    @Override
    public void hidePassword() {
        super.hidePassword(mEditPassword, mShowPasswordButton, mShowButtonText);
    }

    @Override
    public void changeStateOfLoginButton(boolean state) {
        mRegisterButton.setEnabled(state);
    }

    @Override
    public void showErrorInLoginField() {
        showErrorInInputLayout(mLoginInputLayout, mErrorLogin);
    }

    @Override
    public void hideErrorInLoginField() {
        hideErrorFromInputLayout(mLoginInputLayout);
    }

    @Override
    public void showErrorInPasswordField() {
        showErrorInInputLayout(mPasswordInputLayout, mErrorPassword);
    }

    @Override
    public void hideErrorInPasswordField() {
        hideErrorFromInputLayout(mPasswordInputLayout);
    }

    @Override
    public void showErrorInEmailField() {
        showErrorInInputLayout(mEmailInputLayout, mErrorEmail);
    }

    @Override
    public void hideErrorInEmailField() {
        hideErrorFromInputLayout(mEmailInputLayout);
    }

    @Override
    public void showErrorInNameField() {
        showErrorInInputLayout(mNameInputLayout, mErrorName);
    }

    @Override
    public void hideErrorInNameField() {
        hideErrorFromInputLayout(mNameInputLayout);
    }

    @Override
    public void showErrorInSurnameField() {
        showErrorInInputLayout(mSurnameInputLayout, mErrorSurname);
    }

    @Override
    public void hideErrorInSurnameField() {
        hideErrorFromInputLayout(mSurnameInputLayout);
    }

    @Override
    public void openThemesScreen() {
        showToastMessage(mRegisterSuccessMessage);
        startThemeScreen();
    }

    private void startThemeScreen() {
        Intent intent = new Intent(getActivity(), NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected NestedScrollView getMainScroll() {
        return mMainScroll;
    }

}
