package com.kpfu.mikhail.ege.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_scrolling.BaseScrollingFragment;
import com.kpfu.mikhail.ege.screen.profile.ProfileFragment;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.view.ViewGroup.MarginLayoutParams;
import static com.kpfu.mikhail.ege.utils.Constants.MAX_ATTACHMENTS_SIZE;

public class AttachmentsHelper implements ImagePickerCallback {

    private Activity mActivity;
    private CameraImagePicker mCameraImagePicker;
    private BaseScrollingFragment mFragment;
    private ImagePicker mImagePicker;
    private LinearLayout mImageScrollView;
    private List<String> mImageAttachments;
    private String mOutputImagePath;
    private View mMarginsView;
    private int mMarginValue;
    private boolean mIsNotFirstAttachment = false;
    private int mAttachmentsPadding;
    private boolean mIsAvatarPicking = false;
    private ImageView mAvatarIv;
    private CompressImageUtils mImageUtils;

    private Map<String, Bitmap> mCompressedImages;

    public AttachmentsHelper(BaseScrollingFragment fragment,
                             LinearLayout imageScrollView,
                             View marginsView,
                             int attachmentsPadding,
                             int marginValue) {
        mFragment = fragment;
        mActivity = fragment.getActivity();
        mImageScrollView = imageScrollView;
        mMarginsView = marginsView;
        mMarginValue = marginValue;
        mAttachmentsPadding = attachmentsPadding;
        mImageAttachments = new ArrayList<>();
        mImageUtils = new CompressImageUtils(mActivity);
        mCompressedImages = new HashMap<>();
    }

    public AttachmentsHelper(@NonNull BaseScrollingFragment fragment,
                             @NonNull ImageView avatarIv) {
        mFragment = fragment;
        mActivity = fragment.getActivity();
        mIsAvatarPicking = true;
        mAvatarIv = avatarIv;
        mImageUtils = new CompressImageUtils(mActivity);
        mImageAttachments = new ArrayList<>();
    }

    public void showAddPhotoDialogAndTryAddPhoto(@StringRes int title) {
        new MaterialDialog.Builder(mActivity)
                .title(title)
                .items(R.array.add_photo_dialog_list_items)
                .titleColor(ContextCompat.getColor(mActivity, R.color.selected_color))
                .itemsCallback(new MaterialDialog.ListCallback() {
                    final int ITEM_GALLERY = 0;
                    final int ITEM_CAMERA = 1;

                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        switch (which) {
                            case ITEM_GALLERY:
                                requestReadExternalStoragePermissionAndMakeAction(AttachmentsHelper.this::initImagePicker);
                                break;
                            case ITEM_CAMERA:
                                requestCameraPermissionAndMakeAction(AttachmentsHelper.this::initCameraImagePicker);
                                break;
                        }
                    }
                })
                .show();
    }

    private void initCameraImagePicker() {
        mCameraImagePicker = new CameraImagePicker(mFragment);
        mCameraImagePicker.setImagePickerCallback(this);
        mOutputImagePath = mCameraImagePicker.pickImage();
    }

    private void initImagePicker() {
        mImagePicker = new ImagePicker(mFragment);
        mImagePicker.setImagePickerCallback(this);
        if (!mIsAvatarPicking) {
            mImagePicker.allowMultiple();
        }
        mImagePicker.pickImage();
    }

    private void processAvatarPicking(String queryPath) {
        String path = FileUtils.getPath(mActivity, Uri.parse(queryPath));
        Bitmap bitmap = null;
        try {
            bitmap = mImageUtils.compressImage(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (mImageAttachments.isEmpty()) {
            mImageAttachments.add(path);
        } else {
            mImageAttachments.set(0, path);
        }
        ((ProfileFragment) mFragment).uploadAvatar(bitmap);
    }

    private void addImage(String queryPath) {
        LayoutInflater inflater = mActivity.getLayoutInflater();
        View v = inflater.inflate(R.layout.add_photo_layout, mImageScrollView, false);
        configMarginsInAttachments(v);
        RoundedImageView iv = (RoundedImageView) v.findViewById(R.id.loading_image);
        ImageView ivDelete = (ImageView) v.findViewById(R.id.delete_button);
        String path = FileUtils.getPath(mActivity, Uri.parse(queryPath));
        ivDelete.setOnClickListener(view -> {
            mImageScrollView.removeView(v);
            mImageAttachments.remove(path);
            mCompressedImages.remove(path);
            checkAndAddMarginsInSeparator();
        });
        try {
            Bitmap compressedImage = mImageUtils.compressImage(path);
            mCompressedImages.put(path, compressedImage);
            iv.setImageBitmap(compressedImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
        checkAndRemoveMarginsInSeparator();
        mImageScrollView.addView(v);
        mImageAttachments.add(path);
        mIsNotFirstAttachment = true;
    }

    private void configMarginsInAttachments(View attachmentLayout) {
        if (mIsNotFirstAttachment) {
            MarginLayoutParams params = (MarginLayoutParams) attachmentLayout.getLayoutParams();
            params.setMargins(0, mAttachmentsPadding, 0, mAttachmentsPadding);
            attachmentLayout.setLayoutParams(params);
        } else {
            mFragment.configToolbarBehavior();
        }
    }

    private void checkAndRemoveMarginsInSeparator() {
        if (getAttachmentSize() == 0) {
            MarginLayoutParams params = (MarginLayoutParams) mMarginsView.getLayoutParams();
            params.setMargins(0, 0, 0, mMarginValue);
            mMarginsView.setLayoutParams(params);
            mFragment.configToolbarBehavior();
        }
    }

    private void checkAndAddMarginsInSeparator() {
        if (getAttachmentSize() == 0) {
            MarginLayoutParams params = (MarginLayoutParams) mMarginsView.getLayoutParams();
            params.setMargins(0, mMarginValue, 0, mMarginValue);
            mMarginsView.setLayoutParams(params);
            mIsNotFirstAttachment = false;
        }
    }

    public void cleanAttachments() {
        if (mImageAttachments != null) {
            mImageAttachments.clear();
            mImageScrollView.removeAllViews();
            checkAndAddMarginsInSeparator();
        }
    }

    public List<String> getPhotoLinks() {
        return mImageAttachments;
    }

    public List<Bitmap> getCompressedImages() {
        return new ArrayList<>(mCompressedImages.values());
    }

    private void processGalleryImagePickerActivityResult(Intent data) {
        if (mImagePicker == null) {
            initImagePicker();
        }
        mImagePicker.submit(data);
    }

    private void processCameraImagePickerActivityResult(Intent data) {
        if (mCameraImagePicker == null) {
            mCameraImagePicker = new CameraImagePicker(mFragment);
            mCameraImagePicker.reinitialize(mOutputImagePath);
            mCameraImagePicker.setImagePickerCallback(this);
        }
        mCameraImagePicker.submit(data);
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        if (images != null && !images.isEmpty()) {
            if (mIsAvatarPicking) {
                processAvatarPicking(images.get(0).getQueryUri());
            } else {
                processTaskImagesPicking(images);
            }
        }
    }

    private void processTaskImagesPicking(@NonNull List<ChosenImage> images) {
        for (ChosenImage image : images) {
            if (getAttachmentSize() < MAX_ATTACHMENTS_SIZE) {
                addImage(image.getQueryUri());
                mFragment.scrollDown();
            } else {
                UiUtils.showToast(R.string.too_many_content_message, mActivity);
            }
        }
    }

    public int getAttachmentSize() {
        return mImageAttachments.size();
    }

    @Override
    public void onError(String s) {

    }

    private void requestReadExternalStoragePermissionAndMakeAction(Function function) {
        if (Dexter.isRequestOngoing()) return;
        Dexter.checkPermission(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                function.action();
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                //
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                           PermissionToken token) {
                PermissionsUtils.showPermissionRationale(token, mActivity,
                        R.string.attachment_permission_dialog_title_read_file,
                        R.string.attachment_permission_dialog_content_read_file);
            }
        }, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    private void requestCameraPermissionAndMakeAction(Function function) {
        if (Dexter.isRequestOngoing()) return;
        List<String> grantedPermissions = new ArrayList<>();
        Dexter.checkPermissions(new MultiplePermissionsListener() {
                                    @Override
                                    public void onPermissionsChecked(MultiplePermissionsReport report) {
//                                        List<PermissionGrantedResponse> grantedPermissions = report.getGrantedPermissionResponses();
                                        for (PermissionGrantedResponse response : report.getGrantedPermissionResponses()) {
                                            if (!grantedPermissions.contains(response.getPermissionName())) {
                                                grantedPermissions.add(response.getPermissionName());
                                            }
                                        }
                                        if (grantedPermissions.contains(Manifest.permission.READ_EXTERNAL_STORAGE)
                                                && grantedPermissions.contains(Manifest.permission.CAMERA)) {
                                        function.action();
                                        } else {
                                        UiUtils.showToast(R.string.attachment_camera_permissions_denied, mActivity);
                                        }
                                    }

                                    @Override
                                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,
                                                                                   PermissionToken token) {
                                        PermissionsUtils.showPermissionRationale(token, mActivity,
                                                R.string.attachment_permission_dialog_title_camera,
                                                R.string.attachment_permission_dialog_content_camera);
                                    }
                                }, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    public void processMediaResult(int requestCode, Intent data) {
        if (requestCode == Picker.PICK_IMAGE_DEVICE) {
            processGalleryImagePickerActivityResult(data);
        } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
            processCameraImagePickerActivityResult(data);
        }
    }

}
