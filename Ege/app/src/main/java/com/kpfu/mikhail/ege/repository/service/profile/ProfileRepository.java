package com.kpfu.mikhail.ege.repository.service.profile;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.content.Profile;
import com.kpfu.mikhail.ege.content.RatingItem;
import com.kpfu.mikhail.ege.content.School;
import com.kpfu.mikhail.ege.content.Teacher;
import com.kpfu.mikhail.ege.content.forms.AvatarContainer;
import com.kpfu.mikhail.ege.content.forms.SchoolInfo;

import java.util.List;

import rx.Observable;

public interface ProfileRepository {

    @NonNull
    Observable<Profile> getUserProfile(int userId);

    @NonNull
    Observable<List<School>> getSchools();

    @NonNull
    Observable<List<Teacher>> getTeachers();

    @NonNull
    Observable<Void> updateSchoolInfo(int userId,
                                      @NonNull SchoolInfo schoolInfo);

    @NonNull
    Observable<Void> updateAvatar(int userId,
                                  @NonNull AvatarContainer container);

    @NonNull
    Observable<List<RatingItem>> getRating();

}
