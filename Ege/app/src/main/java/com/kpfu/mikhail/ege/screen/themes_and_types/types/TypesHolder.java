package com.kpfu.mikhail.ege.screen.themes_and_types.types;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.types.task_types.TaskType;
import com.kpfu.mikhail.ege.content.types.theme_types.ThemeType;
import com.kpfu.mikhail.ege.content.types.theme_types.ThemeTypeTask;
import com.kpfu.mikhail.ege.content.types.theme_types.ThemeTypeTitle;
import com.kpfu.mikhail.ege.widget.CircularProgressBar;
import com.kpfu.mikhail.ege.widget.textviews.RobotoMediumTextView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.kpfu.mikhail.ege.screen.themes_and_types.types.TypesAdapter.TypesCallback;

public class TypesHolder extends RecyclerView.ViewHolder {

    @BindString(R.string.themes_solved_tasks_format) String mSolvedTasksFormat;
    @BindView(R.id.iv_badge_background) CircleImageView mIvBadgeBackground;
    @BindView(R.id.tv_points) TextView mTvPoints;
    @BindView(R.id.cpb_progress) CircularProgressBar mCpbProgress;
    @BindView(R.id.tv_badge_title) RobotoMediumTextView mTvBadgeTitle;
    @BindView(R.id.type_theme_iv) ImageView mTypeView;

    private TypesCallback mCallback;

    private View mView;

    public TypesHolder(View itemView,
                       @NonNull TypesCallback callback) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mCallback = callback;
        mView = itemView;
    }

    public void bind(@NonNull ThemeType type) {
        mTvBadgeTitle.setText(type.getTitle().toString());
        int solved = type.getSolved();
        int all = type.getAll();
        /*mTvPoints.setText(String.format(mSolvedTasksFormat,
                String.valueOf(solved), String.valueOf(all)));*/
//        mCpbProgress.setProgress(solved / all * 100);
        switch (type.getTitle()) {
            case PART_B:
                mTypeView.setBackgroundResource(ThemeTypeTask.B_THEME.getMark());
                break;
            case PART_C:
                mTypeView.setBackgroundResource(ThemeTypeTask.C_THEME.getMark());
                break;
        }
        setOnItemClickListener(type.getTitle());
    }

    private void setOnItemClickListener(ThemeTypeTitle title) {
        mView.setOnClickListener(v -> {
            switch (title) {
                case PART_B:
                    mCallback.getThemesOfTheType(TaskType.TASK_B.getTaskType());
                    break;
                case PART_C:
                    mCallback.getThemesOfTheType(TaskType.TASK_C.getTaskType());
                    break;
            }
        });
    }

}
