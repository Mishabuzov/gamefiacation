package com.kpfu.mikhail.ege.screen.themes_and_types.themes;

import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_recycler.BaseRecyclerFragmentView;

import java.util.List;

interface ThemesView extends BaseRecyclerFragmentView {

    void showThemes(List<Theme> themes);

}
