package com.kpfu.mikhail.ege.repository.service.themes;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.api.ApiFactory;
import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.content.types.theme_types.ThemeLoadIndicator;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import ru.arturvasilov.rxloader.RxUtils;
import rx.Observable;

import static com.kpfu.mikhail.ege.utils.Constants.ID_FIELD;
import static com.kpfu.mikhail.ege.utils.Constants.TYPE_FIELD;

public class ThemesServiceImpl implements ThemesService {

    @NonNull
    public Observable<List<Theme>> getAllThemesFromDB() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Theme> themes = realm.where(Theme.class).findAll();
        return Observable.just(realm.copyFromRealm(themes));
    }

    @NonNull
    public Observable<List<Theme>> getAllThemesFromServer() {
        return ApiFactory.getGameService()
                .getThemes()
                .flatMap(this::updateThemesInDB);
    }

    @NonNull
    @Override
    public Observable<Theme> getThemeFromDBById(int themeId) {
        return Observable.just(Realm.getDefaultInstance()
                .where(Theme.class)
                .equalTo(ID_FIELD, themeId)
                .findFirst());
    }

    @NonNull
    @Override
    public Observable<List<Theme>> getThemesFromDbByIds(int[] ids) {
        List<Observable<Theme>> observables = new ArrayList<>();
        for (int id : ids) {
            Observable<Theme> observable = Observable.just(Realm.getDefaultInstance()
                    .where(Theme.class)
                    .equalTo(ID_FIELD, id)
                    .findFirst());
            observables.add(observable);
        }
        return Observable
                .combineLatest(observables, args -> {
                    List<Theme> themes = new ArrayList<>();
                    for (Object value : args) {
                        Theme theme = (Theme) value;
                        themes.add(theme);
                    }
                    return themes;
                })
                .compose(RxUtils.async());
    }

    @NonNull
    private Observable<List<Theme>> updateThemesInDB(List<Theme> themes) {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            realm.delete(Theme.class);
            realm.insert(themes);
            ThemeLoadIndicator indicator = new ThemeLoadIndicator();
            indicator.setThemeLoaded(false);
            for (Theme theme : themes) {
                indicator.setTopicId(theme.getId());
                realm.insert(indicator);
            }
        });
        return Observable.just(themes);
    }

    @Override
    public void updateTheme(@NonNull Theme theme) {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            realm.copyToRealmOrUpdate(theme);
        });
    }

    @NonNull
    @Override
    public Observable<List<Theme>> getThemesOfTheType(int themeType) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Theme> themes = realm.where(Theme.class)
                .equalTo(TYPE_FIELD, themeType)
                .findAll();
        return Observable.just(realm.copyFromRealm(themes));
    }

}
