package com.kpfu.mikhail.ege.screen.choosing_school;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.forms.SchoolInfo;
import com.kpfu.mikhail.ege.repository.GameProvider;

import ru.arturvasilov.rxloader.LifecycleHandler;

class SchoolPresenter {

    private LifecycleHandler mLifecycleHandler;
    private SchoolView mView;

    SchoolPresenter(@NonNull LifecycleHandler lifecycleHandler,
                    @NonNull SchoolView view) {
        mLifecycleHandler = lifecycleHandler;
        mView = view;
    }

    void getSchools() {
        GameProvider.provideProfileRepository()
                .getSchools()
                .doOnSubscribe(mView::showLoading)
                .compose(mLifecycleHandler.reload(R.id.choosing_school_request))
                .subscribe(mView::showSchools, throwable -> {
                    mView.handleError(throwable, this::getSchools);
                    mView.configToolbarBehavior(0);
                });
    }

    void updateSchoolInfo(int userId,
                          @NonNull SchoolInfo schoolInfo) {
        GameProvider.provideProfileRepository()
                .updateSchoolInfo(userId, schoolInfo)
                .doOnTerminate(mView::hideLoading)
                .compose(mLifecycleHandler.reload(R.id.update_school_info))
                .subscribe((aVoid) -> {
                    mView.finish();
                    mView.showToastMessage(R.string.profile_school_info_update);
                }, throwable -> {
                    mView.finish();
                    mView.showToastMessage("Не удалось обновить профиль: " + throwable.getMessage());
                });
    }

}
