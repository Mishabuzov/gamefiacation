package com.kpfu.mikhail.ege.utils;


import com.kpfu.mikhail.ege.App;
import com.kpfu.mikhail.ege.R;

import io.realm.Realm;

public class AndroidUtils {

    public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            App.sHandler.post(runnable);
        } else {
            App.sHandler.postDelayed(runnable, delay);
        }
    }

    public static void clearAllSavedData() {
        PreferenceUtils.clearPreference();
        Realm.getDefaultInstance().executeTransaction(
                realm -> realm.deleteAll());
    }

    public static int getPointsSubLineResId(int points) {
        switch (points % 10) {
            case 1:
                return R.string.content_challenge_point_sub_line_v1;
            case 2:
            case 3:
            case 4:
                return R.string.content_challenge_point_sub_line_v2;
            default:
                return R.string.content_challenge_point_sub_line_v3;
        }
    }

    /**
     * Ensures that an object reference passed as a parameter to the calling method is not null.
     *
     * @param reference an object reference
     * @return the non-null reference that was validated
     * @throws NullPointerException if {@code reference} is null
     */
    public static <T> T checkNotNull(T reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
        return reference;
    }
}
