package com.kpfu.mikhail.ege.screen.rating;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.repository.GameProvider;

import ru.arturvasilov.rxloader.LifecycleHandler;

public class RatingPresenter {

    @NonNull
    private final LifecycleHandler mLifecycleHandler;

    @NonNull
    private final RatingView mView;

    RatingPresenter(@NonNull LifecycleHandler lifecycleHandler,
                    @NonNull RatingView view) {
        mLifecycleHandler = lifecycleHandler;
        mView = view;
    }

    void getRating() {
        GameProvider.provideProfileRepository()
                .getRating()
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .compose(mLifecycleHandler.reload(R.id.rating_request))
                .subscribe((ratingItemList) -> {
                    mView.configToolbarBehavior(ratingItemList.size());
                    mView.showRating(ratingItemList);
                }, throwable -> mView.handleError(throwable,
                        this::getRating));
    }

}
