package com.kpfu.mikhail.ege.content.types.theme_types;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ThemeLoadIndicator extends RealmObject {

    @PrimaryKey
    private int topicId;

    private boolean isThemeLoaded;

    public ThemeLoadIndicator() {
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public boolean isThemeLoaded() {
        return isThemeLoaded;
    }

    public void setThemeLoaded(boolean themeLoaded) {
        isThemeLoaded = themeLoaded;
    }

}
