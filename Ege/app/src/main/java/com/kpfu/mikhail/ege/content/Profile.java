package com.kpfu.mikhail.ege.content;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Profile {

    private int id;

    private String name;

    private String avatar;

    private int points;

    @JsonProperty("rating_place")
    private int ratingPlace;

    @JsonProperty("use_point")
    private int bestTraining;

    private List<Badge> badges;

    public Profile() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getRatingPlace() {
        return ratingPlace;
    }

    public void setRatingPlace(int ratingPlace) {
        this.ratingPlace = ratingPlace;
    }

    public int getBestTraining() {
        return bestTraining;
    }

    public void setBestTraining(int bestTraining) {
        this.bestTraining = bestTraining;
    }

    public List<Badge> getBadges() {
        return badges;
    }

    public void setBadges(List<Badge> badges) {
        this.badges = badges;
    }

}
