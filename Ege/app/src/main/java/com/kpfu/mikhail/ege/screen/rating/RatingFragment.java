package com.kpfu.mikhail.ege.screen.rating;

import android.content.Intent;
import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.content.RatingItem;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_recycler.BaseRecyclerFragment;
import com.kpfu.mikhail.ege.screen.profile.ProfileActivity;

import java.util.List;

import static com.kpfu.mikhail.ege.utils.Constants.USER_ID;

public class RatingFragment extends BaseRecyclerFragment
        implements RatingView, RatingAdapter.RatingCallback {

    private RatingPresenter mPresenter;

    private RatingAdapter mAdapter;

    @Override
    protected void getArgs() {
    }

    @Override
    protected void doActions() {
        mPresenter.getRating();
    }

    @Override
    protected void initPresenter() {
        mPresenter = new RatingPresenter(getLifecycleHandler(), this);
    }

    @Override
    protected void installAdapter() {
        mAdapter = new RatingAdapter(this);
        mAdapter.attachToRecyclerView(getRecyclerView());
        getRecyclerView().setAdapter(mAdapter);
    }

    @Override
    public void showRating(@NonNull List<RatingItem> ratingItemList) {
        mAdapter.changeDataSet(ratingItemList);
    }

    @Override
    public void showUserProfile(int profileId) {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        intent.putExtra(USER_ID, profileId);
        startActivity(intent);
    }

}
