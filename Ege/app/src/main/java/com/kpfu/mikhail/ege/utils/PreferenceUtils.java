package com.kpfu.mikhail.ege.utils;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.content.Token;
import com.orhanobut.hawk.Hawk;


public final class PreferenceUtils {

    private static final String TOKEN_KEY = "token";

    private static final String TIME_GETTING_TOKEN_KEY = "time";

//    private static final String TRAINING_TIMER = "training_timer";

    private static final String USER_ID_KEY = "user_id";

    private PreferenceUtils() {
    }

    public static void saveToken(@NonNull Token token) {
        Hawk.put(TOKEN_KEY, String.format("%1$s %2$s", token.getTokenType(), token.getToken()));
        Hawk.put(TIME_GETTING_TOKEN_KEY, getCurrentUnixTime() + token.getExpires());
        saveUserId(token.getId());
    }

    @NonNull
    public static String getToken() {
        return Hawk.get(TOKEN_KEY, "");
    }

    private static long getTokenGettingTime() {
        return Hawk.get(TIME_GETTING_TOKEN_KEY, 0L);
    }

    /*public static void saveTrainingTimer(long timeUntilTrainingFinish) {
        Hawk.put(TRAINING_TIMER, timeUntilTrainingFinish + System.currentTimeMillis());
    }

    public static void clearTrainingTimer() {
        Hawk.remove(TRAINING_TIMER);
    }

    public static long getTimeUntilCurrentTrainingFinished() {
        return Hawk.get(TRAINING_TIMER, 0L) - System.currentTimeMillis();
    }*/

    public static int getUserId() {
        return Hawk.get(USER_ID_KEY);
    }

    private static void saveUserId(int userIdKey) {
        Hawk.put(USER_ID_KEY, userIdKey);
    }

    public static boolean isSignedIn() {
        return !getToken().isEmpty() && (getTokenGettingTime() - getCurrentUnixTime() > 0);
    }

    private static long getCurrentUnixTime() {
        return System.currentTimeMillis() / 1000L;
    }

    static void clearPreference() {
        Hawk.remove(TOKEN_KEY, TIME_GETTING_TOKEN_KEY, USER_ID_KEY);
    }

}
