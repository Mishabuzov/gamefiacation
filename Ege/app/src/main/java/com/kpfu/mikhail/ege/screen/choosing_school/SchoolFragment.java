package com.kpfu.mikhail.ege.screen.choosing_school;

import android.app.Activity;
import android.content.Intent;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.School;
import com.kpfu.mikhail.ege.content.forms.SchoolInfo;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_recycler.BaseRecyclerFragment;
import com.kpfu.mikhail.ege.screen.choosing_teacher.TeacherActivity;
import com.kpfu.mikhail.ege.utils.PreferenceUtils;

import java.util.List;

public class SchoolFragment extends BaseRecyclerFragment
        implements SchoolView, SchoolAdapter.SchoolCallback {

    public static final String EXTRA_TEACHER = "teacher_extra";

    private static final int REQUEST_CHOOSE_TEACHER = 1;

    private SchoolPresenter mPresenter;

    private SchoolActivityCallback mCallback;

    private int mSelectedSchoolId;

    private SchoolAdapter mAdapter;

    @Override
    protected void getArgs() {
    }

    @Override
    protected void doActions() {
        getRecyclerView().setBackgroundResource(R.color.card_background);
        mCallback = (SchoolActivityCallback) getActivity();
        mPresenter.getSchools();
    }

    @Override
    protected void initPresenter() {
        mPresenter = new SchoolPresenter(getLifecycleHandler(), this);
    }

    @Override
    protected void installAdapter() {
        mAdapter = new SchoolAdapter(this);
        mAdapter.attachToRecyclerView(getRecyclerView());
        getRecyclerView().setAdapter(mAdapter);
    }

    @Override
    public void showSchools(List<School> schools) {
        mAdapter.changeDataSet(schools);
        showScreenAndHideLoading();
    }

    void showTeacherChoosingScreen() {
        Intent intent = new Intent(getActivity(), TeacherActivity.class);
        startActivityForResult(intent, REQUEST_CHOOSE_TEACHER);
    }

    @Override
    public void selectSchool(int schoolId) {
        mSelectedSchoolId = schoolId;
        mCallback.showNextButton();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CHOOSE_TEACHER) {
                showLoading();
                int teacherId = data.getIntExtra(EXTRA_TEACHER, 0);
                mPresenter.updateSchoolInfo(PreferenceUtils.getUserId(),
                        new SchoolInfo(mSelectedSchoolId, teacherId));
            }
        }
    }

    interface SchoolActivityCallback {

        void showNextButton();

    }

}
