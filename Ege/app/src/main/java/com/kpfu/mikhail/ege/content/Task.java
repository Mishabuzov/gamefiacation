package com.kpfu.mikhail.ege.content;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Task extends RealmObject implements Parcelable {

    @PrimaryKey
    private int id;

    private String text;

    @JsonProperty("topic_id")
    private int topicId;

    private int type;

    private String code;

    @JsonProperty("max_points")
    private int maxPoints;

    @JsonProperty("user_points")
    private int points;

    @JsonIgnore
    private boolean mIsTrainingTask;

    @JsonIgnore
    private boolean mIsAnsweredTask;

    @JsonIgnore
    private boolean mIsTrainingRightTask;

    public Task() {
    }

    public Task(int id,
                @NonNull String text,
                int topicId,
                int type,
                @NonNull String code,
                int points,
                int maxPoints) {
        this.id = id;
        this.text = text;
        this.topicId = topicId;
        this.type = type;
        this.code = code;
        this.points = points;
        this.maxPoints = maxPoints;
    }

    @JsonIgnore
    public boolean isTrainingTask() {
        return mIsTrainingTask;
    }

    @JsonIgnore
    public void setTrainingTask(boolean trainingTask) {
        mIsTrainingTask = trainingTask;
    }

    @JsonIgnore
    public boolean isAnsweredTask() {
        return mIsAnsweredTask;
    }

    @JsonIgnore
    public void setAnsweredTask(boolean answeredTask) {
        mIsAnsweredTask = answeredTask;
    }

    @JsonIgnore
    public boolean isTrainingRightTask() {
        return mIsTrainingRightTask;
    }

    @JsonIgnore
    public void setTrainingRightTask(boolean trainingRightTask) {
        mIsTrainingRightTask = trainingRightTask;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.text);
        dest.writeInt(this.topicId);
        dest.writeInt(this.type);
        dest.writeString(this.code);
        dest.writeInt(this.maxPoints);
        dest.writeInt(this.points);
        dest.writeByte(this.mIsTrainingTask ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mIsAnsweredTask ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mIsTrainingRightTask ? (byte) 1 : (byte) 0);
    }

    protected Task(Parcel in) {
        this.id = in.readInt();
        this.text = in.readString();
        this.topicId = in.readInt();
        this.type = in.readInt();
        this.code = in.readString();
        this.maxPoints = in.readInt();
        this.points = in.readInt();
        this.mIsTrainingTask = in.readByte() != 0;
        this.mIsAnsweredTask = in.readByte() != 0;
        this.mIsTrainingRightTask = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Task> CREATOR = new Parcelable.Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel source) {
            return new Task(source);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };
}
