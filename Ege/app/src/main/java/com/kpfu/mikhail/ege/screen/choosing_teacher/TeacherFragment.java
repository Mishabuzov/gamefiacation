package com.kpfu.mikhail.ege.screen.choosing_teacher;

import android.app.Activity;
import android.content.Intent;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Teacher;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_recycler.BaseRecyclerFragment;

import java.util.List;

import static com.kpfu.mikhail.ege.screen.choosing_school.SchoolFragment.EXTRA_TEACHER;

public class TeacherFragment extends BaseRecyclerFragment
        implements TeacherView, TeacherAdapter.TeacherCallback {

    private TeacherPresenter mPresenter;

    private TeacherActivityCallback mCallback;

    private TeacherAdapter mAdapter;

    private int mTeacherId;

    @Override
    protected void getArgs() {
    }

    @Override
    protected void doActions() {
        getRecyclerView().setBackgroundResource(R.color.card_background);
        mCallback = (TeacherActivityCallback) getActivity();
        mPresenter.getTeachers();
    }

    @Override
    protected void initPresenter() {
        mPresenter = new TeacherPresenter(getLifecycleHandler(), this);
    }

    @Override
    protected void installAdapter() {
        mAdapter = new TeacherAdapter(this);
        mAdapter.attachToRecyclerView(getRecyclerView());
        getRecyclerView().setAdapter(mAdapter);
    }

    @Override
    public void showTeachers(List<Teacher> teachers) {
        mAdapter.changeDataSet(teachers);
        showScreenAndHideLoading();
    }

    @Override
    public void selectTeacher(int teacherId) {
        mTeacherId = teacherId;
        mCallback.showNextButton();
    }

    void finishChoosingTeacher() {
        Intent intent = new Intent();
        getActivity().setResult(Activity.RESULT_OK, intent);
        intent.putExtra(EXTRA_TEACHER, mTeacherId);
        finish();
    }

    interface TeacherActivityCallback {

        void showNextButton();

    }

}
