package com.kpfu.mikhail.ege.screen.themes_and_types.types;


import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.types.theme_types.ThemeType;
import com.kpfu.mikhail.ege.widget.BaseAdapter;

import java.util.ArrayList;

public class TypesAdapter extends BaseAdapter<TypesHolder, ThemeType> {

    private TypesCallback mCallback;

    public TypesAdapter(TypesCallback callback) {
        super(new ArrayList<>());
        mCallback = callback;
    }

    @Override
    public TypesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TypesHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.badges_list_item, parent, false),
                mCallback);
    }

    @Override
    public void onBindViewHolder(TypesHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        ThemeType type = getItem(position);
        holder.bind(type);
    }

    interface TypesCallback {

        void getThemesOfTheType(int themeType);

    }

}
