package com.kpfu.mikhail.ege.screen.base.fragments.base_fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleActivityWithFragmentView;
import com.kpfu.mikhail.ege.screen.base.fragments.error_fragment.ErrorFragment;
import com.kpfu.mikhail.ege.widget.progressbar.LoadingDialog;
import com.kpfu.mikhail.ege.widget.progressbar.LoadingView;

import ru.arturvasilov.rxloader.LifecycleHandler;
import ru.arturvasilov.rxloader.LoaderLifecycleHandler;

public abstract class BaseFragment extends ErrorFragment
        implements BaseFragmentView {

    private LoadingView mLoadingView;

    private LifecycleHandler mLifecycleHandler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLifecycleHandler = LoaderLifecycleHandler.create(getContext(),
                getActivity().getSupportLoaderManager());
        mLoadingView = LoadingDialog.view(getActivity()
                .getSupportFragmentManager());
    }

    @Override
    public void setToolbarTitle(String title) {
        if (title != null && !title.isEmpty()) {
            ((SingleActivityWithFragmentView) getActivity()).setToolbarTitle(title);
        }
    }

    public void setToolbarTitle(@StringRes int title) {
        if (title != 0) {
            ((SingleActivityWithFragmentView) getActivity()).setToolbarTitle(title);
        }
    }

    public LifecycleHandler getLifecycleHandler() {
        return mLifecycleHandler;
    }

    @Override
    public void showLoading() {
        mLoadingView.showLoading();
    }

    @Override
    public void hideLoading() {
        mLoadingView.hideLoading();
    }

  /*  @Override
    public void showErrorPage(String errorMessage) {
        //TODO: Exceptions handling
        UiUtils.showToast("Error! " + errorMessage, getActivity());
    }*/

    @Override
    public void finish() {
        getActivity().finish();
    }

   /* public interface ToolbarCallback{

        void setToolbarBehavior(RecyclerView recyclerView);

        void setToolbarBehavior(ScrollView scrollView);

    }*/

}
