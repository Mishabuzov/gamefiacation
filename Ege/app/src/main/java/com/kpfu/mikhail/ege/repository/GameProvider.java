package com.kpfu.mikhail.ege.repository;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.repository.service.images.ImageService;
import com.kpfu.mikhail.ege.repository.service.images.ImageServiceImpl;
import com.kpfu.mikhail.ege.repository.service.profile.ProfileRepository;
import com.kpfu.mikhail.ege.repository.service.profile.ProfileRepositoryImpl;
import com.kpfu.mikhail.ege.repository.service.tasks.TasksService;
import com.kpfu.mikhail.ege.repository.service.tasks.TasksServiceImpl;
import com.kpfu.mikhail.ege.repository.service.themes.ThemesService;
import com.kpfu.mikhail.ege.repository.service.themes.ThemesServiceImpl;

public final class GameProvider {

    private static GameRepository sGameRepository;

    private static TasksService sTasksService;

    private static ThemesService sThemesService;

    private static ImageService sImageService;

    private static ProfileRepository sProfileRepository;

    private GameProvider() {
    }

    @NonNull
    public static GameRepository provideGameRepository() {
        if (sGameRepository == null) {
            sGameRepository = new DefaultGameRepository();
        }
        return sGameRepository;
    }

    public static void setGameRepository(@NonNull GameRepository gameRepository) {
        sGameRepository = gameRepository;
    }

    @MainThread
    public static void init() {
        sGameRepository = new DefaultGameRepository();
    }

    public static TasksService provideTasksService() {
        if (sTasksService == null) {
            sTasksService = new TasksServiceImpl();
        }
        return sTasksService;
    }

    public static ThemesService provideThemesService() {
        if (sThemesService == null) {
            sThemesService = new ThemesServiceImpl();
        }
        return sThemesService;
    }

    public static ImageService provideImageService() {
        if (sImageService == null) {
            sImageService = new ImageServiceImpl();
        }
        return sImageService;
    }

    public static ProfileRepository provideProfileRepository() {
        if (sProfileRepository == null) {
            sProfileRepository = new ProfileRepositoryImpl();
        }
        return sProfileRepository;
    }

}
