package com.kpfu.mikhail.ege.screen.choosing_teacher;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.repository.GameProvider;

import ru.arturvasilov.rxloader.LifecycleHandler;

class TeacherPresenter {

    private LifecycleHandler mLifecycleHandler;
    private TeacherView mView;

    TeacherPresenter(@NonNull LifecycleHandler lifecycleHandler,
                     @NonNull TeacherView view) {
        mLifecycleHandler = lifecycleHandler;
        mView = view;
    }

    void getTeachers() {
        GameProvider.provideProfileRepository()
                .getTeachers()
                .doOnSubscribe(mView::showLoading)
                .compose(mLifecycleHandler.reload(R.id.choosing_school_request))
                .subscribe(mView::showTeachers, throwable -> {
                    mView.handleError(throwable, this::getTeachers);
                    mView.configToolbarBehavior(0);
                });

    }

}
