package com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_scrolling;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.view.ViewTreeObserver;

import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleActivityWithFragmentView;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;

public abstract class BaseScrollingFragment extends BaseFragment implements BaseScrollingFragmentView {

    private NestedScrollView mMainScroll;

    @Override
    public void setToolbarState(int scrollHeight,
                                int scrollPaddingTop,
                                int scrollPaddingBottom,
                                int childHeight) {
        ((SingleActivityWithFragmentView) getActivity()).setToolbarBehavior(scrollHeight,
                scrollPaddingTop, scrollPaddingBottom, childHeight);
    }

    protected abstract NestedScrollView getMainScroll();

    protected void setMainScroll() {
        mMainScroll = getMainScroll();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setMainScroll();
        configToolbarBehavior();
    }

    @Override
    public void configToolbarBehavior() {
        mMainScroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mMainScroll.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                BaseScrollingFragment.this.setToolbarState(
                        mMainScroll.getHeight(),
                        mMainScroll.getPaddingTop(),
                        mMainScroll.getPaddingBottom(),
                        mMainScroll.getChildAt(0).getHeight());

            }
        });
    }

    @Override
    public void scrollDown() {
        mMainScroll.post(() -> mMainScroll.fullScroll(View.FOCUS_DOWN));
    }

    @Override
    public void scrollUp() {
        mMainScroll.post(() -> mMainScroll.scrollTo(5, 10));
//                mMainScroll.fullScroll(View.FOCUS_UP));
    }

    @Override
    public void hideMainScrollView() {
        showLoading();
        if (mMainScroll != null)
        mMainScroll.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMainScrollView() {
        hideLoading();
        if (mMainScroll != null)
        mMainScroll.setVisibility(View.VISIBLE);
    }

}
