package com.kpfu.mikhail.ege.screen.login;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.forms.LoginForm;
import com.kpfu.mikhail.ege.repository.GameProvider;
import com.kpfu.mikhail.ege.utils.PreferenceUtils;

import ru.arturvasilov.rxloader.LifecycleHandler;

import static com.kpfu.mikhail.ege.utils.Constants.MIN_LOGIN_LENGTH;
import static com.kpfu.mikhail.ege.utils.Constants.MIN_PASSWORD_LENGTH;

class LoginPresenter {

    private final LifecycleHandler mLifecycleHandler;

    private final LoginView mLoginView;

    private boolean mIsPasswordHidden = true;

    LoginPresenter(@NonNull LifecycleHandler lifecycleHandler,
                   @NonNull LoginView loginView) {
        mLifecycleHandler = lifecycleHandler;
        mLoginView = loginView;
    }

    void tryEnableAndDisableLoginButton(String login, String password) {
        if (isAllFieldsAreValid(login, password)) {
            mLoginView.changeStateOfLoginButton(true);
        } else {
            mLoginView.changeStateOfLoginButton(false);
        }
    }

    private boolean isAllFieldsAreValid(String login, String password) {
        return login.length() >= MIN_LOGIN_LENGTH && password.length() >= MIN_PASSWORD_LENGTH;
    }

    private void login(String username, String password) {
        GameProvider.provideGameRepository()
                .login(new LoginForm(username, password))
                .doOnSubscribe(mLoginView::showLoading)
                .doOnTerminate(mLoginView::hideLoading)
                .compose(mLifecycleHandler.reload(R.id.login_request))
                .subscribe(token -> {
                    PreferenceUtils.saveToken(token);
                    mLoginView.openThemesScreen();
                    mLoginView.finish();
                }, throwable -> mLoginView.handleErrorWithToasts(throwable,
                        R.string.login_error_message));
    }

    void defineInputType() {
        if (mIsPasswordHidden) {
            mLoginView.showPassword();
            mIsPasswordHidden = false;
        } else {
            mLoginView.hidePassword();
            mIsPasswordHidden = true;
        }
    }

    void checkFieldsAndTryLogin(String login, String password) {
        if (isAllFieldsAreValid(login, password)) {
            login(login, password);
        } else {
            defineIncorrectField(login, password);
        }
    }

    private void defineIncorrectField(String login, String password) {
        if (login.length() < MIN_LOGIN_LENGTH) {
            mLoginView.showErrorInLoginField();
        } else {
            mLoginView.hideErrorInLoginField();
        }
        if (password.length() < MIN_PASSWORD_LENGTH) {
            mLoginView.showErrorInPasswordField();
        } else {
            mLoginView.hideErrorInPasswordField();
        }
    }
}
