package com.kpfu.mikhail.ege.repository;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.api.ApiFactory;
import com.kpfu.mikhail.ege.content.RightAnswer;
import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.content.Token;
import com.kpfu.mikhail.ege.content.forms.CheckAnswersForm;
import com.kpfu.mikhail.ege.content.forms.LoginForm;
import com.kpfu.mikhail.ege.content.forms.RegistrationForm;

import java.util.List;

import ru.arturvasilov.rxloader.RxUtils;
import rx.Observable;

import static com.kpfu.mikhail.ege.repository.GameProvider.provideTasksService;
import static com.kpfu.mikhail.ege.repository.GameProvider.provideThemesService;

class DefaultGameRepository implements GameRepository {

    @NonNull
    @Override
    public Observable<Token> login(LoginForm loginForm) {
        return ApiFactory.getGameService()
                .login(LoginForm.getGrantType(),
                        loginForm.getLogin(),
                        loginForm.getPassword())
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<Void> register(RegistrationForm registrationForm) {
        return ApiFactory.getGameService()
                .registerUser(registrationForm)
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<List<Theme>> getThemes() {
        return provideThemesService().getAllThemesFromDB()
                .filter(result -> !result.isEmpty())
                .switchIfEmpty(provideThemesService()
                        .getAllThemesFromServer())
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<List<Task>> getTasks(int topicId, int offset, int limit) {
        if (provideTasksService().isThemeTasksLoaded(topicId)) {
            return provideTasksService().getAllTasksFromTopicFromDB(topicId);
        } else {
            return provideTasksService().getTasksFromTopicFromServer(topicId, offset, limit);
        }
        /*return provideTasksService().getAllTasksFromTopicFromDB(topicId)
                .filter(result -> !result.isEmpty() && !(result.size() == 1))
                .switchIfEmpty(provideTasksService()
                        .getTasksFromTopicFromServer(topicId, offset, limit))
                .compose(RxUtils.async());*/
    }

    @NonNull
    @Override
    public Observable<List<RightAnswer>> checkUserAnswers(CheckAnswersForm answersForm) {
        return ApiFactory.getGameService()
                .checkUserAnswers(answersForm)
                .compose(RxUtils.async());
    }

}
