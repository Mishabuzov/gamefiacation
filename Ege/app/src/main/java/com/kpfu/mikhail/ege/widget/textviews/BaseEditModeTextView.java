package com.kpfu.mikhail.ege.widget.textviews;

import android.content.Context;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatTextView;

import com.kpfu.mikhail.ege.utils.FontUtils;

public abstract class BaseEditModeTextView extends AppCompatTextView {
    public BaseEditModeTextView(Context context) {
        super(context);
    }

    public BaseEditModeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseEditModeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void setTypeface(FontUtils.TypefaceType typefaceType) {
        if (!isInEditMode()) {
            super.setTypeface(FontUtils.getTypeface(getContext(), typefaceType));
        }
    }
}
