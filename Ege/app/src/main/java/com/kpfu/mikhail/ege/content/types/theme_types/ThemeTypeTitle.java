package com.kpfu.mikhail.ege.content.types.theme_types;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ThemeTypeTitle {

    PART_B("Часть B"),
    PART_C("Часть C");

    private final String mTitle;

    ThemeTypeTitle(String title) {
        mTitle = title;
    }

    @Override
    @JsonValue
    public String toString() {
        return mTitle;
    }

    public static ThemeTypeTitle getEnum(String value) {
        for (ThemeTypeTitle v : values()) {
            if (v.toString().equalsIgnoreCase(value)) {
                return v;
            }
        }
        return null;
    }
}
