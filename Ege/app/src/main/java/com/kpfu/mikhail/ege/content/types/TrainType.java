package com.kpfu.mikhail.ege.content.types;

import com.fasterxml.jackson.annotation.JsonValue;

public enum TrainType {

    FREE("free"),
    EXAM("exam");

    private String mValue;

    TrainType(String value) {
        mValue = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return mValue;
    }

    public static TrainType getEnum(String value) {
        for (TrainType v : values()) {
            if (v.toString().equalsIgnoreCase(value)) {
                return v;
            }
        }
        return null;
    }

}
