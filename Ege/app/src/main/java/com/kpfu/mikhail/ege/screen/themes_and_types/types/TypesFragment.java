package com.kpfu.mikhail.ege.screen.themes_and_types.types;

import android.content.Intent;

import com.kpfu.mikhail.ege.content.types.theme_types.ThemeType;
import com.kpfu.mikhail.ege.content.types.theme_types.ThemeTypeTitle;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_recycler.BaseRecyclerFragment;
import com.kpfu.mikhail.ege.screen.themes_and_types.themes.ThemesActivity;

import java.util.ArrayList;
import java.util.List;

import static com.kpfu.mikhail.ege.utils.Constants.THEME_TYPE;

public class TypesFragment extends BaseRecyclerFragment
        implements TypesAdapter.TypesCallback {

    private TypesAdapter mAdapter;

    private int mRecyclerItemsCount;

    @Override
    protected void getArgs() {
    }

    @Override
    protected void doActions() {
        List<ThemeType> types = new ArrayList<>();
        types.add(new ThemeType(ThemeTypeTitle.PART_B));
        types.add(new ThemeType(ThemeTypeTitle.PART_C));
        mRecyclerItemsCount = types.size();
        mAdapter.changeDataSet(types);
        showScreen();
    }

    public void setToolbarState() {
        configToolbarBehavior(mRecyclerItemsCount);
    }

    @Override
    protected void initPresenter() {
        //There is no hard logic -> no reasons in presenter
    }

    @Override
    protected void installAdapter() {
        mAdapter = new TypesAdapter(this);
        mAdapter.attachToRecyclerView(getRecyclerView());
        getRecyclerView().setAdapter(mAdapter);
    }

    @Override
    public void getThemesOfTheType(int themeType) {
        Intent intent = new Intent(getActivity(), ThemesActivity.class);
        intent.putExtra(THEME_TYPE, themeType);
        startActivity(intent);
    }

}
