package com.kpfu.mikhail.ege.screen.themes_and_types.themes;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.widget.BaseAdapter;

import java.util.ArrayList;

class ThemesAdapter extends BaseAdapter<ThemesHolder, Theme> {

    private final ThemesCallback mCallback;

    ThemesAdapter(ThemesCallback callback) {
        super(new ArrayList<>());
        mCallback = callback;
    }

    @Override
    public ThemesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ThemesHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.badges_list_item, parent, false), mCallback);
    }

    @Override
    public void onBindViewHolder(ThemesHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        Theme theme = getItem(position);
        holder.bind(theme);
    }

    interface ThemesCallback {

        void openSingleThemeScreen(Theme theme);

    }

}
