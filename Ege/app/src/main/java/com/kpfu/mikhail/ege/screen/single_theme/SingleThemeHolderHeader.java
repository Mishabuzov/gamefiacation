package com.kpfu.mikhail.ege.screen.single_theme;

import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.widget.CircularProgressBar;
import com.kpfu.mikhail.ege.widget.textviews.RobotoMediumTextView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

class SingleThemeHolderHeader extends RecyclerView.ViewHolder {

    @BindString(R.string.themes_solved_tasks_format) String mSolvedTasksFormat;

    @BindString(R.string.task_list_header_empty_subtitle) String mEmptySubtitle;

    @BindView(R.id.main_header_layout) LinearLayout mMainHeaderLayout;

    @BindView(R.id.iv_badge_background) CircleImageView mIvBadgeBackground;

    @BindView(R.id.tv_points) TextView mTvPoints;

    @BindView(R.id.cpb_progress) CircularProgressBar mCpbProgress;

    @BindView(R.id.tv_badge_title) RobotoMediumTextView mTvBadgeTitle;

    @BindView(R.id.tv_badge_content_description) TextView mTaskTitleTextView;

    private Theme mTheme;

    SingleThemeHolderHeader(View itemView, Theme theme) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mTheme = theme;
    }

    public void bind(final int itemsCount) {
        if (mTheme != null) {
            mTvBadgeTitle.setText(mTheme.getTitle());
            int solved = mTheme.getSolved();
            int all = mTheme.getAll();
            mTvPoints.setText(String.format(mSolvedTasksFormat,
                    String.valueOf(solved), String.valueOf(all)));
            mCpbProgress.setProgress(solved * 100 / all);
            if (itemsCount - 1 == 0) { // if there are no task items, except header
                setEmptyTaskTitle();
            }
        }
    }

    private void setEmptyTaskTitle() {
        mMainHeaderLayout.setLayoutParams(new LinearLayoutCompat.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mTaskTitleTextView.setText(mEmptySubtitle);
        LinearLayout.LayoutParams emptyTaskTitleParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        emptyTaskTitleParams.setMargins(0, 0, 0, 0);
        mTaskTitleTextView.setLayoutParams(emptyTaskTitleParams);
        mTaskTitleTextView.setGravity(Gravity.CENTER);
    }

}
