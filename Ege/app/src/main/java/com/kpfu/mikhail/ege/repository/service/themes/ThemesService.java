package com.kpfu.mikhail.ege.repository.service.themes;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.content.Theme;

import java.util.List;

import rx.Observable;

public interface ThemesService {

    @NonNull
    Observable<List<Theme>> getAllThemesFromDB();

    @NonNull
    Observable<List<Theme>> getAllThemesFromServer();

    @NonNull
    Observable<Theme> getThemeFromDBById(int themeId);

    void updateTheme(@NonNull Theme theme);

    @NonNull
    Observable<List<Theme>> getThemesFromDbByIds(int[] ids);

    @NonNull
    Observable<List<Theme>> getThemesOfTheType(int themeType);

}
