package com.kpfu.mikhail.ege.repository.service.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.api.ApiFactory;
import com.kpfu.mikhail.ege.content.PictureLink;
import com.kpfu.mikhail.ege.utils.FileUtils;
import com.kpfu.mikhail.ege.utils.logger.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ru.arturvasilov.rxloader.RxUtils;
import rx.Observable;

public class ImageServiceImpl implements ImageService {

    private static final String MEDIA_REQUEST_FORMAT = "multipart/form-data";

    @NonNull
    private MultipartBody.Part createFileFromUri(@NonNull String fileUri,
                                                 @NonNull String fileName) {
        File file = new File(fileUri);
        RequestBody requestFile = RequestBody
                .create(MediaType.parse(MEDIA_REQUEST_FORMAT), file);
        return MultipartBody.Part
                .createFormData(fileName, file.getName(), requestFile);
    }

    @NonNull
    private MultipartBody.Part createFileFromBitmap(@NonNull Context context,
                                                    @NonNull Bitmap bitmap,
                                                    @NonNull String fileName) {
        File filesDir = context.getFilesDir();
        File imageFile = new File(filesDir, fileName);
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (IOException e) {
            Logger.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        RequestBody requestFile = RequestBody
                .create(MediaType.parse(MEDIA_REQUEST_FORMAT), imageFile);
        return MultipartBody.Part
                .createFormData(fileName, imageFile.getName(), requestFile);
    }

    @NonNull
    private RequestBody createFilename(String fileName) {
        return RequestBody
                .create(MediaType.parse(MEDIA_REQUEST_FORMAT), fileName);
    }

    @NonNull  //Uploading images by paths
    @Override
    public Observable<List<PictureLink>> uploadImages(@NonNull List<String> fileUris) {
        List<Observable<PictureLink>> observables = new ArrayList<>();
        String fileName;
        for (int i = 0; i < fileUris.size(); i++) {
            fileName = FileUtils.createDefaultFileName(i);
            Observable<PictureLink> observable =
                    ApiFactory.getGameService().uploadPicture(createFilename(fileName),
                            createFileFromUri(fileUris.get(i), fileName));
            observables.add(observable);
        }
        return Observable
                .combineLatest(observables, args -> {
                    List<PictureLink> links = new ArrayList<>();
                    for (Object value : args) {
                        PictureLink link = (PictureLink) value;
                        links.add(link);
                    }
                    return links;
                })
                .compose(RxUtils.async());
    }

    @NonNull     //Uploading images by compressed bitmaps
    @Override
    public Observable<List<PictureLink>> uploadBitmaps(@NonNull List<Bitmap> bitmaps,
                                                       @NonNull Context context) {
        List<Observable<PictureLink>> observables = new ArrayList<>();
        String fileName;
        for (int i = 0; i < bitmaps.size(); i++) {
            fileName = FileUtils.createDefaultFileName(i);
            Observable<PictureLink> observable =
                    ApiFactory.getGameService().uploadPicture(createFilename(fileName),
                            createFileFromBitmap(context, bitmaps.get(i), fileName));
            observables.add(observable);
        }
        return Observable
                .combineLatest(observables, args -> {
                    List<PictureLink> links = new ArrayList<>();
                    for (Object value : args) {
                        PictureLink link = (PictureLink) value;
                        links.add(link);
                    }
                    return links;
                })
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<PictureLink> uploadAvatar(@NonNull Bitmap bitmapAvatar,
                                                @NonNull Context context) {
        String filename = FileUtils.createDefaultFileName(0);
        return ApiFactory.getGameService()
                .uploadAvatar(createFileFromBitmap(context, bitmapAvatar, filename))
                .compose(RxUtils.async());
    }

}
