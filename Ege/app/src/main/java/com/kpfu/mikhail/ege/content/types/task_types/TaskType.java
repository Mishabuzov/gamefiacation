package com.kpfu.mikhail.ege.content.types.task_types;


import android.support.annotation.DrawableRes;

import com.kpfu.mikhail.ege.R;

public enum TaskType {

    TASK_B(0, R.drawable.b_not_started_challenge_points_bg, R.drawable.b_challenge_points_bg, R.drawable.toolbar_task_b),

    TASK_C(1, R.drawable.c_not_started_challenge_points_bg, R.drawable.c_challenge_points_bg, R.drawable.toolbar_task_c);

    private final int mTaskType;

    private final int mNotStartedTaskImage;

    private final int mStartedTaskImage;

    private final int mToolbarBackgroundImage;

    TaskType(int taskType,
             @DrawableRes int notStartedTaskImage,
             @DrawableRes int startedTaskImage,
             @DrawableRes int toolbarBackgroundImage) {
        mTaskType = taskType;
        mNotStartedTaskImage = notStartedTaskImage;
        mStartedTaskImage = startedTaskImage;
        mToolbarBackgroundImage = toolbarBackgroundImage;
    }

    public int getTaskType() {
        return mTaskType;
    }

    @DrawableRes
    public int getNotStartedTaskImage() {
        return mNotStartedTaskImage;
    }

    @DrawableRes
    public int getStartedTaskImage() {
        return mStartedTaskImage;
    }

    @DrawableRes
    public int getToolbarBackgroundImage() {
        return mToolbarBackgroundImage;
    }

}
