package com.kpfu.mikhail.ege.screen.single_theme;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.screen.single_theme.SingleThemeAdapter.TaskCallback;
import com.kpfu.mikhail.ege.utils.UiUtils;
import com.kpfu.mikhail.ege.widget.textviews.RobotoBoldTextView;
import com.kpfu.mikhail.ege.widget.textviews.RobotoMediumTextView;
import com.kpfu.mikhail.ege.widget.textviews.RobotoRegularTextView;

import butterknife.BindView;

class InCompletedTaskHolder extends TaskHolder {

    @BindView(R.id.points_bg) RelativeLayout mPointsBg;

    @BindView(R.id.challenge_title) RobotoMediumTextView mTaskTitle;

    @BindView(R.id.subject_name) RobotoRegularTextView mSubjectName;

    @BindView(R.id.badge_bg) ImageView mBadgeBg;

    @BindView(R.id.points_value) RobotoBoldTextView mPointsValue;

    @BindView(R.id.points_sub_line) TextView mPointsNameLabel;

    InCompletedTaskHolder(@NonNull View itemView,
                          @NonNull String themeTitle,
                          @NonNull TaskCallback taskCallback,
                          boolean isTrainingMode) {
        super(itemView, themeTitle, taskCallback, isTrainingMode);
    }

    public void bind(@NonNull Task task) {
        UiUtils.fillTaskHeader(task, getThemeTitle(), mTaskTitle,
                mSubjectName, mPointsValue, mPointsBg, mPointsNameLabel);
        setOnItemClickListener(task.getTopicId(), task);
    }

}
