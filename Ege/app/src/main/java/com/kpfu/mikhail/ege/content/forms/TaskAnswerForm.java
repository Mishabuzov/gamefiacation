package com.kpfu.mikhail.ege.content.forms;

import com.kpfu.mikhail.ege.utils.Function;

import static com.kpfu.mikhail.ege.utils.DialogUtils.AnswerType;

public class TaskAnswerForm {

    private String mTaskCode;

    private String mTextAnswer;

    private String mAttachmentsCount;

    private AnswerType mAnswerType;

    private String mRightAnswer;

    private String mPoints;

    private String mMaxPoints;

    private Function mNextFunction;

    public TaskAnswerForm(String taskCode,
                          String textAnswer,
                          AnswerType answerType,
                          String attachmentsCount) {
        mTaskCode = taskCode;
        mTextAnswer = textAnswer;
        mAttachmentsCount = attachmentsCount;
        mAnswerType = answerType;
    }

    public TaskAnswerForm(String taskCode,
                          String textAnswer,
                          AnswerType answerType,
                          String points,
                          String maxPoints) {
        mTaskCode = taskCode;
        mTextAnswer = textAnswer;
        mAnswerType = answerType;
        mPoints = points;
        mMaxPoints = maxPoints;
    }

    public AnswerType getAnswerType() {
        return mAnswerType;
    }

    public void setAnswerType(AnswerType answerType) {
        mAnswerType = answerType;
    }

    public String getRightAnswer() {
        return mRightAnswer;
    }

    public void setRightAnswer(String rightAnswer) {
        mRightAnswer = rightAnswer;
    }

    public String getPoints() {
        return mPoints;
    }

    public void setPoints(String points) {
        mPoints = points;
    }

    public String getMaxPoints() {
        return mMaxPoints;
    }

    public void setMaxPoints(String maxPoints) {
        mMaxPoints = maxPoints;
    }

    public Function getNextFunction() {
        return mNextFunction;
    }

    public void setNextFunction(Function nextFunction) {
        mNextFunction = nextFunction;
    }

    public String getTaskCode() {
        return mTaskCode;
    }

    public void setTaskCode(String taskCode) {
        mTaskCode = taskCode;
    }

    public String getTextAnswer() {
        return mTextAnswer;
    }

    public void setTextAnswer(String textAnswer) {
        this.mTextAnswer = textAnswer;
    }

    public String getAttachmentsCount() {
        return mAttachmentsCount;
    }

    public void setAttachmentsCount(String attachmentsCount) {
        this.mAttachmentsCount = attachmentsCount;
    }

}
