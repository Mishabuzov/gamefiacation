package com.kpfu.mikhail.ege.repository;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.content.RightAnswer;
import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.content.Token;
import com.kpfu.mikhail.ege.content.forms.CheckAnswersForm;
import com.kpfu.mikhail.ege.content.forms.LoginForm;
import com.kpfu.mikhail.ege.content.forms.RegistrationForm;

import java.util.List;

import rx.Observable;

public interface GameRepository {

    @NonNull
    Observable<Token> login(LoginForm loginForm);

    @NonNull
    Observable<Void> register(RegistrationForm registrationForm);

    @NonNull
    Observable<List<Theme>> getThemes();

    @NonNull
    Observable<List<Task>> getTasks(int topicId, int offset, int limit);

    @NonNull
    Observable<List<RightAnswer>> checkUserAnswers(CheckAnswersForm answersForm);

}
