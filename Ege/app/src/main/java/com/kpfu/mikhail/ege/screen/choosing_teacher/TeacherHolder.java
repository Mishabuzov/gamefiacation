package com.kpfu.mikhail.ege.screen.choosing_teacher;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Teacher;
import com.kpfu.mikhail.ege.widget.textviews.RobotoBoldTextView;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TeacherHolder extends RecyclerView.ViewHolder {

    @BindColor(R.color.card_background) int mUnselectedColor;

    @BindColor(R.color.selected_color) int mSelectedColor;

    @BindView(R.id.name_tv) RobotoBoldTextView mNameTv;

    private final View mMainView;

    private View mSelectedView;

    private TeacherViewCallback mViewCallback;

    private TeacherAdapter.TeacherCallback mTeacherCallback;

    public TeacherHolder(View itemView,
                         View selectedView,
                         TeacherViewCallback viewCallback,
                         TeacherAdapter.TeacherCallback teacherCallback) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mMainView = itemView;
        mSelectedView = selectedView;
        mViewCallback = viewCallback;
        mTeacherCallback = teacherCallback;
    }

    void bind(@NonNull Teacher teacher) {
        mNameTv.setText(teacher.getName());
        mMainView.setOnClickListener(v -> {
            if (mSelectedView != null) {
                deselectView(mSelectedView);
            }
            selectView(v);
            mTeacherCallback.selectTeacher(teacher.getId());
        });
    }

    private void deselectView(View view) {
        view.setBackgroundColor(mUnselectedColor);
    }

    private void selectView(View view) {
        view.setBackgroundColor(mSelectedColor);
        mSelectedView = view;
        mViewCallback.setSelectedView(view);
    }

    interface TeacherViewCallback {

        void setSelectedView(View view);

    }

}
