package com.kpfu.mikhail.ege.screen.register;

import android.os.Bundle;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleFragmentActivity;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;

public class RegisterActivity extends SingleFragmentActivity {

    @Override
    protected BaseFragment installFragment() {
        return new RegisterFragment();
    }

    @Override
    protected Bundle attachFragmentArguments() {
        return null;
    }

    @Override
    protected void doCreatingActions() {
        setToolbarTitle(R.string.register_title);
    }

}
