package com.kpfu.mikhail.ege.content.forms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SchoolInfo {

    @JsonProperty("school_id")
    private int schoolId;

    @JsonProperty("teacher_id")
    private int teacherId;

    public SchoolInfo() {
    }

    public SchoolInfo(int schoolId, int teacherId) {
        this.schoolId = schoolId;
        this.teacherId = teacherId;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }
}
