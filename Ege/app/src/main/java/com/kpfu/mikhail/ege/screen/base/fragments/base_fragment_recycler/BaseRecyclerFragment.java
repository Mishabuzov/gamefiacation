package com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_recycler;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleActivityWithFragmentView;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;
import com.kpfu.mikhail.ege.widget.EmptyRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseRecyclerFragment extends BaseFragment implements BaseRecyclerFragmentView {

    @BindView(R.id.main_layout) RelativeLayout mMainLayout;

    @BindView(R.id.recyclerView) EmptyRecyclerView mRecyclerView;

    @BindView(R.id.empty) TextView mEmpty;

    private LinearLayoutManager mLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base_recycler, container, false);
        ButterKnife.bind(this, view);
        getArgs();
        initFragmentElements();
        initPresenter();
        doActions();
        return view;
    }

    @Override
    public void configToolbarBehavior(int recyclerItemsSize) {
        mRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mRecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ((SingleActivityWithFragmentView) getActivity()).setToolbarBehavior(mLayoutManager, recyclerItemsSize);
            }
        });
    }

    protected abstract void getArgs();

    protected abstract void doActions();

    protected abstract void initPresenter();

    private void initFragmentElements() {
        installAdapter();
        setupRecyclerView();

    }

    protected abstract void installAdapter();

    private void setupRecyclerView() {
        mLayoutManager = new LinearLayoutManager(mRecyclerView.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setEmptyView(mEmpty);
        mRecyclerView.setBackgroundResource(R.color.theme_primary);
    }

    public EmptyRecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    @Override
    public void showScreenAndHideLoading() {
        showScreen();
        hideLoading();
    }

    @Override
    public void hideScreenAndShowLoading() {
        hideScreen();
        showLoading();
    }

    @Override
    public void showScreen() {
        mMainLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideScreen() {
        mMainLayout.setVisibility(View.INVISIBLE);
    }

}