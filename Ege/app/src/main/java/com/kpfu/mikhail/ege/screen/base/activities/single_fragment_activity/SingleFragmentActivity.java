package com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.LayoutParams;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.screen.base.activities.base_activity.BaseActivity;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;
import com.kpfu.mikhail.ege.utils.Function;
import com.kpfu.mikhail.ege.utils.ToolbarUtils;
import com.kpfu.mikhail.ege.widget.textviews.RobotoBoldTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS;
import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL;
import static com.kpfu.mikhail.ege.utils.Constants.NONE_SCROLLING;

public abstract class SingleFragmentActivity extends BaseActivity
        implements SingleActivityWithFragmentView {

    @BindView(R.id.toolbar_image) ImageView mToolbarImage;

    @BindView(R.id.toolbar_title) RobotoBoldTextView mToolbarTitle;

    @BindView(R.id.toolbar) Toolbar mToolbar;

    @BindView(R.id.appBarLayout) AppBarLayout mAppBarLayout;

    @BindView(R.id.main_layout) RelativeLayout mMainLayout;

    @BindView(R.id.btn_reload) Button mBtnReload;

    @BindView(R.id.network_error_view) RelativeLayout mNetworkErrorLayout;

    @BindView(R.id.fragment_layout) FrameLayout mFragmentLayout;

    @BindView(R.id.tv_network_error_message) TextView mNetworkErrorTv;

    private SingleActivityWithFragmentPresenter mFragmentPresenter;

    private final Handler mDelayHandler = new Handler();

    private BaseFragment mFragment;

    @Override
    public void setFragment(@NonNull BaseFragment fragment) {
        mFragment = fragment;
        fragment.setArguments(attachFragmentArguments());
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fm.findFragmentById(R.id.fragment_layout) != null) {
            ft.replace(R.id.fragment_layout, fragment, FRAGMENT_TAG);
            ft.commit();
        } else {
            ft.add(R.id.fragment_layout, fragment, FRAGMENT_TAG);
            ft.commit();
        }
    }

    protected abstract Bundle attachFragmentArguments();

    protected abstract BaseFragment installFragment();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_fragment);
        ButterKnife.bind(this);
        mFragmentPresenter = new SingleActivityWithFragmentPresenter(this);
        setFragment(installFragment());
        setSupportActionBar(mToolbar);
        doCreatingActions();
    }

    protected void doCreatingActions() {
    }

    protected void setToolbarBackArrow() {
        View toolbarUpgrade = getLayoutInflater().inflate(R.layout.toolbar_back_arrow_upgrade, null);
        ToolbarUtils.setToolbarBackArrow(toolbarUpgrade, getToolbar(), this::finish);
    }

    @Override
    public void setToolbarBehavior(int scrollHeight,
                                   int scrollPaddingTop,
                                   int scrollPaddingBottom,
                                   int childHeight) {
        mFragmentPresenter.defineToolbarScrollingState(scrollHeight,
                scrollPaddingTop, scrollPaddingBottom, childHeight);
    }

    @Override
    public void setToolbarBehavior(LinearLayoutManager layoutManager, int size) {
        mFragmentPresenter.defineToolbarScrollingState(layoutManager.findLastCompletelyVisibleItemPosition(), size);
    }

    @Override
    public void turnOffToolbarScrolling() {
        //turn off scrolling
        setToolbarScrollingState(NONE_SCROLLING, null);
    }

    @Override
    public void turnOnToolbarScrolling() {
        //turn on scrolling
        setToolbarScrollingState(SCROLL_FLAG_SCROLL | SCROLL_FLAG_ENTER_ALWAYS,
                new AppBarLayout.Behavior());
    }

    private void setToolbarScrollingState(int scrollFlags, AppBarLayout.Behavior behavior) {
        LayoutParams toolbarLayoutParams = (LayoutParams) mToolbar.getLayoutParams();
        toolbarLayoutParams.setScrollFlags(scrollFlags);
        mToolbar.setLayoutParams(toolbarLayoutParams);

        CoordinatorLayout.LayoutParams appBarLayoutParams = (CoordinatorLayout.LayoutParams) mAppBarLayout.getLayoutParams();
        appBarLayoutParams.setBehavior(behavior);
        mAppBarLayout.setLayoutParams(appBarLayoutParams);
    }

    @Override
    public void setToolbarTitle(String title) {
        if (title != null && !title.isEmpty()) {
            mToolbarTitle.setText(title);
        }
    }

    @Override
    public void setToolbarTitle(@StringRes int title) {
        if (title != 0) {
            mToolbarTitle.setText(title);
        }
    }

    @Override
    public void setToolbarBackgroundImage(@DrawableRes int resId) {
        mToolbarImage.setImageResource(resId);
    }

    protected RelativeLayout getMainLayout() {
        return mMainLayout;
    }

    protected FrameLayout getFragmentLayout() {
        return mFragmentLayout;
    }

    protected RelativeLayout getNetworkErrorLayout() {
        return mNetworkErrorLayout;
    }

    protected Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public void setNetworkErrorScreen(Function reloadRequest) {
        mNetworkErrorLayout.setVisibility(View.VISIBLE);
        mFragmentLayout.setVisibility(View.GONE);
        mNetworkErrorTv.setText(R.string.network_error_message);
        mBtnReload.setOnClickListener(v -> {
            mFragmentLayout.setVisibility(View.VISIBLE);
            mNetworkErrorLayout.setVisibility(View.GONE);
            repeatRequestWithDelay(reloadRequest);
        });
    }

    /**
     * Repeating request with 1 sec delay;
     */
    private void repeatRequestWithDelay(Function request) {
        mFragment.showLoading();
        mDelayHandler.postDelayed(request::action, 1000);
    }

}
