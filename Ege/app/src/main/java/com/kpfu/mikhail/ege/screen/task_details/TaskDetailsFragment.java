package com.kpfu.mikhail.ege.screen.task_details;


import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.content.forms.TaskAnswerForm;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_scrolling.BaseScrollingFragment;
import com.kpfu.mikhail.ege.utils.AttachmentsHelper;
import com.kpfu.mikhail.ege.utils.DialogUtils;
import com.kpfu.mikhail.ege.utils.UiUtils;
import com.kpfu.mikhail.ege.widget.textviews.RobotoBoldTextView;
import com.kpfu.mikhail.ege.widget.textviews.RobotoMediumTextView;
import com.kpfu.mikhail.ege.widget.textviews.RobotoRegularTextView;

import java.util.ArrayList;

import butterknife.BindDimen;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.kpfu.mikhail.ege.content.types.task_types.TaskType.TASK_B;
import static com.kpfu.mikhail.ege.utils.Constants.IS_TRAINING_FINISHED;
import static com.kpfu.mikhail.ege.utils.Constants.IS_TRAINING_MODE;

public class TaskDetailsFragment extends BaseScrollingFragment implements TaskDetailsView {

    public static final String THEME_ID = "theme_id";

    public static final String THEME_TITLE = "theme_title";

    public static final String TASK_POSITION = "task_position";

    @BindDimen(R.dimen.margin_20) int mMarginValue;

    @BindDimen(R.dimen.padding_14) int mAttachmentsPaddingTopAndBottom;

    @BindString(R.string.task_details_your_answer) String mAnswerLabel;

    @BindString(R.string.alert_dialog_attachments_text_view) String mAttachmentsLabel;

    @BindString(R.string.task_answer_format) String mAnswerFormat;

    @BindView(R.id.task_description) TextView mTaskDescriptionTv;

    @BindView(R.id.points_value) RobotoBoldTextView mPointsValue;

    @BindView(R.id.points_bg) RelativeLayout mPointsBg;

    @BindView(R.id.challenge_title) RobotoMediumTextView mTaskTitle;

    @BindView(R.id.subject_name) RobotoRegularTextView mSubjectName;

    @BindView(R.id.edit_answer) EditText mEditAnswer;

    @BindView(R.id.add_image_button) ImageButton mAddImageButton;

    @BindView(R.id.images_scroll_view) LinearLayout mImagesScrollView;

    @BindView(R.id.margins_view) View mMarginsView;

    @BindView(R.id.send_button) Button mSendButton;

    @BindView(R.id.main_scroll) NestedScrollView mScrollView;

    @BindView(R.id.points_sub_line) TextView mPointsNameLabel;

    @BindView(R.id.result_layout) LinearLayout mResultLayout;

    @BindView(R.id.mark_layout) LinearLayout mMarkLayout;

    @BindView(R.id.task_answer) RobotoMediumTextView mTaskAnswer;

    @BindView(R.id.task_attachments_count) RobotoMediumTextView mTaskAttachmentsCount;

    @BindView(R.id.image_mark) ImageView mMarkImage;

    @BindView(R.id.text_mark) TextView mMarkText;

    @BindView(R.id.edit_answer_layout) CardView mEditAnswerLayout;

    @BindView(R.id.bottom_layout) LinearLayout mBottomLayout;

    @BindView(R.id.answer_margins_view) View mAnswerMarginsView;

    private int mThemeId;

    private String mThemeTitle;

    private TaskDetailsPresenter mPresenter;

    private int mTaskPosition;

    private AttachmentsHelper mAttachmentsHelper;

    private Task mTask;

    private boolean mIsTrainingMode;

    private boolean mIsTrainingFinished;

    @OnClick(R.id.send_button)
    public void onClickSendButton() {
        mPresenter.checkTextAnswerAndProcess(String.valueOf(mEditAnswer.getText()).trim(), mTask);
    }

    @OnClick(R.id.add_image_button)
    public void onClickAttachmentButton() {
        mPresenter.tryAddPhoto();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_details, container, false);
        ButterKnife.bind(this, view);
        setMainScroll();
        getArgs();
        initFragmentElements();
        mPresenter.getTasksAndShowOne(mTaskPosition);
        return view;
    }

    private void getArgs() {
        Bundle args = getArguments();
        if (args != null) {
            mThemeId = args.getInt(THEME_ID);
            mTaskPosition = args.getInt(TASK_POSITION);
            mThemeTitle = args.getString(THEME_TITLE);
            mIsTrainingMode = args.getBoolean(IS_TRAINING_MODE);
            mIsTrainingFinished = args.getBoolean(IS_TRAINING_FINISHED);
        }
    }

    private void initFragmentElements() {
        mAttachmentsHelper = new AttachmentsHelper(this, mImagesScrollView,
                mMarginsView, mAttachmentsPaddingTopAndBottom, mMarginValue);
        mPresenter = new TaskDetailsPresenter(getLifecycleHandler(), this,
                ((TaskCallback) getActivity()), mAttachmentsHelper,
                mIsTrainingMode, mIsTrainingFinished);
        if (!mIsTrainingMode) {
            mPresenter.setTopicId(mThemeId);
        }
        mPresenter.defineTrainingState();
    }

    @Override
    public void showNewTask(Task task) {
        mTask = task;
        UiUtils.fillTaskHeader(task, mThemeTitle, mTaskTitle,
                mSubjectName, mPointsValue, mPointsBg, mPointsNameLabel);
        setTextDescription(task.getText());
      /*  getActivity()
        setBackgroundSendButton(task.getType());*/
    }

    @Override
    public void clearFields() {
        mEditAnswer.setText("");
        mAttachmentsHelper.cleanAttachments();
    }

    @Override
    public void showAnswerMessage(TaskAnswerForm form) {
        form.setNextFunction(this::showNextTask);
        DialogUtils.createTaskAnswerDialog(getActivity(), form);
    }

    @Override
    public void showNextTask() {
        mPresenter.updateTaskInfo(++mTaskPosition);
    }

    @Override
    public void cleanAnswerField() {
        mEditAnswer.setText("");
    }

    @Override
    public void disableSendingAnswersUI() {
        mSendButton.setVisibility(View.GONE);
        mMarginsView.setVisibility(View.GONE);
        mEditAnswerLayout.setVisibility(View.GONE);
        mBottomLayout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        mAnswerMarginsView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showEmptyAnswerErrorOnTaskB() {
        showToastMessage(R.string.task_details_empty_answer_task_b_message);
    }

    @Override
    public void showEmptyAnswerErrorOnTaskC() {
        showToastMessage(R.string.task_details_empty_answer_task_c_message);
    }

    @Override
    public void showCurrentAnswer(String answer,
                                  int imagesCount,
                                  @NonNull Task currentTask) {
        if (!answer.isEmpty()) {
            String answerFullLabel = String.format(mAnswerFormat, mAnswerLabel, answer);
            mTaskAnswer.setText(answerFullLabel);
            mTaskAnswer.setVisibility(View.VISIBLE);
        }
        if (imagesCount != 0) {
            String attachmentsFullLabel = String.format(mAnswerFormat, mAttachmentsLabel,
                    String.valueOf(imagesCount));
            mTaskAttachmentsCount.setText(attachmentsFullLabel);
            mTaskAttachmentsCount.setVisibility(View.VISIBLE);
        } else {
            mTaskAttachmentsCount.setVisibility(View.GONE);
        }
        if (mIsTrainingFinished) {
            processFinishedTraining(answer, imagesCount, currentTask);
        }
        mResultLayout.setVisibility(View.VISIBLE);
    }

    private void processFinishedTraining(String answer,
                                         int imagesCount,
                                         @NonNull Task currentTask) {
        if (currentTask.isTrainingRightTask()) {
            mMarkImage.setImageResource(R.drawable.right);
            mMarkText.setText(R.string.task_details_right_answer);
            mMarkImage.setVisibility(View.VISIBLE);
        }
        if (imagesCount != 0) {
            mMarkText.setText(R.string.task_details_hard_answer);
            mMarkImage.setVisibility(View.GONE);
        } else {
            setErrorAnswerImage();
            mMarkText.setText(R.string.task_details_wrong_answer);
        }
        mMarkLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showEmptyAnswerResult() {
        mMarkText.setText(R.string.task_details_empty_answer);
        setErrorAnswerImage();
        mMarkLayout.setVisibility(View.VISIBLE);
        mResultLayout.setVisibility(View.VISIBLE);
        mTaskAnswer.setVisibility(View.GONE);
    }

    private void setErrorAnswerImage() {
        mMarkImage.setImageResource(R.drawable.error);
        mMarkImage.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideAnswer() {
        mTaskAnswer.setVisibility(View.GONE);
        mTaskAttachmentsCount.setVisibility(View.GONE);
        mResultLayout.setVisibility(View.GONE);
    }

    @Override
    public void startTimer() {
        ((TaskCallback) getActivity()).startTimer();
    }

    @Override
    public void stopTimer() {
        ((TaskCallback) getActivity()).stopTimer();
    }

    public void showPreviousTask() {
        mPresenter.updateTaskInfo(--mTaskPosition);
    }

    private void setTextDescription(String textDescription) {
//        mTaskDescriptionTv.setText(textDescription);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mTaskDescriptionTv.setText(Html.fromHtml(textDescription,
                    Html.FROM_HTML_MODE_COMPACT));
//                    new URLImageParser(mTaskDescriptionTv, getContext()), null));
        } else {
            mTaskDescriptionTv.setText(Html.fromHtml(textDescription));
//                    new URLImageParser(mTaskDescriptionTv, getContext()), null));
        }
    }

    void setBackgroundSendButton(int taskType) {
        if (taskType == TASK_B.getTaskType()) {
            mAddImageButton.setVisibility(View.GONE);
            mSendButton.setBackgroundResource(R.color.send_answer_task_b_button_color);
        } else {
            mAddImageButton.setVisibility(View.VISIBLE);
            mSendButton.setBackgroundResource(R.color.send_answer_task_c_button_color);
        }
    }

    ArrayList<Integer> getAnsweredIndexes() {
        return mPresenter.getAnsweredTaskIndexes();
    }

    ArrayList<Task> getAnsweredTasks() {
        return mPresenter.getAnsweredTasks();
    }

    void refreshDataInDb() {
        mPresenter.refreshDataInDb();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            mAttachmentsHelper.processMediaResult(requestCode, data);
        }
    }

    @Override
    protected NestedScrollView getMainScroll() {
        return mScrollView;
    }

    interface TaskCallback extends TrainingView {

        void hideLeftArrow();

        void showLeftArrow();

        void hideRightArrow();

        void showRightArrow();

        void configColors(int themeType);

    }

}
