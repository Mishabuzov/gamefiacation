package com.kpfu.mikhail.ege.screen.base.activities.base_activity;


import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.kpfu.mikhail.ege.screen.base.BaseView;

public interface BaseActivityView extends BaseView {

    void setToolbarTitle(String title);

    void setToolbarTitle(@StringRes int title);

    void setToolbarBackgroundImage(@DrawableRes int resId);

}
