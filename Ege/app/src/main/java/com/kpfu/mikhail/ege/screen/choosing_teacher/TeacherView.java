package com.kpfu.mikhail.ege.screen.choosing_teacher;

import com.kpfu.mikhail.ege.content.Teacher;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_recycler.BaseRecyclerFragmentView;

import java.util.List;

interface TeacherView extends BaseRecyclerFragmentView {

    void showTeachers(List<Teacher> teachers);

}
