package com.kpfu.mikhail.ege.screen.profile;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleFragmentActivity;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;
import com.kpfu.mikhail.ege.utils.ToolbarUtils;

public class ProfileActivity extends SingleFragmentActivity implements ProfileFragment.ProfileCallback {

    @Override
    protected BaseFragment installFragment() {
        return new ProfileFragment();
    }

    @Override
    protected Bundle attachFragmentArguments() {
        return getIntent().getExtras();
    }

    @Override
    protected void doCreatingActions() {
        setToolbarTitle(R.string.menu_item_profile);
    }

    @Override
    public void upgradeProfileToolbar(View upgradeView) {
        ViewGroup mainToolbarView = (ViewGroup) getToolbar().findViewById(R.id.main_toolbar_layout);
        mainToolbarView.addView(upgradeView);
        configProfileToolbarUpgrade(upgradeView);
    }

    private void configProfileToolbarUpgrade(View upgradeView) {
        ((Button) upgradeView).setText(R.string.profile_toolbar_settings);
        ToolbarUtils.configToolbarRightButtonView(upgradeView,
                (int) getResources().getDimension(R.dimen.margin_10));
    }

    /*private void configToolbarUpgradeView(View upgradeView) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) upgradeView.getLayoutParams();
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        upgradeView.setLayoutParams(params);
    }*/

}
