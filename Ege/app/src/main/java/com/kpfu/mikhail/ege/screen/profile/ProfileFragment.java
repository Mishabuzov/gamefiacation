package com.kpfu.mikhail.ege.screen.profile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.PopupMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kpfu.mikhail.ege.BuildConfig;
import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Badge;
import com.kpfu.mikhail.ege.content.Profile;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_scrolling.BaseScrollingFragment;
import com.kpfu.mikhail.ege.utils.AndroidUtils;
import com.kpfu.mikhail.ege.utils.AttachmentsHelper;
import com.kpfu.mikhail.ege.utils.CompressImageUtils;
import com.kpfu.mikhail.ege.utils.DialogUtils;
import com.kpfu.mikhail.ege.widget.textviews.RobotoBoldTextView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.kpfu.mikhail.ege.utils.Constants.USER_ID;

public class ProfileFragment extends BaseScrollingFragment implements ProfileView {

    @BindInt(R.integer.badges_grid_layout_column_count) int mBadgesGridLayoutColumnCount;
    @BindView(R.id.main_scroll) NestedScrollView mMainScroll;
    @BindView(R.id.profile_image) CircleImageView mProfileImage;
    @BindView(R.id.user_name) RobotoBoldTextView mUserName;
    @BindView(R.id.scores) RobotoBoldTextView mScores;
    @BindView(R.id.badges) LinearLayout mBadgesLayout;
    @BindView(R.id.rating_place) TextView mRating;

    @BindView(R.id.empty_text) LinearLayout mEmptyLayout;

    private ProfilePresenter mPresenter;
    private GridLayout mBadgesGridLayout;
    private int mBadgesRowSpec;
    private int mBadgesColumnSpec;
    private LayoutInflater mLayoutInflater;
    private int mUserId;
    private int mBadgeMargin;
    private int mBadgeWidth;
    private AttachmentsHelper mAttachmentsHelper;
    private CompressImageUtils mImageUtils;
    private String mAvatarPath;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        getArgs();
        initFragmentElements();
        mPresenter.getUserProfile();
        return view;
    }

    private void initFragmentElements() {
        mAttachmentsHelper = new AttachmentsHelper(this, mProfileImage);
        mImageUtils = new CompressImageUtils(getContext());
        mPresenter = new ProfilePresenter(getLifecycleHandler(), this, mAttachmentsHelper, mUserId);
        mLayoutInflater = getActivity().getLayoutInflater();
        mPresenter.solveToUpgradeToolbar();
    }

    @Override
    public void upgradeToolbar() {
        View toolbarUpgrade = mLayoutInflater.inflate(R.layout.toolbar_right_button_upgrade, null);
        toolbarUpgrade.setOnClickListener(this::showPopupMenu);
        ProfileCallback callback = (ProfileCallback) getActivity();
        callback.upgradeProfileToolbar(toolbarUpgrade);
    }

    private void showPopupMenu(View v) {
        PopupMenu popupMenu = new PopupMenu(getActivity(), v);
        popupMenu.inflate(R.menu.config_menu);
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
               /* case R.id.item_edit_school:
                    startActivity(new Intent(getContext(), SchoolActivity.class));
                    return true;*/
                case R.id.item_exit:
                    DialogUtils.createConfirmDialogWithContent(getContext(), this::cleanDataAndExit,
                            R.string.profile_exit, R.string.profile_exit_confirm);
                default:
                    return false;
            }
        });
        popupMenu.show();
    }

    private void getArgs() {
        Bundle args = getArguments();
        if (args != null) {
            mUserId = args.getInt(USER_ID);
        }
    }

    public void uploadAvatar(Bitmap avatarBitmap) {
        mAvatarPath = mAttachmentsHelper.getPhotoLinks().get(0);
        mPresenter.uploadAvatarAndUpdate(avatarBitmap);
    }

    @Override
    public void setCompressAvatar() {
        try {
            mProfileImage.setImageBitmap(mImageUtils.compressImage(mAvatarPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
/*
    @Override
    public void exitFromAccount() {
        getActivity().startActivity(new Intent(getActivity(), LoginFragment.class));
        getActivity().finish();
    }*/

    @Override
    protected NestedScrollView getMainScroll() {
        return mMainScroll;
    }

    @Override
    public void showProfile(Profile profile) {
        mUserName.setText(profile.getName());
        mRating.setText(String.format("%s место | %s %s",
                String.valueOf(profile.getRatingPlace()),
                String.valueOf(profile.getPoints()),
                getContext().getString(AndroidUtils.getPointsSubLineResId(profile.getPoints()))));
        mScores.setText(String.valueOf(profile.getBestTraining()));
        setAvatar(profile.getAvatar());
//        profile.setBadges(TestUtils.createBadgesForProfile());
        showBadges(profile.getBadges());
    }

    private void setAvatar(String avatarUri) {
        if (avatarUri == null || avatarUri.isEmpty()) {
            mProfileImage.setImageResource(R.drawable.user_pic);
        } else {
            Picasso.with(getContext())
                    .load(BuildConfig.API_ENDPOINT + avatarUri)
                    .into(mProfileImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            mMainScroll.setVisibility(View.VISIBLE);
                            hideLoading();
                        }

                        @Override
                        public void onError() {
                            showNetworkErrorMessage();
                        }
                    });
        }
        mProfileImage.setOnClickListener(v -> mPresenter.changeAvatar());
    }

    private void showBadges(List<Badge> badges) {
        if (badges == null || badges.size() == 0) {
            mEmptyLayout.setVisibility(View.VISIBLE);
            return;
        }
        int gridLayoutCellWidth = mBadgesLayout.getWidth() / 3;
        mBadgeMargin = mPresenter.calculateBadgeMargin(gridLayoutCellWidth);
        mBadgeWidth = gridLayoutCellWidth - mBadgeMargin * 2;
        mBadgesGridLayout = initGridLayout();
        for (Badge b : badges) {
            addBadgeToGridLayout(b);
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER_VERTICAL;
        if (mBadgesGridLayout != null) {
            mBadgesGridLayout.setLayoutParams(layoutParams);
            mBadgesLayout.addView(mBadgesGridLayout);
        }
    }

    private void addBadgeToGridLayout(Badge badge) {
        ImageView badgeImageView = (ImageView) mLayoutInflater.inflate(R.layout.profile_badge_item, null);
        //TODO: REMOVE TEST DATA
        Picasso.with(getActivity())
//                .load(BuildConfig.API_ENDPOINT + badge.getImg())
                .load(badge.getTestImage())
                .into(badgeImageView);
        badgeImageView.setOnClickListener(v -> showToastMessage(badge.getDescription()));
        mBadgesGridLayout.addView(badgeImageView, initBadgeGridLayoutParams(mBadgesRowSpec, mBadgesColumnSpec));
        if (mBadgesColumnSpec + 1 >= mBadgesGridLayoutColumnCount) {
            mBadgesColumnSpec = 0;
            mBadgesRowSpec += 1;
        } else {
            mBadgesColumnSpec += 1;
        }
    }

    private GridLayout.LayoutParams initBadgeGridLayoutParams(int rowSpec, int columnSpec) {
        GridLayout.LayoutParams params = new GridLayout.LayoutParams();
        params.width = mBadgeWidth;
        params.height = mBadgeWidth;
        params.setMargins(mBadgeMargin, mBadgeMargin, mBadgeMargin, mBadgeMargin);
        params.setGravity(Gravity.CENTER);
        params.rowSpec = GridLayout.spec(rowSpec);
        params.columnSpec = GridLayout.spec(columnSpec);
        return params;
    }

    private GridLayout initGridLayout() {
        return (GridLayout) mLayoutInflater.inflate(R.layout.badges_grid_layout, null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            mAttachmentsHelper.processMediaResult(requestCode, data);
        }
    }

    public interface ProfileCallback {

        void upgradeProfileToolbar(View upgradeView);

    }

}
