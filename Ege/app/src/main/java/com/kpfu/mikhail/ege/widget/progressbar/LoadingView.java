package com.kpfu.mikhail.ege.widget.progressbar;

public interface LoadingView {

    void showLoading();

    void hideLoading();

}