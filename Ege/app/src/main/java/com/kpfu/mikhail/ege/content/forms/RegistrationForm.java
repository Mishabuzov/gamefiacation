package com.kpfu.mikhail.ege.content.forms;


import android.support.annotation.NonNull;

public class RegistrationForm {

    private String email;
    private String password;
    private String name;
    private String username;

    public RegistrationForm() {
    }

    public RegistrationForm(@NonNull String email,
                            @NonNull String password,
                            @NonNull String name,
                            @NonNull String username) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.username = username;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NonNull String email) {
        this.email = email;
    }

    @NonNull
    public String getPassword() {
        return password;
    }

    public void setPassword(@NonNull String password) {
        this.password = password;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getUsername() {
        return username;
    }

    public void setUsername(@NonNull String username) {
        this.username = username;
    }
}
