package com.kpfu.mikhail.ege.api;

import com.kpfu.mikhail.ege.content.PictureLink;
import com.kpfu.mikhail.ege.content.Profile;
import com.kpfu.mikhail.ege.content.RatingItem;
import com.kpfu.mikhail.ege.content.RightAnswer;
import com.kpfu.mikhail.ege.content.School;
import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.content.Teacher;
import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.content.Token;
import com.kpfu.mikhail.ege.content.forms.AvatarContainer;
import com.kpfu.mikhail.ege.content.forms.CheckAnswersForm;
import com.kpfu.mikhail.ege.content.forms.RegistrationForm;
import com.kpfu.mikhail.ege.content.forms.SchoolInfo;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface GameService {

    @FormUrlEncoded
    @POST("api/v1/account/login")
    Observable<Token> login(@Field("grant_type") String grantType,
                            @Field("username") String username,
                            @Field("password") String password);

    @POST("api/v1/account/signup")
    Observable<Void> registerUser(@Body RegistrationForm form);

    @GET("api/v1/themes")
    Observable<List<Theme>> getThemes();

    @GET("api/v1/tasks")
    Observable<List<Task>> getTasks(@Query("topic_id") int topicId,
                                    @Query("type") int type,
                                    @Query("offset") int offset,
                                    @Query("limit") int limit);

    @GET("api/v1/tasks")
    Observable<List<Task>> getTasks(@Query("topic_id") int topicId,
                                    @Query("offset") int offset,
                                    @Query("limit") int limit);

    @GET("api/v1/tasks/train")
    Observable<List<Task>> getTraining();

    @POST("api/v1/tasks/check")
    Observable<List<RightAnswer>> checkUserAnswers(@Body CheckAnswersForm form);

    @Multipart
    @POST("api/v1/uploads")
    Observable<PictureLink> uploadPicture(@Part("name") RequestBody name, @Part MultipartBody.Part file);

    @Multipart
    @POST("api/v1/uploads")
    Observable<PictureLink> uploadAvatar(@Part MultipartBody.Part file);

    @GET("api/v1/users/{id}")
    Observable<Profile> getUserProfile(@Path("id") int userId);

    @GET("api/v1/schools")
    Observable<List<School>> getSchools();

    @GET("api/v1/teachers")
    Observable<List<Teacher>> getTeachers();

    @POST("/api/v1/account/edit")
    Observable<Void> updateSchoolInfo(@Body SchoolInfo schoolInfo);

    @POST("/api/v1/account/edit")
    Observable<Void> updateAvatar(@Body AvatarContainer container);

    @GET("api/v1/rating")
    Observable<List<RatingItem>> getRating();

}
