package com.kpfu.mikhail.ege.screen.register;

import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_scrolling.BaseScrollingFragmentView;

interface RegisterView extends BaseScrollingFragmentView {

    void showPassword();

    void hidePassword();

    void changeStateOfLoginButton(boolean state);

    void showErrorInLoginField();

    void hideErrorInLoginField();

    void showErrorInPasswordField();

    void hideErrorInPasswordField();

    void showErrorInEmailField();

    void hideErrorInEmailField();

    void showErrorInNameField();

    void hideErrorInNameField();

    void showErrorInSurnameField();

    void hideErrorInSurnameField();

    void openThemesScreen();
}
