package com.kpfu.mikhail.ege.screen.register;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.forms.LoginForm;
import com.kpfu.mikhail.ege.content.forms.RegistrationForm;
import com.kpfu.mikhail.ege.repository.GameProvider;
import com.kpfu.mikhail.ege.utils.PreferenceUtils;

import ru.arturvasilov.rxloader.LifecycleHandler;

import static android.support.v4.util.PatternsCompat.EMAIL_ADDRESS;
import static com.kpfu.mikhail.ege.utils.Constants.MIN_EMAIL_LENGTH;
import static com.kpfu.mikhail.ege.utils.Constants.MIN_LOGIN_LENGTH;
import static com.kpfu.mikhail.ege.utils.Constants.MIN_PASSWORD_LENGTH;

class RegisterPresenter {

    private final LifecycleHandler mLifecycleHandler;

    private final RegisterView mRegisterView;

    private boolean mIsPasswordHidden = true;

    RegisterPresenter(@NonNull LifecycleHandler lifecycleHandler,
                      @NonNull RegisterView registerView) {
        mLifecycleHandler = lifecycleHandler;
        mRegisterView = registerView;
    }

    void defineInputType() {
        if (mIsPasswordHidden) {
            mRegisterView.showPassword();
            mIsPasswordHidden = false;
        } else {
            mRegisterView.hidePassword();
            mIsPasswordHidden = true;
        }
    }

    private void register(RegistrationForm registrationForm) {
        GameProvider.provideGameRepository()
                .register(registrationForm)
                .doOnSubscribe(mRegisterView::showLoading)
                .doOnTerminate(mRegisterView::hideLoading)
                .compose(mLifecycleHandler.load(R.id.register_request))
                .subscribe(aVoid -> login(
                        registrationForm.getUsername(),
                        registrationForm.getPassword())
                        , throwable -> mRegisterView.handleErrorWithToasts(throwable,
                                R.string.register_request_error_message));
    }

    private void login(String username, String password) {
        GameProvider.provideGameRepository()
                .login(new LoginForm(username, password))
                .doOnSubscribe(mRegisterView::showLoading)
                .doOnTerminate(mRegisterView::hideLoading)
                .compose(mLifecycleHandler.reload(R.id.login_request))
                .subscribe(
                        token -> {
                            PreferenceUtils.saveToken(token);
                            mRegisterView.openThemesScreen();
                        }, throwable -> mRegisterView.handleErrorWithToasts(throwable,
                                R.string.login_unsuccess_default_error));
    }

    void tryEnableAndDisableLoginButton(String login, String password,
                                        String name, String surname,
                                        String email) {
        if (isAllFieldsAreValid(login, password, name, surname, email)
                ) {
            mRegisterView.changeStateOfLoginButton(true);
        } else {
            mRegisterView.changeStateOfLoginButton(false);
        }
    }

    void checkFieldsAndTryRegister(String login, String password,
                                   String name, String surname,
                                   String email) {
        if (isAllFieldsAreValid(login, password, name, surname, email)) {
            register(new RegistrationForm(email, password, name + " " + surname, login));
        } else {
            defineAndProcessError(login, password, name, surname, email);
        }
    }

    private void defineAndProcessError(String login, String password,
                                       String name, String surname,
                                       String email) {
        if (login.length() < MIN_LOGIN_LENGTH) {
            mRegisterView.showErrorInLoginField();
        } else {
            mRegisterView.hideErrorInLoginField();
        }
        if (password.length() < MIN_PASSWORD_LENGTH) {
            mRegisterView.showErrorInPasswordField();
        } else {
            mRegisterView.hideErrorInPasswordField();
        }
        if (name.length() < MIN_LOGIN_LENGTH) {
            mRegisterView.showErrorInNameField();
        } else {
            mRegisterView.hideErrorInNameField();
        }
        if (surname.length() < MIN_LOGIN_LENGTH) {
            mRegisterView.showErrorInSurnameField();
        } else {
            mRegisterView.hideErrorInSurnameField();
        }
        if (!isEmailValid(email) || email.length() < MIN_EMAIL_LENGTH) {
            mRegisterView.showErrorInEmailField();
        } else {
            mRegisterView.hideErrorInEmailField();
        }
    }

    private boolean isAllFieldsAreValid(String login, String password,
                                        String name, String surname,
                                        String email) {
        return login.length() >= MIN_LOGIN_LENGTH
                && password.length() >= MIN_PASSWORD_LENGTH
                && email.length() >= MIN_EMAIL_LENGTH && isEmailValid(email)
                && name.length() >= MIN_LOGIN_LENGTH
                && surname.length() >= MIN_LOGIN_LENGTH;
    }

    private boolean isEmailValid(String email) {
        return EMAIL_ADDRESS.matcher(email).matches();
    }

}