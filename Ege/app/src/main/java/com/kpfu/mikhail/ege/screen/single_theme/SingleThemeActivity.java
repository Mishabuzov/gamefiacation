package com.kpfu.mikhail.ege.screen.single_theme;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.StringRes;
import android.view.ViewGroup;
import android.widget.Button;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleFragmentActivity;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;
import com.kpfu.mikhail.ege.utils.DialogUtils;
import com.kpfu.mikhail.ege.utils.Function;
import com.kpfu.mikhail.ege.utils.ToolbarUtils;
import com.kpfu.mikhail.ege.utils.UiUtils;

import static com.kpfu.mikhail.ege.content.types.task_types.TaskType.TASK_B;
import static com.kpfu.mikhail.ege.content.types.task_types.TaskType.TASK_C;
import static com.kpfu.mikhail.ege.screen.single_theme.SingleThemeFragment.THEME;
import static com.kpfu.mikhail.ege.screen.single_theme.SingleThemeFragment.TimerCallback;
import static com.kpfu.mikhail.ege.utils.Constants.EXAM_TIME;
import static com.kpfu.mikhail.ege.utils.Constants.IS_TRAINING_MODE;

public class SingleThemeActivity extends SingleFragmentActivity implements TimerCallback {

    private static final int NO_DESCRIPTION = 0;

    private long mTimeUntilFinished;

    private CountDownTimer mTimer;

    private boolean mIsTrainingMode;

    private SingleThemeFragment mFragment;

    private Theme mTheme;

    private boolean mIsTrainingFinished = false;

    private Button mToolbarRightButtonUpgrade;

    private ViewGroup mMainToolbarView;

    @Override
    protected BaseFragment installFragment() {
        mFragment = new SingleThemeFragment();
        return mFragment;
    }

    @Override
    protected Bundle attachFragmentArguments() {
        return getIntent().getExtras();
    }

    @Override
    public void doCreatingActions() {
        getArgs();
        if (mIsTrainingMode) {
            installInitialTime();
//            installTimer(31000);
            upgradeToolbarForTraining();
        } else if (mTheme != null) {
            upgradeToolbarForSingleTheme();
        }
    }

    private void installInitialTime() {
        mTimeUntilFinished = EXAM_TIME;
    }

    private void upgradeToolbarForSingleTheme() {
        setToolbarBackground();
        setToolbarBackArrow();
       /* View toolbarUpgrade = getLayoutInflater().inflate(R.layout.toolbar_back_arrow_upgrade, null);
        toolbarUpgrade.setOnClickListener(v -> finish());
        ViewGroup mMainToolbarView = (ViewGroup) getToolbar().findViewById(R.id.main_toolbar_layout);
        mMainToolbarView.addView(toolbarUpgrade);
        ToolbarUtils.configToolbarUpgradeView(toolbarUpgrade);*/
    }

    private void setToolbarBackground() {
        if (mTheme.getType() == TASK_B.getTaskType()) {
            setToolbarBackgroundImage(TASK_B.getToolbarBackgroundImage());
        } else if (mTheme.getType() == TASK_C.getTaskType()) {
            setToolbarBackgroundImage(TASK_C.getToolbarBackgroundImage());
        }
    }

    private void installTimer(long time) {
        mTimer = new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeUntilFinished = millisUntilFinished;
//                long seconds = millisUntilFinished / 1000;
                setToolbarTitle(UiUtils.convertMillisToHourReadableFormat(millisUntilFinished));
//                UiUtils.showToast("kek", SingleThemeActivity.this);
            }

            @Override
            public void onFinish() {
                //TODO: View after finishing timer;
//                UiUtils.showToast("ololololo", SingleThemeActivity.this);
                mFragment.finishTraining();
            }
        };
        mTimer.start();
    }

    private void upgradeToolbarForTraining() {
        mToolbarRightButtonUpgrade = (Button) getLayoutInflater().inflate(R.layout.toolbar_right_button_upgrade, null);
        mMainToolbarView = (ViewGroup) getToolbar().findViewById(R.id.main_toolbar_layout);
        mMainToolbarView.addView(mToolbarRightButtonUpgrade);
        mToolbarRightButtonUpgrade.setText(R.string.training_end_button);
        ToolbarUtils.configToolbarRightButtonView(mToolbarRightButtonUpgrade,
                (int) getResources().getDimension(R.dimen.margin_10));
        mToolbarRightButtonUpgrade.setOnClickListener(v -> DialogUtils
                .createConfirmDialog(this, mFragment::finishTraining, R.string.training_finish_dialog_title));
    }

    @Override
    public void configToolbarUpgrade(Function detailsFunction) {
        mTimer.cancel();
        mIsTrainingFinished = true;
        setToolbarTitle(R.string.training_result_dialog_title);
        mToolbarRightButtonUpgrade.setText(R.string.training_details_button);
        mToolbarRightButtonUpgrade.setOnClickListener(v -> detailsFunction.action());
        addBackArrowUpgradeInToolbar();
    }

    private void addBackArrowUpgradeInToolbar() {
        Button backArrow = (Button) getLayoutInflater().inflate(R.layout.toolbar_back_arrow_upgrade, null);
        mMainToolbarView.addView(backArrow);
        backArrow.setOnClickListener((v) ->
                createAndShowExitDialog(R.string.training_exit_dialog_title, NO_DESCRIPTION));
    }

    private void getArgs() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mIsTrainingMode = bundle.getBoolean(IS_TRAINING_MODE);
            mTheme = bundle.getParcelable(THEME);
        }
    }

    private void stopTimer() {
        mTimer.cancel();
    }

    @Override
    public long getTimeUntilFinishedAndStopTimer() {
        stopTimer();
        return mTimeUntilFinished;
    }

    @Override
    public long getTimeUntilFinished() {
        return mTimeUntilFinished;
    }

    @Override
    public void updateTimer(long time) {
        installTimer(time);
    }

    @Override
    public boolean isTrainingFinished() {
        return mIsTrainingFinished;
    }

    private void createAndShowExitDialog(@StringRes int title, @StringRes int content) {
        switch (content) {
            case NO_DESCRIPTION:
                DialogUtils.createConfirmDialog(this, mFragment::cleanTrainingDataAndExit,
                        title);
                break;
            default:
                DialogUtils.createConfirmDialogWithContent(this, mFragment::cleanTrainingDataAndExit,
                        title, content);
        }
    }

    @Override
    public void onBackPressed() {
        if (mIsTrainingFinished) {
            createAndShowExitDialog(R.string.training_exit_dialog_title, NO_DESCRIPTION);
        } else if (mIsTrainingMode) {
            createAndShowExitDialog(R.string.training_exit_without_saving_dialog_title,
                    R.string.training_exit_without_saving_dialog_content);
        } else {
            super.onBackPressed();
        }
    }

    /*@Override
    protected void onStop() {
        PreferenceUtils.saveTrainingTimer(mTimeUntilFinished);
        super.onStop();
    }*/

    /*    @Override
    public void finish() {
        if (mIsTrainingFinished) {
            mFragment.cleanTrainingDataAndExit();
        }
        super.finish();
    }*/

}