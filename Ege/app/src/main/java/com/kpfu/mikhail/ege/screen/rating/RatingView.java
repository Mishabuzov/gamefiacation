package com.kpfu.mikhail.ege.screen.rating;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.content.RatingItem;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_recycler.BaseRecyclerFragmentView;

import java.util.List;

interface RatingView extends BaseRecyclerFragmentView {

    void showRating(@NonNull List<RatingItem> ratingItemList);

}
