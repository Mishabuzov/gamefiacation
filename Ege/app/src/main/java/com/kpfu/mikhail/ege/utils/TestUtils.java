package com.kpfu.mikhail.ege.utils;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Badge;

import java.util.ArrayList;
import java.util.List;

public class TestUtils {

    public static List<Badge> createBadgesForProfile() {
        Badge badge = new Badge(1, R.drawable.error, "kek");
        Badge badge1 = new Badge(1, R.drawable.error, "LOL");
        Badge badge2 = new Badge(1, R.drawable.right, "MUZHIK");
        Badge badge3 = new Badge(1, R.drawable.right, "MUZHIK");
        Badge badge4 = new Badge(1, R.drawable.error, "GROM");
        Badge badge5 = new Badge(1, R.drawable.error, "GNOM");
        Badge badge6 = new Badge(1, R.drawable.error, "TOPOR");
        Badge badge7 = new Badge(1, R.drawable.error, "ROM");
        Badge badge8 = new Badge(1, R.drawable.error, "XOR");
        Badge badge9 = new Badge(1, R.drawable.error, "RUBL");
        List<Badge> badges = new ArrayList<>();
        badges.add(badge);
        badges.add(badge1);
        badges.add(badge2);
        badges.add(badge3);
        badges.add(badge4);
        badges.add(badge5);
        badges.add(badge6);
        badges.add(badge7);
        badges.add(badge8);
        badges.add(badge9);
        badges.add(badge9);
        return badges;
    }

}
