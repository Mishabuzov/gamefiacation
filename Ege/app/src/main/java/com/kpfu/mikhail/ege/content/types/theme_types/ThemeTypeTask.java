package com.kpfu.mikhail.ege.content.types.theme_types;


import android.support.annotation.DrawableRes;

import com.kpfu.mikhail.ege.R;

public enum ThemeTypeTask {

    B_THEME("B", R.drawable.b_challenge_bg_badge),
    C_THEME("C", R.drawable.c_challenge_bg_badge);

    private String mValue;

    private int mMark;

    ThemeTypeTask(String value, @DrawableRes int mark) {
        mValue = value;
        mMark = mark;
    }

    @Override
    public String toString() {
        return mValue;
    }

    @DrawableRes
    public int getMark() {
        return mMark;
    }
}
