package com.kpfu.mikhail.ege.content;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kpfu.mikhail.ege.content.forms.RealmString;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAnswer extends RealmObject {

    @PrimaryKey
    private int id;

    private String answer;

    private String comment;

    @Ignore
    private List<String> images;

    @JsonIgnore
    private int imagesCount;

    @JsonIgnore
    private RealmList<RealmString> mRealmImages;

    public UserAnswer() {
    }

    public UserAnswer(int id, String answer, List<String> images) {
        this.id = id;
        this.answer = answer;
        this.images = images;
        imagesCount = images.size();
        mRealmImages = convertImagesListToRealmImages();
    }

    private RealmList<RealmString> convertImagesListToRealmImages() {
        RealmList<RealmString> realmStrings = new RealmList<>();
        for (String imageLink : images) {
            RealmString realmString = new RealmString(imageLink);
            realmStrings.add(realmString);
        }
        return realmStrings;
    }

    @JsonIgnore
    public RealmList<RealmString> getRealmImages() {
        return mRealmImages;
    }

    @JsonIgnore
    public void setRealmImages(RealmList<RealmString> realmImages) {
        mRealmImages = realmImages;
    }

    public void convertRealmImagesToImages() {
        for (RealmString realmString : mRealmImages) {
            if (images == null) {
                images = new ArrayList<>();
            }
            images.add(realmString.getString());
        }
    }

    public UserAnswer(int id, String answer) {
        this.id = id;
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public int getImagesCount() {
        return imagesCount;
    }

    public void setImagesCount(int imagesCount) {
        this.imagesCount = imagesCount;
    }
}
