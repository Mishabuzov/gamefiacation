package com.kpfu.mikhail.ege.screen.choosing_school;

import com.kpfu.mikhail.ege.content.School;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_recycler.BaseRecyclerFragmentView;

import java.util.List;

interface SchoolView extends BaseRecyclerFragmentView {

    void showSchools(List<School> schools);

}
