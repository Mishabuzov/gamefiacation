package com.kpfu.mikhail.ege.screen.choosing_school;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.School;
import com.kpfu.mikhail.ege.widget.BaseAdapter;

import java.util.ArrayList;

public class SchoolAdapter extends BaseAdapter<SchoolHolder, School>
        implements SchoolHolder.SchoolViewCallback {

    private View mSelectedView;

    private SchoolCallback mSchoolCallback;

    public SchoolAdapter(@NonNull SchoolCallback schoolCallback) {
        super(new ArrayList<>());
        mSchoolCallback = schoolCallback;
    }

    @Override
    public SchoolHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SchoolHolder(
                LayoutInflater.from(
                        parent.getContext()).inflate(R.layout.school_teacher_list_item, parent, false),
                mSelectedView,
                this,
                mSchoolCallback);
    }

    @Override
    public void onBindViewHolder(SchoolHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        School school = getItem(position);
        holder.bind(school);
    }

    @Override
    public void setSelectedView(View view) {
        mSelectedView = view;
    }

    @Override
    public View getSelectedView() {
        return mSelectedView;
    }

    interface SchoolCallback {

        void selectSchool(int schoolId);

    }

}
