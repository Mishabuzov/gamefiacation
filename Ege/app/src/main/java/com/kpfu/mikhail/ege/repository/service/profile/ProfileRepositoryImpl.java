package com.kpfu.mikhail.ege.repository.service.profile;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.api.ApiFactory;
import com.kpfu.mikhail.ege.content.Profile;
import com.kpfu.mikhail.ege.content.RatingItem;
import com.kpfu.mikhail.ege.content.School;
import com.kpfu.mikhail.ege.content.Teacher;
import com.kpfu.mikhail.ege.content.forms.AvatarContainer;
import com.kpfu.mikhail.ege.content.forms.SchoolInfo;

import java.util.List;

import ru.arturvasilov.rxloader.RxUtils;
import rx.Observable;

public class ProfileRepositoryImpl implements ProfileRepository {

    @NonNull
    @Override
    public Observable<Profile> getUserProfile(int userId) {
        return ApiFactory.getGameService()
                .getUserProfile(userId)
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<List<School>> getSchools() {
        return ApiFactory.getGameService()
                .getSchools()
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<List<Teacher>> getTeachers() {
        return ApiFactory.getGameService()
                .getTeachers()
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<Void> updateSchoolInfo(int userId,
                                             @NonNull SchoolInfo schoolInfo) {
        return ApiFactory.getGameService()
                .updateSchoolInfo(schoolInfo)
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<Void> updateAvatar(int userId,
                                         @NonNull AvatarContainer container) {
        return ApiFactory.getGameService()
                .updateAvatar(container)
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<List<RatingItem>> getRating() {
        return ApiFactory.getGameService()
                .getRating()
                .compose(RxUtils.async());
    }

}
