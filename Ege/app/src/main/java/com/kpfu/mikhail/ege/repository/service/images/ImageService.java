package com.kpfu.mikhail.ege.repository.service.images;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.content.PictureLink;

import java.util.List;

import rx.Observable;

public interface ImageService {

    @NonNull
    Observable<List<PictureLink>> uploadImages(@NonNull List<String> fileUris);

    @NonNull
    Observable<List<PictureLink>> uploadBitmaps(@NonNull List<Bitmap> bitmaps,
                                                @NonNull Context context);

    @NonNull
    Observable<PictureLink> uploadAvatar(@NonNull Bitmap bitmapAvatar,
                                         @NonNull Context context);

}
