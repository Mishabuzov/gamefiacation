package com.kpfu.mikhail.ege.utils;


import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.content.types.task_types.TaskType;

import java.util.concurrent.TimeUnit;

import static com.kpfu.mikhail.ege.content.types.task_types.TaskType.TASK_B;
import static com.kpfu.mikhail.ege.content.types.task_types.TaskType.TASK_C;

public class UiUtils {

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != activity.getCurrentFocus() && null != activity.getCurrentFocus().getWindowToken())
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void showToast(@NonNull final String message, @NonNull Activity activity) {
        AndroidUtils.runOnUIThread(() ->
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show());
    }

    public static void showToast(@StringRes final int message, @NonNull Activity activity) {
        AndroidUtils.runOnUIThread(() -> Toast.makeText(activity, message, Toast.LENGTH_SHORT).show());
    }

    public static String convertMillisToHourReadableFormat(long millis) {
        long hoursLong = TimeUnit.MILLISECONDS.toHours(millis);
        long minutesLong = TimeUnit.MILLISECONDS.toMinutes(millis)
                - TimeUnit.HOURS.toMinutes(hoursLong);
        long secondsLong = TimeUnit.MILLISECONDS.toSeconds(millis)
                - TimeUnit.HOURS.toSeconds(hoursLong) - TimeUnit.MINUTES.toSeconds(minutesLong);
        String hours = String.valueOf(hoursLong);
        String minutes = String.valueOf(minutesLong);
        String seconds = String.valueOf(secondsLong);
        if (hoursLong < 10) {
            hours = "0" + hours;
        }
        if (minutesLong < 10) {
            minutes = "0" + minutes;
        }
        if (secondsLong < 10) {
            seconds = "0" + seconds;
        }
        return String.format("%s:%s:%s",
                hours, minutes, seconds);
    }

    public static void fillTaskHeader(Task task,
                                      String themeTitle,
                                      TextView taskTitle,
                                      TextView subjectName,
                                      TextView pointsValue,
                                      RelativeLayout pointsBackground,
                                      TextView pointsNameLabel) {
        taskTitle.setText(task.getCode());
        subjectName.setText(themeTitle);
        pointsValue.setText(String.valueOf(task.getMaxPoints()));
        pointsNameLabel.setText(pointsValue.getContext().getString(
                AndroidUtils.getPointsSubLineResId(task.getMaxPoints())));
        if (task.getPoints() != 0) {
            task.setAnsweredTask(true);
        }
        defineAndSetPointsBackground(task.getType(),
                task.isAnsweredTask(), pointsBackground);
    }

    private static void defineAndSetPointsBackground(int taskType,
                                                     boolean isSolved,
                                                     RelativeLayout pointsBg) {
        if (taskType == TASK_B.getTaskType()) {
            setPointsBackground(TASK_B, isSolved, pointsBg);
        } else {
            setPointsBackground(TASK_C, isSolved, pointsBg);
        }
    }

    private static void setPointsBackground(TaskType type,
                                            boolean isSolved,
                                            RelativeLayout pointsBg) {
        if (isSolved) {
            pointsBg.setBackgroundResource(type.getStartedTaskImage());
        } else {
            pointsBg.setBackgroundResource(type.getNotStartedTaskImage());
        }
    }

    public static View getUpgradeView(@NonNull Activity activity,
                                      @LayoutRes int upgradeRes) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(upgradeRes, null);
    }

}
