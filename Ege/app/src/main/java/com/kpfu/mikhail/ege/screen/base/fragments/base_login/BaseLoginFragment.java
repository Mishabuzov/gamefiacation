package com.kpfu.mikhail.ege.screen.base.fragments.base_login;

import android.support.design.widget.TextInputLayout;
import android.text.method.PasswordTransformationMethod;
import android.widget.Button;
import android.widget.EditText;

import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_scrolling.BaseScrollingFragment;

public abstract class BaseLoginFragment extends BaseScrollingFragment {

    protected void showErrorInInputLayout(TextInputLayout textInputLayout, String errorMessage) {
        textInputLayout.setErrorEnabled(true);
        textInputLayout.setError(errorMessage);
    }

    protected void hideErrorFromInputLayout(TextInputLayout textInputLayout) {
        textInputLayout.setError(null);
        textInputLayout.setErrorEnabled(false);
    }

    public void showPassword(EditText passwordEdit, Button showPasswordButton, String buttonText) {
        passwordEdit.setTransformationMethod(null);
        showPasswordButton.setText(buttonText);
    }

    public void hidePassword(EditText passwordEdit, Button showPasswordButton, String buttonText) {
        passwordEdit.setTransformationMethod(new PasswordTransformationMethod());
        showPasswordButton.setText(buttonText);
    }

}
