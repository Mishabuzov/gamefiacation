package com.kpfu.mikhail.ege.screen.themes_and_types.themes;

import android.content.Intent;
import android.os.Bundle;

import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_recycler.BaseRecyclerFragment;
import com.kpfu.mikhail.ege.screen.single_theme.SingleThemeActivity;

import java.util.List;

import static com.kpfu.mikhail.ege.screen.single_theme.SingleThemeFragment.THEME;
import static com.kpfu.mikhail.ege.utils.Constants.ALL_THEMES;
import static com.kpfu.mikhail.ege.utils.Constants.IS_TRAINING_MODE;
import static com.kpfu.mikhail.ege.utils.Constants.THEME_TYPE;

public class ThemesFragment extends BaseRecyclerFragment
        implements ThemesView, ThemesAdapter.ThemesCallback {

    private ThemesPresenter mPresenter;

    private ThemesAdapter mThemesAdapter;

    private int mRecyclerItemsCount;

    private int mThemeType;

    @Override
    protected void initPresenter() {
        mPresenter = new ThemesPresenter(getLifecycleHandler(), this);
    }

    @Override
    protected void getArgs() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mThemeType = arguments.getInt(THEME_TYPE, ALL_THEMES);
        } else {
            mThemeType = ALL_THEMES;
        }
    }

    @Override
    protected void doActions() {
        mPresenter.filterThemes(mThemeType);
    }

    @Override
    protected void installAdapter() {
        mThemesAdapter = new ThemesAdapter(this);
        mThemesAdapter.attachToRecyclerView(getRecyclerView());
        getRecyclerView().setAdapter(mThemesAdapter);
    }

    @Override
    public void showThemes(List<Theme> themes) {
        mRecyclerItemsCount = themes.size();
        mThemesAdapter.changeDataSet(themes);
        showScreenAndHideLoading();
    }

    public void setToolbarState() {
        super.configToolbarBehavior(mRecyclerItemsCount);
    }

    @Override
    public void openSingleThemeScreen(Theme theme) {
        Intent intent = new Intent(getActivity(), SingleThemeActivity.class);
        intent.putExtra(THEME, theme);
        intent.putExtra(IS_TRAINING_MODE, false);
        startActivity(intent);
    }

}
