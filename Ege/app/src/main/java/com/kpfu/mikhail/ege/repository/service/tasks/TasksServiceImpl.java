package com.kpfu.mikhail.ege.repository.service.tasks;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.api.ApiFactory;
import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.content.UserAnswer;
import com.kpfu.mikhail.ege.content.types.theme_types.ThemeLoadIndicator;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import ru.arturvasilov.rxloader.RxUtils;
import rx.Observable;

import static com.kpfu.mikhail.ege.utils.Constants.IS_TRAINING_TASK_FIELD;
import static com.kpfu.mikhail.ege.utils.Constants.TOPIC_ID_FIELD;

public class TasksServiceImpl implements TasksService {

    @NonNull
    @Override
    public Observable<List<Task>> getTasksFromTopicFromServer(int topicId, int offset, int limit) {
        return ApiFactory.getGameService()
                .getTasks(topicId, offset, limit)
                .flatMap(tasks -> updateTasksFromTopicInDB(topicId, tasks))
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<List<Task>> getAllTasksFromTopicFromDB(int topicId) {
        Realm realm = Realm.getDefaultInstance();  // в случае ошибки - бери данные из базы
        RealmResults<Task> commits = realm.where(Task.class)
                .equalTo(TOPIC_ID_FIELD, topicId).findAll();
        return Observable.just(realm.copyFromRealm(commits));
    }

    @Override
    public boolean isThemeTasksLoaded(int topicId) {
        Realm realm = Realm.getDefaultInstance();
        ThemeLoadIndicator indicator = realm.where(ThemeLoadIndicator.class)
                .equalTo(TOPIC_ID_FIELD, topicId).findFirst();
        return indicator != null && indicator.isThemeLoaded();
    }

    @Override
    public void setThemeTasksLoaded(int topicId) {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            ThemeLoadIndicator indicator = realm.where(ThemeLoadIndicator.class)
                    .equalTo(TOPIC_ID_FIELD, topicId)
                    .findFirst();
            indicator.setThemeLoaded(true);
            realm.insertOrUpdate(indicator);
        });
    }

    @NonNull
    @Override
    public Observable<List<Task>> updateTasksFromTopicInDB(int topicId, List<Task> tasks) {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            Task trainingTask = realm.where(Task.class)
                    .equalTo(TOPIC_ID_FIELD, topicId)
                    .equalTo(IS_TRAINING_TASK_FIELD, true)
                    .findFirst();
            realm.insertOrUpdate(tasks);
            if (trainingTask != null) {
                trainingTask.setTrainingTask(true);
                realm.insertOrUpdate(trainingTask);
            }
        });
        return Observable.just(tasks);
    }

    @NonNull
    @Override
    public Observable<List<Task>> getTrainingTasks() {
        return getTrainingFromDb()
                .filter(result -> !result.isEmpty())
                .switchIfEmpty(getTrainingFromServer())
                .compose(RxUtils.async());
    }

    @NonNull
    @Override
    public Observable<List<Task>> getTrainingFromDb() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Task> trainingTasks = realm.where(Task.class)
                .equalTo(IS_TRAINING_TASK_FIELD, true).findAll().sort(TOPIC_ID_FIELD);
        return Observable.just(realm.copyFromRealm(trainingTasks));
    }

    @NonNull
    @Override
    public Observable<List<UserAnswer>> getUserAnswers() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<UserAnswer> userAnswers = realm.where(UserAnswer.class)
                .findAll();
        return Observable.just(realm.copyFromRealm(userAnswers));
    }

   /* private void saveUserAnswers(List<UserAnswer> answers){
        Realm.getDefaultInstance().executeTransaction(realm ->{
            realm.where(UserAnswer.class)
                    .findAll()
                    .deleteAllFromRealm();
            realm.insert(answers);
        });
    }*/

    @Override
    public void insertOrUpdateUserAnswer(@NonNull UserAnswer answer) {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            realm.insertOrUpdate(answer);
        });
    }

    @NonNull
    private Observable<List<Task>> getTrainingFromServer() {
        return ApiFactory.getGameService()
                .getTraining()
                .flatMap(this::updateTrainingTasksInDB)
                .compose(RxUtils.async());
    }

    @NonNull
    public Observable<List<Task>> updateTrainingTasksInDB(List<Task> tasks) {
        for (Task task : tasks) {
            task.setTrainingTask(true);
        }
        Realm.getDefaultInstance().executeTransaction(realm -> realm.insertOrUpdate(tasks));
        return Observable.just(tasks);
    }

    @Override
    public void clearTrainingUserAnswers() {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            realm.where(UserAnswer.class)
                    .findAll()
                    .deleteAllFromRealm();
        });
    }

    @Override
    public void clearTrainingTasksInDB() {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            RealmResults<Task> trainingTasks = realm.where(Task.class)
                    .equalTo(IS_TRAINING_TASK_FIELD, true).findAll();
            for (Task task : trainingTasks) {
                task.setTrainingTask(false);
                task.setAnsweredTask(false);
            }
        });
    }

}
