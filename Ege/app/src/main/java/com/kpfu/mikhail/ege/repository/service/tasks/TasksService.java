package com.kpfu.mikhail.ege.repository.service.tasks;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.content.Task;
import com.kpfu.mikhail.ege.content.UserAnswer;

import java.util.List;

import rx.Observable;

public interface TasksService {

    @NonNull
    Observable<List<Task>> getTasksFromTopicFromServer(int topicId, int offset, int limit);

    @NonNull
    Observable<List<Task>> getAllTasksFromTopicFromDB(int topicId);

    boolean isThemeTasksLoaded(int topicId);

    void setThemeTasksLoaded(int topicId);

    @NonNull
    Observable<List<Task>> getTrainingTasks();

    @NonNull
    Observable<List<Task>> getTrainingFromDb();

    @NonNull
    Observable<List<UserAnswer>> getUserAnswers();

    @NonNull
    Observable<List<Task>> updateTrainingTasksInDB(List<Task> tasks);

    @NonNull
    Observable<List<Task>> updateTasksFromTopicInDB(int topicId, List<Task> tasks);

    void insertOrUpdateUserAnswer(UserAnswer answer);

    void clearTrainingUserAnswers();

    void clearTrainingTasksInDB();

}
