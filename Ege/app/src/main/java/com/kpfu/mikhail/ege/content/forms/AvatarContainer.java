package com.kpfu.mikhail.ege.content.forms;

public class AvatarContainer {

    private String avatar;

    public AvatarContainer() {
    }

    public AvatarContainer(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
