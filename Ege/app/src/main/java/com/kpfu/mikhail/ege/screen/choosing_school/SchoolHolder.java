package com.kpfu.mikhail.ege.screen.choosing_school;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.School;
import com.kpfu.mikhail.ege.widget.textviews.RobotoBoldTextView;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SchoolHolder extends RecyclerView.ViewHolder {

    @BindColor(R.color.card_background) int mUnselectedColor;

    @BindColor(R.color.selected_color) int mSelectedColor;

    @BindView(R.id.name_tv) RobotoBoldTextView mNameTv;

    private final View mMainView;

//    private View mSelectedView;

    private SchoolViewCallback mViewCallback;

    private SchoolAdapter.SchoolCallback mSchoolCallback;

    public SchoolHolder(View itemView,
                        View selectedView,
                        SchoolViewCallback viewCallback,
                        SchoolAdapter.SchoolCallback schoolCallback) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mMainView = itemView;
//        mSelectedView = selectedView;
        mViewCallback = viewCallback;
        mSchoolCallback = schoolCallback;
    }

    void bind(@NonNull School school) {
        mNameTv.setText(school.getTitle());
        mMainView.setOnClickListener(v -> {
            View selectedView = mViewCallback.getSelectedView();
            if (selectedView != null) {
                deselectView(selectedView);
            }
            selectView(v);
            mSchoolCallback.selectSchool(school.getId());
        });
    }

    private void deselectView(View selectedView) {
        selectedView.setBackgroundColor(mUnselectedColor);
    }

    private void selectView(View view) {
        view.setBackgroundColor(mSelectedColor);
        mViewCallback.setSelectedView(view);
    }

    interface SchoolViewCallback {

        void setSelectedView(View view);

        View getSelectedView();

    }

}
