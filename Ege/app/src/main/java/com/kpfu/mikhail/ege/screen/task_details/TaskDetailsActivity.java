package com.kpfu.mikhail.ege.screen.task_details;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.types.task_types.TaskType;
import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleFragmentActivity;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;
import com.kpfu.mikhail.ege.utils.ToolbarUtils;
import com.kpfu.mikhail.ege.utils.UiUtils;

import static com.kpfu.mikhail.ege.screen.task_details.TaskDetailsFragment.TaskCallback;
import static com.kpfu.mikhail.ege.utils.Constants.IS_TRAINING_FINISHED;
import static com.kpfu.mikhail.ege.utils.Constants.IS_TRAINING_MODE;
import static com.kpfu.mikhail.ege.utils.Constants.REFRESHED_INDEXES;
import static com.kpfu.mikhail.ege.utils.Constants.REFRESHED_TASKS;
import static com.kpfu.mikhail.ege.utils.Constants.TIMER;

public class TaskDetailsActivity extends SingleFragmentActivity implements TaskCallback {

    public static final String TASK_TYPE = "task_type";

    private Button mBackButton;

    private Button mLeftArrowButton;

    private Button mRightArrowButton;

    private TaskDetailsFragment mFragment;

    private long mTimeUntilFinished;

    private int mThemeType;

    private boolean mIsTrainingMode;

    private boolean mIsTrainingFinished;

    private CountDownTimer mTimer;

    private Intent mIntent;

    @Override
    protected BaseFragment installFragment() {
        return mFragment = new TaskDetailsFragment();
    }

    @Override
    protected Bundle attachFragmentArguments() {
        return getIntent().getExtras();
    }

    @Override
    protected void doCreatingActions() {
        getArgs();
        upgradeToolbar();
        mIntent = new Intent();
        if (mIsTrainingMode) {
            startTimer();
        }
    }

    @Override
    public void startTimer() {
        mTimer = new CountDownTimer(mTimeUntilFinished, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeUntilFinished = millisUntilFinished;
                setToolbarTitle(UiUtils.convertMillisToHourReadableFormat(millisUntilFinished));
            }

            @Override
            public void onFinish() {
                //TODO: View after finishing timer;
                if (!mIsTrainingFinished) {
                    finish();
                }
            }
        };
        mTimer.start();
    }

    @Override
    public void stopTimer() {
        mTimer.cancel();
    }

    private void upgradeToolbar() {
        View upgradeView = UiUtils.getUpgradeView(this, R.layout.toolbar_task_upgrade);
        initToolbarButtons(upgradeView);
        setColors();
        ViewGroup mainToolbarView = (ViewGroup) getToolbar().findViewById(R.id.main_toolbar_layout);
        mainToolbarView.addView(upgradeView);
        ToolbarUtils.configToolbarUpgradeView(upgradeView);
    }

   /* private void configToolbarUpgradeView(View upgradeView) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) upgradeView.getLayoutParams();
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        upgradeView.setLayoutParams(params);
    }*/

    private void initToolbarButtons(View upgradeView) {
        mBackButton = (Button) upgradeView.findViewById(R.id.back_arrow);
        mLeftArrowButton = (Button) upgradeView.findViewById(R.id.left_arrow);
        mRightArrowButton = (Button) upgradeView.findViewById(R.id.right_arrow);
        mBackButton.setOnClickListener(v -> finish());
        mLeftArrowButton.setOnClickListener(v -> mFragment.showPreviousTask());
        mRightArrowButton.setOnClickListener(v -> mFragment.showNextTask());
    }

    private void setColors() {
        if (mThemeType == TaskType.TASK_B.getTaskType()) {
            setToolbarBackgroundImage(TaskType.TASK_B.getToolbarBackgroundImage());
            mLeftArrowButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.b_arrow_left, 0, 0, 0);
            mRightArrowButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.b_arrow_right, 0, 0, 0);
        } else {
            setToolbarBackgroundImage(TaskType.TASK_C.getToolbarBackgroundImage());
            mLeftArrowButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.c_arrow_left, 0, 0, 0);
            mRightArrowButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.c_arrow_right, 0, 0, 0);
        }
    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_switch_tasks, menu);
        mLeftArrow = menu.findItem(R.id.arrow_left);
        mRightArrow = menu.findItem(R.id.arrow_right);
        if(getThemeType() == TaskType.TASK_B.getTaskType()){
            setToolbarBackgroundImage(TaskType.TASK_B.getToolbarBackgroundImage());
            mLeftArrow.setIcon(R.drawable.b_arrow_left);
            mRightArrow.setIcon(R.drawable.b_arrow_right);
        } else {
            setToolbarBackgroundImage(TaskType.C_TASK.getToolbarBackgroundImage());
            mLeftArrow.setIcon(R.drawable.c_arrow_left);
            mRightArrow.setIcon(R.drawable.c_arrow_right);
        }
        return true;
    }*/

    private void getArgs() {
        Bundle args = getIntent().getExtras();
        if (args != null) {
            mTimeUntilFinished = args.getLong(TIMER);
            mThemeType = args.getInt(TASK_TYPE);
            mIsTrainingMode = args.getBoolean(IS_TRAINING_MODE);
            mIsTrainingFinished = args.getBoolean(IS_TRAINING_FINISHED);
        }
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.home:
                finish();
                break;
            case R.id.arrow_left:
                mFragment.showPreviousTask();
                break;
            case R.id.arrow_right:
                mFragment.showNextTask();
                break;

        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void hideLeftArrow() {
        mLeftArrowButton.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showLeftArrow() {
        mLeftArrowButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRightArrow() {
        mRightArrowButton.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showRightArrow() {
        mRightArrowButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void configColors(int themeType) {
        mThemeType = themeType;
        setColors();
        mFragment.setBackgroundSendButton(themeType);
    }

   /* @Override
    public void setAnsweredTasks(ArrayList<Integer> answeredTasksIndexes) {
        mIntent.putIntegerArrayListExtra(REFRESHED_INDEXES, answeredTasksIndexes);
    }*/

    private void saveTrainingResults() {
        if (mIsTrainingMode) {
            mIntent.putExtra(TIMER, mTimeUntilFinished);
        }
        mIntent.putIntegerArrayListExtra(REFRESHED_INDEXES, mFragment.getAnsweredIndexes());
        mIntent.putParcelableArrayListExtra(REFRESHED_TASKS, mFragment.getAnsweredTasks());
        mFragment.refreshDataInDb();
        setResult(Activity.RESULT_OK, mIntent);
    }


    @Override
    public void onBackPressed() {
        saveTrainingResults();
        super.onBackPressed();
    }

    @Override
    public void finish() {
        saveTrainingResults();
        super.finish();
    }

   /* @Override
    protected void onStop() {
        PreferenceUtils.saveTrainingTimer(mTimeUntilFinished);
        super.onStop();
    }*/

}
