package com.kpfu.mikhail.ege.screen.base.activities.navigation;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.types.ScreenType;
import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleFragmentActivity;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;
import com.kpfu.mikhail.ege.screen.profile.ProfileFragment;
import com.kpfu.mikhail.ege.screen.rating.RatingFragment;
import com.kpfu.mikhail.ege.screen.themes_and_types.ThemesSwitchFragment;
import com.kpfu.mikhail.ege.screen.training.StartTrainingFragment;
import com.kpfu.mikhail.ege.utils.DialogUtils;
import com.kpfu.mikhail.ege.utils.ToolbarUtils;

import static com.kpfu.mikhail.ege.content.types.ScreenType.PROFILE;
import static com.kpfu.mikhail.ege.content.types.ScreenType.RATING;
import static com.kpfu.mikhail.ege.content.types.ScreenType.THEMES;
import static com.kpfu.mikhail.ege.content.types.ScreenType.TRAINING;
import static com.kpfu.mikhail.ege.utils.Constants.CURRENT_USER_PROFILE_ID;
import static com.kpfu.mikhail.ege.utils.Constants.USER_ID;

public class NavigationActivity extends SingleFragmentActivity implements NavigationView, ProfileFragment.ProfileCallback {

    private BottomNavigationView mNavigation;

    private NavigationPresenter mPresenter;

    private ScreenType mType = THEMES;

    private View mProfileUpgradeView;

    private RelativeLayout mNetworkErrorLayout;

    @Override
    protected BaseFragment installFragment() {
        return new ThemesSwitchFragment();
    }

    @Override
    protected Bundle attachFragmentArguments() {
        return null;
    }

    @Override
    protected void doCreatingActions() {
        initElements();
        upgradeNavigationActivity();
    }

    private void initElements() {
        mPresenter = new NavigationPresenter(this);
        mNetworkErrorLayout = getNetworkErrorLayout();
    }

    private void upgradeNavigationActivity() {
        RelativeLayout mainLayout = getMainLayout();
        mNavigation = (BottomNavigationView) getLayoutInflater().inflate(R.layout.bottom_nav_view, mainLayout, false);
        installBottomNavigation();
        mainLayout.addView(mNavigation);
        ViewGroup mainChild = (ViewGroup) mainLayout.findViewById(R.id.main_child);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mainChild.getLayoutParams();
        params.addRule(RelativeLayout.ABOVE, R.id.navigation);
    }

    private void installBottomNavigation() {
        mNavigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.menu_tasks:
                    mType = THEMES;
                    break;
                case R.id.menu_traning:
                    mType = TRAINING;
                    break;
                case R.id.menu_profile:
                    mType = PROFILE;
                    break;
                case R.id.menu_rating:
                    mType = RATING;
                    break;
            }
            mPresenter.checkAndSwitchScreen(mType);
            return true;
        });
    }

    @Override
    public void setRatingFragment() {
        setFragment(new RatingFragment());
        setToolbarTitle(R.string.rating_toolbar_title);
    }

    @Override
    public void setTrainingFragment() {
        setFragment(new StartTrainingFragment());
        setToolbarTitle(R.string.training_toolbar_title);
    }

    @Override
    public void hideNetworkErrorLayout() {
        if (mNetworkErrorLayout.getVisibility() == View.VISIBLE) {
            mNetworkErrorLayout.setVisibility(View.GONE);
            getFragmentLayout().setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProfileToolbarUpgrade() {
        if (mProfileUpgradeView != null) {
            mProfileUpgradeView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setProfileFragment() {
        BaseFragment profileFragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(USER_ID, CURRENT_USER_PROFILE_ID);
        profileFragment.setArguments(bundle);
        setFragment(profileFragment);
        setToolbarTitle(getString(R.string.menu_item_profile));
    }

    @Override
    public void upgradeProfileToolbar(View upgradeView) {
        mProfileUpgradeView = upgradeView;
        ViewGroup mainToolbarView = (ViewGroup) getToolbar().findViewById(R.id.main_toolbar_layout);
        mainToolbarView.addView(upgradeView);
//        configProfileToolbarUpgrade(upgradeView);
        configProfileToolbarUpgrade(upgradeView);
    }

    private void configProfileToolbarUpgrade(View upgradeView) {
        ((Button) upgradeView).setText(R.string.profile_toolbar_settings);
        ToolbarUtils.configToolbarRightButtonView(upgradeView,
                (int) getResources().getDimension(R.dimen.margin_10));
        /*RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) upgradeView.getLayoutParams();
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        params.addRule(RelativeLayout.ALIGN_PARENT_END);
        int marginEnd = (int) getResources().getDimension(R.dimen.margin_10);
        params.setMargins(0, 0, marginEnd, 0);
        upgradeView.setLayoutParams(params);*/
    }

    @Override
    public void onBackPressed() {
        DialogUtils.createConfirmDialog(this, super::onBackPressed, R.string.exit_message);
    }

}