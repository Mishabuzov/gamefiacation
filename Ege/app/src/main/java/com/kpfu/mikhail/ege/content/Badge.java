package com.kpfu.mikhail.ege.content;

import android.support.annotation.DrawableRes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Badge {


    //TODO: REMOVE TEST DATA!!!


    private int id;

    private String img;

    @DrawableRes
    private int testImage;

    private String description;

    public Badge() {
    }

    public Badge(int id, int testImage, String description) {
        this.id = id;
        this.testImage = testImage;
        this.description = description;
    }

    public int getTestImage() {
        return testImage;
    }

    public void setTestImage(int testImage) {
        this.testImage = testImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
