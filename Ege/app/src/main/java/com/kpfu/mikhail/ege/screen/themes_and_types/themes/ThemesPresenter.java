package com.kpfu.mikhail.ege.screen.themes_and_types.themes;

import android.support.annotation.NonNull;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.repository.GameProvider;

import ru.arturvasilov.rxloader.LifecycleHandler;

import static com.kpfu.mikhail.ege.utils.Constants.ALL_THEMES;

class ThemesPresenter {

    @NonNull
    private final LifecycleHandler mLifecycleHandler;

    @NonNull
    private final ThemesView mView;

    ThemesPresenter(@NonNull LifecycleHandler lifecycleHandler,
                    @NonNull ThemesView view) {
        mLifecycleHandler = lifecycleHandler;
        mView = view;
    }

    void filterThemes(int themeType) {
        if (themeType == ALL_THEMES) {
            getAllThemes();
        } else {
            getThemesOfTheType(themeType);
        }
    }

    private void getAllThemes() {
        GameProvider.provideGameRepository()
                .getThemes()
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .compose(mLifecycleHandler.reload(R.id.themes_request))
                .subscribe((themes) -> {
                    mView.showThemes(themes);
                    mView.configToolbarBehavior(themes.size());
                }, throwable -> mView.handleError(throwable,
                        this::getAllThemes));
    }

    private void getThemesOfTheType(int themeType) {
        GameProvider.provideThemesService()
                .getThemesOfTheType(themeType)
                .doOnSubscribe(mView::showLoading)
                .doOnTerminate(mView::hideLoading)
                .subscribe((themes) -> {
                    mView.showThemes(themes);
                    mView.configToolbarBehavior(themes.size());
                }, Throwable::printStackTrace);
    }

}
