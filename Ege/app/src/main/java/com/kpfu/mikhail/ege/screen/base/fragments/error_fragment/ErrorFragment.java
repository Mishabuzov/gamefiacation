package com.kpfu.mikhail.ege.screen.base.fragments.error_fragment;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.util.SparseArrayCompat;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleActivityWithFragmentView;
import com.kpfu.mikhail.ege.screen.login.LoginActivity;
import com.kpfu.mikhail.ege.utils.AndroidUtils;
import com.kpfu.mikhail.ege.utils.Function;
import com.kpfu.mikhail.ege.utils.UiUtils;
import com.kpfu.mikhail.ege.utils.logger.Logger;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

import retrofit2.HttpException;

import static com.kpfu.mikhail.ege.utils.Constants.HTTP_EXCEPTION_CODE;
import static com.kpfu.mikhail.ege.utils.Constants.SERVER_EXCEPTION_CODE;

public abstract class ErrorFragment extends Fragment implements ErrorView {

    private final List<Class<?>> mNetworkExceptions =
            Arrays.asList(UnknownHostException.class, SocketTimeoutException.class,
                    ConnectException.class);

    private SparseArrayCompat<Integer> mHttpErrors = new SparseArrayCompat<>();

    {
        mHttpErrors.put(HTTP_EXCEPTION_CODE, R.string.login_unsuccess_default_error);
        mHttpErrors.put(SERVER_EXCEPTION_CODE, R.string.unidentified_problems_message);
    }

    private void showUnidentifiedError(Throwable e) {
        Logger.w(e.getMessage());
        showToastMessage(R.string.unidentified_problems_message);
    }

    @Override
    public void showNetworkErrorMessage() {
        showToastMessage(R.string.network_error_message);
    }

    @Override
    public void showToastMessage(@NonNull String message) {
        UiUtils.showToast(message, getActivity());
    }

    @Override
    public void showToastMessage(@StringRes int message) {
        UiUtils.showToast(message, getActivity());
    }

    private void setNetworkErrorScreen(Function reloadFunction) {
        ((SingleActivityWithFragmentView) getActivity()).setNetworkErrorScreen(reloadFunction);
    }

    @Override
    public void handleError(Throwable e, Function reloadFunction) {
        if (e instanceof HttpException) {
            handleHttpException((HttpException) e, false);
        } else if (mNetworkExceptions.contains(e.getClass())) {
            setNetworkErrorScreen(reloadFunction);
        } else {
            showUnidentifiedError(e);
        }
        e.printStackTrace();
    }

    @Override
    public void handleErrorWithToasts(@NonNull Throwable e,
                                      @StringRes int httpErrorToast) {
        if (e instanceof HttpException) {
            mHttpErrors.put(HTTP_EXCEPTION_CODE, httpErrorToast);
            handleHttpException((HttpException) e, true);
        } else if (mNetworkExceptions.contains(e.getClass())) {
            showToastMessage(R.string.network_error_message);
        } else {
            showUnidentifiedError(e);
        }
    }

    private void handleHttpException(@NonNull HttpException httpException,
                                     boolean isToastHandling) {
        final Integer resourceName = mHttpErrors.get(httpException.code());
        if (httpException.code() == HTTP_EXCEPTION_CODE && resourceName != null) {
            if (!isToastHandling) {
                cleanDataAndExit();
            }
            showToastMessage(resourceName);
        } else if (httpException.code() == SERVER_EXCEPTION_CODE && resourceName != null) {
            showUnidentifiedError(httpException);
        } else {
            showUnidentifiedError(httpException);
        }
    }

    @Override
    public void cleanDataAndExit() {
        AndroidUtils.clearAllSavedData();
        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }

}
