package com.kpfu.mikhail.ege.content.types;

public enum ScreenType {
    THEMES,
    PROFILE,
    TRAINING,
    RATING;
}
