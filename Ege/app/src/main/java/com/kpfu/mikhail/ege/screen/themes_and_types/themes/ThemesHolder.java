package com.kpfu.mikhail.ege.screen.themes_and_types.themes;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Theme;
import com.kpfu.mikhail.ege.content.types.theme_types.ThemeTypeTask;
import com.kpfu.mikhail.ege.screen.themes_and_types.themes.ThemesAdapter.ThemesCallback;
import com.kpfu.mikhail.ege.widget.CircularProgressBar;
import com.kpfu.mikhail.ege.widget.textviews.RobotoMediumTextView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

class ThemesHolder extends RecyclerView.ViewHolder {

    @BindString(R.string.themes_solved_tasks_format) String mSolvedTasksFormat;
    @BindView(R.id.iv_badge_background) CircleImageView mIvBadgeBackground;
    @BindView(R.id.tv_points) TextView mTvPoints;
    @BindView(R.id.cpb_progress) CircularProgressBar mCpbProgress;
    @BindView(R.id.tv_badge_title) RobotoMediumTextView mTvBadgeTitle;
    @BindView(R.id.type_theme_iv) ImageView mTypeView;
    private final View mMainView;
    private final ThemesCallback mCallback;

    ThemesHolder(@NonNull View itemView,
                 @NonNull ThemesCallback callback) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mMainView = itemView;
        mCallback = callback;
    }

    void bind(@NonNull Theme theme) {
        mTvBadgeTitle.setText(theme.getTitle());
        int solved = theme.getSolved();
        int all = theme.getAll();
        mTvPoints.setText(String.format(mSolvedTasksFormat,
                String.valueOf(solved), String.valueOf(all)));
        mCpbProgress.setProgress(solved * 100 / all);
        setItemMarkImage(theme.getCode());
        setOnItemClickListener(theme);
    }

    private void setItemMarkImage(String themeCode) {
        if (themeCode.contains(ThemeTypeTask.B_THEME.toString())) {
            mTypeView.setBackgroundResource(ThemeTypeTask.B_THEME.getMark());
        } else if (themeCode.contains(ThemeTypeTask.C_THEME.toString())) {
            mTypeView.setBackgroundResource(ThemeTypeTask.C_THEME.getMark());
        }
    }

    private void setOnItemClickListener(Theme theme) {
        mMainView.setOnClickListener(v -> {
            mCallback.openSingleThemeScreen(theme);
        });
    }

}
