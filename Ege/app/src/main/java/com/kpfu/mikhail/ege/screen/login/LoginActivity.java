package com.kpfu.mikhail.ege.screen.login;

import android.os.Bundle;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleFragmentActivity;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;

public class LoginActivity extends SingleFragmentActivity {

    @Override
    protected BaseFragment installFragment() {
        return new LoginFragment();
    }

    @Override
    protected Bundle attachFragmentArguments() {
        return null;
    }

    @Override
    protected void doCreatingActions() {
        setToolbarTitle(R.string.login_title);
    }

}
