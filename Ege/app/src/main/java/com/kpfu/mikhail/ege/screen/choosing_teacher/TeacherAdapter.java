package com.kpfu.mikhail.ege.screen.choosing_teacher;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.Teacher;
import com.kpfu.mikhail.ege.widget.BaseAdapter;

import java.util.ArrayList;

public class TeacherAdapter extends BaseAdapter<TeacherHolder, Teacher>
        implements TeacherHolder.TeacherViewCallback {

    private View mSelectedView;

    private TeacherCallback mTeacherCallback;

    public TeacherAdapter(@NonNull TeacherCallback teacherCallback) {
        super(new ArrayList<>());
        mTeacherCallback = teacherCallback;
    }

    @Override
    public TeacherHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TeacherHolder(
                LayoutInflater.from(
                        parent.getContext()).inflate(R.layout.school_teacher_list_item, parent, false),
                mSelectedView,
                this,
                mTeacherCallback);
    }

    @Override
    public void onBindViewHolder(TeacherHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        Teacher teacher = getItem(position);
        holder.bind(teacher);
    }

    @Override
    public void setSelectedView(View view) {
        mSelectedView = view;
    }

    interface TeacherCallback {

        void selectTeacher(int teacherId);

    }

}
