package com.kpfu.mikhail.ege.screen.rating;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kpfu.mikhail.ege.R;
import com.kpfu.mikhail.ege.content.RatingItem;
import com.kpfu.mikhail.ege.widget.BaseAdapter;

import java.util.ArrayList;

class RatingAdapter extends BaseAdapter<RatingHolder, RatingItem> {

    private RatingCallback mRatingCallback;

    RatingAdapter(@NonNull RatingCallback ratingCallback) {
        super(new ArrayList<>());
        mRatingCallback = ratingCallback;
    }

    @Override
    public RatingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RatingHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.rating_list_item, parent, false),
                mRatingCallback);
    }

    @Override
    public void onBindViewHolder(RatingHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        RatingItem ratingItem = getItem(position);
        holder.bind(ratingItem);
    }

    interface RatingCallback {

        void showUserProfile(int profileId);

        void showScreenAndHideLoading();

        void showNetworkErrorMessage();

    }

}
