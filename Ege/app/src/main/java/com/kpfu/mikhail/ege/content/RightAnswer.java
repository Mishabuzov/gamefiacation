package com.kpfu.mikhail.ege.content;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RightAnswer {

    @JsonProperty("task_id")
    private int taskId;

    @JsonProperty("right_answer")
    private String correctAnswer;

    private int type;

    private int points;

    @JsonProperty("max_points")
    private int maxPoints;

    public RightAnswer() {
    }

    public RightAnswer(int taskId, String correctAnswer, int type, int points, int maxPoints) {
        this.taskId = taskId;
        this.correctAnswer = correctAnswer;
        this.type = type;
        this.points = points;
        this.maxPoints = maxPoints;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }

}
