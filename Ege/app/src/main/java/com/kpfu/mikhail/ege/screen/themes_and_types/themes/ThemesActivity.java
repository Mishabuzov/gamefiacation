package com.kpfu.mikhail.ege.screen.themes_and_types.themes;

import android.os.Bundle;

import com.kpfu.mikhail.ege.content.types.task_types.TaskType;
import com.kpfu.mikhail.ege.content.types.theme_types.ThemeTypeTitle;
import com.kpfu.mikhail.ege.screen.base.activities.single_fragment_activity.SingleFragmentActivity;
import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragment;

import static com.kpfu.mikhail.ege.utils.Constants.THEME_TYPE;

public class ThemesActivity extends SingleFragmentActivity {

    @Override
    protected void doCreatingActions() {
        getArgsAndSetTitle();
    }

    private void getArgsAndSetTitle() {
        Bundle args = getIntent().getExtras();
        if (args != null) {
            upgradeToolbarForTheTypes(args.getInt(THEME_TYPE));
        }
    }

    private void upgradeToolbarForTheTypes(int themeType) {
        setToolbarBackArrow();
        setToolbarThemesTitle(themeType);
    }

    private void setToolbarThemesTitle(int themeType) {
        if (themeType == TaskType.TASK_B.getTaskType()) {
            setToolbarTitle(ThemeTypeTitle.PART_B.toString());
            setToolbarBackgroundImage(TaskType.TASK_B.getToolbarBackgroundImage());
        } else if (themeType == TaskType.TASK_C.getTaskType()) {
            setToolbarTitle(ThemeTypeTitle.PART_C.toString());
            setToolbarBackgroundImage(TaskType.TASK_C.getToolbarBackgroundImage());
        }
    }

    @Override
    protected BaseFragment installFragment() {
        return new ThemesFragment();
    }

    @Override
    protected Bundle attachFragmentArguments() {
        return getIntent().getExtras();
    }

}
