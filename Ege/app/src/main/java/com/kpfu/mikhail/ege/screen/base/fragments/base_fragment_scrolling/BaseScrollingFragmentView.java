package com.kpfu.mikhail.ege.screen.base.fragments.base_fragment_scrolling;

import com.kpfu.mikhail.ege.screen.base.fragments.base_fragment.BaseFragmentView;

public interface BaseScrollingFragmentView extends BaseFragmentView {

    void setToolbarState(int scrollHeight,
                         int scrollPaddingTop,
                         int scrollPaddingBottom,
                         int childHeight);

    void configToolbarBehavior();

    void scrollDown();

    void scrollUp();

    void hideMainScrollView();

    void showMainScrollView();

}
