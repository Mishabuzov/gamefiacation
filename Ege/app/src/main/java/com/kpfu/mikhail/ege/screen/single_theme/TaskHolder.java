package com.kpfu.mikhail.ege.screen.single_theme;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kpfu.mikhail.ege.content.Task;

import butterknife.ButterKnife;

abstract class TaskHolder extends RecyclerView.ViewHolder {

    private final SingleThemeAdapter.TaskCallback mCallback;

    private final View mItemView;

    private String mThemeTitle;

    private boolean mIsTrainingMode;

    TaskHolder(@NonNull View itemView,
               @NonNull String themeTitle,
               @NonNull SingleThemeAdapter.TaskCallback taskCallback,
               boolean isTrainingMode) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mItemView = itemView;
        mThemeTitle = themeTitle;
        mCallback = taskCallback;
        mIsTrainingMode = isTrainingMode;
    }

    public String getThemeTitle() {
        return mThemeTitle;
    }

    void setThemeTitle(String themeTitle) {
        mThemeTitle = themeTitle;
    }

    void setOnItemClickListener(int themeId, Task task) {
        mItemView.setOnClickListener(v ->
                mCallback.openTaskDetailsScreen(themeId, mThemeTitle,
                        defineRealPositionIfHeader(), task.getType()));
    }

    private int defineRealPositionIfHeader() {
        if (mIsTrainingMode) {
            return getAdapterPosition();
        } else {
            return getAdapterPosition() - 1;
        }
    }

}
